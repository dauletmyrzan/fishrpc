<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\VacancyController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\PageController;
use Illuminate\Support\Facades\Route;

// Заявки
Route::get('/', [
	NewsController::class,
	'index',
])->name('admin.index');

Route::get('/requests', [
    AdminController::class,
    'requests',
])->name('admin.requests');

// Страницы
Route::prefix('pages')->group(function () {
    Route::get('/', [
        PageController::class,
        'index',
    ])->name('admin.pages');

    Route::get('/{id}', [
        PageController::class,
        'edit',
    ])->whereNumber('id');

    Route::get('/create', [
        PageController::class,
        'create',
    ])->name('admin.page.create');

    Route::post('/page/store', [
        PageController::class,
        'store',
    ])->name('admin.page.store');

    Route::post('/page/update', [
        PageController::class,
        'update',
    ])->name('admin.page.update');

    Route::post('/delete', [
        PageController::class,
        'delete',
    ]);
});

// Вакансии
Route::prefix('vacancies')->group(function () {
    Route::get('/', [
        VacancyController::class,
        'index',
    ])->name('admin.vacancies.index');

    Route::get('/edit/{id}', [
        VacancyController::class,
        'edit',
    ]);

    Route::get('/create', [
        VacancyController::class,
        'create',
    ])->name('admin.vacancies.create');

    Route::post('/store', [
        VacancyController::class,
        'store',
    ])->name('admin.vacancies.store');

    Route::post('/update', [
        VacancyController::class,
        'update',
    ])->name('admin.vacancies.update');

    Route::post('/delete', [
        VacancyController::class,
        'delete',
    ])->name('admin.vacancies.delete');
});

// Галерея
Route::prefix('gallery')->group(function () {
    Route::get('/', [
        GalleryController::class,
        'index',
    ])->name('admin.gallery');

    // Добавление фото в галерею
    Route::post('/store', [
        GalleryController::class,
        'store',
    ])->name('admin.gallery.store');

    // Удаление галереи
    Route::post('/delete', [
        GalleryController::class,
        'delete',
    ])->name('admin.gallery.delete');
});

// Отзывы
Route::prefix('reviews')->group(function () {
    Route::get('/', [
        ReviewController::class,
        'index',
    ])->name('admin.reviews');

    // Добавление фото в отзывы
    Route::post('/store', [
        ReviewController::class,
        'store',
    ])->name('admin.reviews.store');

    // Удаление отзыва
    Route::post('/delete', [
        ReviewController::class,
        'delete',
    ])->name('admin.reviews.delete');
});

Route::get('/subscribers', [
    AdminController::class,
    'subscribers',
])->name('admin.subscribers');

// Новости
Route::prefix('news')->group(function () {
    Route::get('/', [
        NewsController::class,
        'index',
    ])->name('admin.news.index');

    Route::get('/{id}', [
        NewsController::class,
        'edit',
    ])->whereNumber('id');

    Route::get('/create', [
        NewsController::class,
        'create',
    ])->name('admin.news.create');

    Route::post('/delete', [
        NewsController::class,
        'delete'
    ])->name('admin.news.delete');

    Route::post('/store', [
        NewsController::class,
        'store'
    ])->name('admin.news.store');

    Route::post('/update', [
        NewsController::class,
        'update',
    ])->name('admin.news.update');
});

// OTHER
Route::post('/export', [
	'as'   => 'export',
	'uses' => 'AdminController@export',
]);

Route::get('/downloadFollowers/{type}', [
	'as'   => 'downloadFollowers/{type}',
	'uses' => 'ExcelController@downloadFollowers',
]);

Route::prefix('about')->group(function () {
    Route::get('/', [
        AdminController::class,
        'about',
    ]);

    Route::get('/head-office', [
        AdminController::class,
        'headOffice',
    ]);

    Route::get('/information-base', [
        AdminController::class,
        'informationBase',
    ]);

    Route::get('/achievements', [
        AdminController::class,
        'achievements',
    ]);
});
