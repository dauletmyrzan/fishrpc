<?php

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\LabController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\ReviewController;
use Illuminate\Support\Facades\Route;

Route::get('/search', [
	PostsController::class,
	'search',
]);

// Pages Routes
Route::get('/department/{code}', [
    PageController::class,
    'show',
]);

Route::get('/', [
    HomeController::class,
    'showNews',
])->name('home');

Route::get('/about', [
    HomeController::class,
    'about'
])->name('about');

Route::get('/achievements', [
    HomeController::class,
    'achievements'
])->name('achievements');

Route::get('/contact', [
    HomeController::class,
    'contact'
])->name('contact');

Route::get('/structure', [
    HomeController::class,
    'structure'
])->name('structure');

Route::get('/services', [
    HomeController::class,
    'services'
])->name('services');

Route::get('/head-office', [
    HomeController::class,
    'headOffice'
])->name('head-office');

Route::get('/department/{code}', [
    DepartmentController::class,
    'show'
]);

Route::get('/branch/{code}', [
    BranchController::class,
    'show'
]);

Route::get('/lab/{code}', [
    LabController::class,
    'show'
]);

Route::get('/cites', function () {
    return view('/cites');
})->name('cites');

Route::get('/international-cooperation', function () {
    return view('/international-cooperation');
})->name('international-cooperation');

Route::get('/information-base', [
    HomeController::class,
    'informationBase',
])->name('information-base');

Route::post('/request', [
    \App\Http\Controllers\FormController::class,
    'request',
])->name('request');

Route::post('/subscribe', [
    \App\Http\Controllers\FormController::class,
    'subscribe',
])->name('subscribe');

Route::get('/gallery', [
    GalleryController::class,
    'index',
])->name('gallery.index');

Route::get('/reviews', [
    ReviewController::class,
    'index',
])->name('reviews.index');

// Новости
Route::prefix('news')->group(function () {
    Route::get('/', [
        NewsController::class,
        'index',
    ])->name('news.index');

    Route::get('/{slug}', [
        NewsController::class,
        'show'
    ]);
});

// Вакансии
Route::prefix('vacancies')->group(function () {
    Route::get('/', [
        \App\Http\Controllers\VacancyController::class,
        'index'
    ])->name('vacancies.index');

    Route::get('/{id}', [
        \App\Http\Controllers\VacancyController::class,
        'show'
    ]);
});

// ERRORS
Route::get('404', ['as' => '404', 'uses' => 'ErrorController@notfound']);

Route::get('/posts', ['uses' => 'PostController@getPosts'])->name('posts');

/*
 | ---------------------------------------------------------
 | Переключение языков
 | ---------------------------------------------------------
*/
Route::get('locale/{locale}', [
	HomeController::class,
	'setLocale',
])->name('setLocale');

require __DIR__.'/auth.php';
