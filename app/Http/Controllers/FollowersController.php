<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Followers;

class FollowersController extends Controller
{
    public function index()
    {
        $followers = Followers::all();
        return view('admin.followers', compact(['followers']));

    }
}