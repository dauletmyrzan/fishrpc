<?php
 
namespace App\Http\Controllers;

use App\Exports\FollowersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
class ExcelController extends Controller
{ 
    public function downloadFollowers($type)
    {
        return Excel::download(new FollowersExport, 'Followers.xlsx');
    }
}