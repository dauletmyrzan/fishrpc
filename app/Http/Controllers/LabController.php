<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Page;
use Illuminate\Http\Request;

class LabController extends Controller
{
    public function show(Request $request)
    {
        $category = Category::query()->where('code', '=', 'lab')->first();
        $lab      = Page::query()
            ->where('category_id', $category->id)
            ->where('code', $request->code)
            ->first();

        if (!$lab) {
            abort(404);
        }

        return view('lab', compact('lab'));
    }
}
