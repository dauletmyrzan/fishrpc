<?php
namespace App\Http\Controllers;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Models\Requests;
use App\Models\Followers;
use App\Models\Vacancy;

class AdminController extends Controller
{
    public function requests(Request $request)
    {
        $requests = Requests::all();

        return view('admin.requests', compact(['requests']));
    }

    public function jobs(Request $request)
    {
        $jobs = Vacancy::all();

        return view('admin.vacancies', compact(['jobs']));
    }

    public function job(Request $request)
    {
        $job = Vacancy::find($request->id);

        if (!isset($job)) {
            return redirect()->route('admin.vacancies');
        }

        return view('admin.job', compact(['job']));
    }

    public function subscribers(Request $request)
    {
        $followers = Followers::all();

        return view('admin.subscribers', compact(['followers']));
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function about()
    {
        return view('admin.about.about');
    }

    public function headOffice()
    {
        return view('admin.about.head-office');
    }
}
