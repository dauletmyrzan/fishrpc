<?php
namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $lab        = Category::query()->where('code', 'lab')->first();
        $department = Category::query()->where('code', 'department')->first();
        $branch     = Category::query()->where('code', 'branch')->first();

        View::share('labs', $lab ? $lab->pages : []);
        View::share('departments', $department ? $department->pages : []);
        View::share('branches', $branch ? $branch->pages : []);

        $this->middleware('preventBackHistory');
    }
}
