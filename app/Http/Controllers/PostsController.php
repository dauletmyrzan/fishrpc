<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->input('query');
        $posts = News::query()
            ->where('title', 'LIKE', "%{$query}%")
            ->orWhere('details', 'LIKE', "%{$query}%")
            ->get();

        foreach($posts as $post)
            {
                $post->img_src = DB::table("pictures")->select("img_src")->where('news_id', $post->id)->first();
            }

        return view('search', compact('posts'));
    }
}
