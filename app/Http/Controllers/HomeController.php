<?php
namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
	public function about()
	{
		return view('about');
	}

	public function achievements()
	{
		return view('achievements');
	}

	public function contact()
	{
		return view('contact');
	}

	public function structure()
	{
		return view('structure');
	}

	public function services()
	{
		return view('services');
	}

	public function headOffice()
	{
		return view('head-office');
	}

	public function informationBase()
	{
        return view('/information-base');
	}

    public function showNews(Request $request)
    {
        $news = News::query()->orderBy('created_at', 'DESC')->limit(3)->get();

        return view('home', compact('news'));
    }

    /**
     * Устанавливает язык.
     *
     * @param  string           $locale
     * @return RedirectResponse
     */
	public function setLocale(string $locale): RedirectResponse
    {
        if (in_array($locale, config('app.available_locales'))) {
            app()->setLocale($locale);
            session()->put('locale', $locale);
        }

		return redirect()->back();
	}
}
