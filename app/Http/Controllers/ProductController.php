<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;

class ProductController extends Controller
{
    public function showProduct(Request $request)
    {
        $id = $request->id ?? null;

        if ($id == null) {
            return abort(404);
        }

        $product = Products::query()->find($id);

        if (!$product) {
            return abort(404);
        }

        return view('product', compact(['product',]));
    }

    public function index(Request $request)
    {
        $products = Products::all();

        return view('products', compact(['products']));
    }

    public function homeview()
    {
        $products = Products::all();

        return view('home', compact(['products']));
    }

    public function all_products(Request $request)
    {
        $products = Products::all();

        return view('admin.products', compact(['products']));
    }

    public function product(Request $request)
    {
        $id = isset($request->id) ? $request->id : null;
        if ($id == null) {
            return abort(404);
        }
        $product = Products::where('id', '=', $id)->first();
        if (!isset($product)) {
            return abort(404);
        }

        return view('admin.product', compact(['product']));
    }

    public function create_product()
    {
        $product = Products::all();

        return view('admin.new_product', compact(['product']));
    }

    public function add_product(Request $request)
    {
        $product                     = new Products;
        $product->name               = $request->name;
        $product->name_en            = $request->name_en;
        $product->formula            = $request->formula;
        $product->formula_en         = $request->formula_en;
        $product->properties         = $request->properties;
        $product->properties_en      = $request->properties_en;
        $product->appearance         = $request->appearance;
        $product->appearance_en      = $request->appearance_en;
        $product->application        = $request->application;
        $product->application_en     = $request->application_en;
        $product->recommendations    = $request->recommendations;
        $product->recommendations_en = $request->recommendations_en;
        $product->rate               = $request->rate;
        $product->rate_en            = $request->rate_en;
        $product->packaging          = $request->packaging;
        $product->packaging_en       = $request->packaging_en;
        $product->img_src            = $request->img_src;
        $product->save();

        return redirect()->route('admin.all_products')->with('status', 'Продукт успешно добавлен!');
    }

    public function updateProduct(Request $request)
    {
        $product                     = Products::find($request->id);
        $product->id                 = $request->id;
        $product->name               = $request->name;
        $product->name_en            = $request->name_en;
        $product->formula            = $request->formula;
        $product->formula_en         = $request->formula_en;
        $product->properties         = $request->properties;
        $product->properties_en      = $request->properties_en;
        $product->appearance         = $request->appearance;
        $product->appearance_en      = $request->appearance_en;
        $product->application        = $request->application;
        $product->application_en     = $request->application_en;
        $product->recommendations    = $request->recommendations;
        $product->recommendations_en = $request->recommendations_en;
        $product->rate               = $request->rate;
        $product->rate_en            = $request->rate_en;
        $product->packaging          = $request->packaging;
        $product->packaging_en       = $request->packaging_en;
        $product->img_src            = $request->img_src;
        $product->save();

        return redirect()->back()->with('status', 'Информация по продукту успешно обновлена!');
    }

    public function delete(Request $request)
    {
        Products::destroy($request->id);

        return redirect()->route('admin.all_products')->with('status', 'Продукт успешно удален!');
    }
}
