<?php
namespace App\Http\Controllers;

use App\Models\Vacancy;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
	public function show(Request $request)
    {
        if ($request->id === null) {
            abort(404);
        }

        $job = Vacancy::query()->find($request->id);

        if (!isset($job)) {
            return abort(404);
        }

        return view('vacancies.show', compact('job'));
    }

    public function index()
    {
        $jobs = Vacancy::query()->orderBy('created_at', 'desc')->get();

		return view('vacancies.index', compact('jobs'));
    }
}
