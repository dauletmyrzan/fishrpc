<?php
namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    public function request(Request $request): RedirectResponse
    {
        $name    = $request->name;
        $phone   = $request->phone;
        $email   = $request->email;
        $message = $request->message ?? '';

        DB::table("requests")->insert([
            ['email' => $email, 'name' => $name, 'phone' => $phone, 'message' => $message],
        ]);

        return redirect()->back();
    }

    public function subscribe(Request $request): RedirectResponse
    {
        DB::table("followers")->insert([
            ['email' => $request->email],
        ]);

        return redirect()->back();
    }
}
