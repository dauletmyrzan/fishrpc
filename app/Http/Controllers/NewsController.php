<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::query()->orderBy('created_at', 'DESC')->paginate(15);

        return view('news.index', compact('news'));
    }

    public function show(Request $request)
    {
        if ($request->slug === null) {
            abort(404);
        }

		$post = News::query()->where('slug', $request->slug)->first();

        if (!$post) {
            abort(404);
        }

        return view('news.show', compact('post'));
    }
}
