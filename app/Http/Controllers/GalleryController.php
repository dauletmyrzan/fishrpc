<?php
namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\News;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * Главная страница галереи
     *
     * @param  Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $images = Gallery::all();

        return view('gallery', compact('images'));
    }
}
