<?php
namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function show(Request $request)
	{
	    $page = Page::query()->where('code', $request->code)->first();

	    if (!$page) {
	        abort(404, 'Not found');
	    }

	    return view('page', compact('page'));
	}

	public function index(Request $request)
	{
	    $pages = Page::all();

	    return view('admin.pages', compact('pages'));
	}


	public function PageSetting(Request $request)
    {
        $page = Page::find($request->id);
        if(!isset($page))
        {
            return redirect()->route('admin.pages');
        }
        return view('admin.page', compact(['page']));
    }

    public function NewPage()
    {
        $page = Page::all();
        return view('admin.new_page', compact(['page']));
    }


    public function updatePage(Request $request)
    {
        $page = Page::find($request->id);
        $page->code = str_slug($request->name_ru);

		$page->parent = $request->parent;

		$page->name_ru = $request->name_ru;
		$page->name_kz = $request->name_kz;
		$page->name_en = $request->name_en;

		$page->content_ru = $request->content_ru;
		$page->content_kz = $request->content_kz;
		$page->content_en = $request->content_en;
        $page->save();
        return redirect()->back()->with('status', 'Информация по странице успешно обновлена!');
    }

    public function CreateNewPage(Request $request)
    {
        $page = new Page();
		$page->code = str_slug($request->name_ru);

		$page->parent = $request->parent;

		$page->name_ru = $request->name_ru;
		$page->name_kz = $request->name_kz;
		$page->name_en = $request->name_en;

		$page->content_ru = $request->content_ru;
		$page->content_kz = $request->content_kz;
		$page->content_en = $request->content_en;
        if($request->hasFile('pictures'))
        {
            $this->validate($request, [
                'pictures[]' => 'mimes:jpeg,png,bmp,tiff,gif|max:4096',
            ],
            $messages = [
                'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max' => 'Размер файла великоват. Надо до 4 Мб.'
            ]);
            foreach($request->file('pictures') as $pic)
            {
                $extension = $pic->extension();
                $hash = md5($pic->getClientOriginalName().rand(0, 10000));
                Storage::disk('public_uploads')->putFileAs('/images/pages/', $pic, $hash . "." . $extension);
                $page->img_src = $hash . "." . $extension;
                $page->save();
            }
        }
         return redirect()->back()->with('status', 'Страница успешно добавлена!');
    }
}
