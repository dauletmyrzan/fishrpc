<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Review;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReviewController extends Controller
{
    public function index()
    {
        $reviews = Review::query()->paginate(15);

        return view('admin.reviews.index', compact('reviews'));
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        if (!$request->hasFile('pictures')) {
            return back()->with('status', 'Пожалуйста, загрузите картинки.');
        }

        $this->validate($request, [
            'pictures[]' => 'mimes:jpeg,png,bmp,tiff,gif|max:4096',
        ], [
            'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
            'max'   => 'Размер файла великоват. Надо до 4 Мб.',
        ]);

        $pictures = $request->file('pictures');

        foreach ($pictures as $pic) {
            $extension = $pic->extension();
            $hash      = md5($pic->getClientOriginalName() . rand(0, 10000));
            Storage::disk('public_uploads')->putFileAs('/images/reviews/', $pic, $hash . "." . $extension);
            $review          = new Review;
            $review->img_src = $hash . "." . $extension;
            $review->save();
        }

        return redirect()->route('admin.reviews')->with('status', 'Картинки успешно добавлены!');
    }

    /**
     * Удаление картинки
     *
     * @param  Request          $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        $photo   = Review::query()->find($request->id);
        $deleted = false;

        if (Storage::disk('public_uploads')->delete('/images/reviews/' . $photo->img_src)) {
            $deleted = $photo->delete();
        }

        return redirect()->back()
            ->with('status', $deleted ? 'Картинка успешно удалена!' : 'Ошибка при удалении');
    }
}
