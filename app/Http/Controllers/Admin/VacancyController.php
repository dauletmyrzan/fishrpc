<?php
namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Vacancy;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VacancyController extends Controller
{
    use SoftDeletes;

    public function index()
    {
        $vacancies = Vacancy::query()->paginate(15);

        return view('admin.vacancies.index', compact('vacancies'));
    }

    public function create()
    {
        $category = Category::query()->where('code', '=', 'branch')->first();
        $branches = $category->pages;

        return view('admin.vacancies.create', compact('branches'));
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $vacancy  = new Vacancy();
        $fileName = '';

        if ($request->has('file')) {
            $this->validate($request, [
                'file' => 'mimes:pdf,doc,docx,xlsx,xls|max:20480',
            ], [
                'mimes' => 'Неверный формат. Допустимые форматы: pdf, doc, docx, xlsx, xls.',
                'max'   => 'Размер файла великоват. Надо до 20 Мб.',
            ]);

            $file      = $request->file('file');
            $extension = $file->extension();
            $hash      = md5($file->getClientOriginalName() . rand(0, 10000));

            Storage::disk('public_uploads')->putFileAs('/files/', $file, $hash . '.' . $extension);
            $fileName = $hash . '.' . $extension;
        }

        $vacancy->position_ru     = $request->position_ru;
        $vacancy->position_kz     = $request->position_kz;
        $vacancy->position_en     = $request->position_en;
        $vacancy->requirements_ru = urlencode($request->requirements_ru);
        $vacancy->requirements_kz = urlencode($request->requirements_kz);
        $vacancy->requirements_en = urlencode($request->requirements_en);
        $vacancy->conditions_ru   = urlencode($request->conditions_ru);
        $vacancy->conditions_kz   = urlencode($request->conditions_kz);
        $vacancy->conditions_en   = urlencode($request->conditions_en);
        $vacancy->duties_ru       = urlencode($request->duties_ru);
        $vacancy->duties_kz       = urlencode($request->duties_kz);
        $vacancy->duties_en       = urlencode($request->duties_en);
        $vacancy->schedule        = $request->schedule;
        $vacancy->wage            = $request->wage;
        $vacancy->branch_code     = $request->branch_code;
        $vacancy->file            = $fileName;

        return redirect()
            ->route('admin.vacancies.index')
            ->with('status', $vacancy->save() ? 'Вакансия добавлена' : 'Ошибка при добавлении');
    }

    public function edit(Request $request)
    {
        $vacancy = Vacancy::query()->find($request->id);

        if (!$vacancy) {
            abort(404);
        }

        return view('admin.vacancies.edit', compact('vacancy'));
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request): RedirectResponse
    {
        $vacancy = Vacancy::query()->find($request->id);

        if (!$vacancy) {
            abort(404);
        }

        $fileName = '';

        if ($request->has('file')) {
            $this->validate($request, [
                'file' => 'mimes:pdf,doc,docx,xlsx,xls|max:20480',
            ], [
                'mimes' => 'Неверный формат. Допустимые форматы: pdf, doc, docx, xlsx, xls.',
                'max'   => 'Размер файла великоват. Надо до 20 Мб.',
            ]);

            $file      = $request->file('file');
            $extension = $file->extension();
            $hash      = md5($file->getClientOriginalName() . rand(0, 10000));

            Storage::disk('public_uploads')->putFileAs('/files/', $file, $hash . '.' . $extension);
            $fileName = $hash . '.' . $extension;
        }

        $updated = $vacancy->update([
            'position_ru'     => $request->position_ru,
            'position_kz'     => $request->position_kz,
            'position_en'     => $request->position_en,
            'requirements_ru' => urlencode($request->requirements_ru),
            'requirements_kz' => urlencode($request->requirements_kz),
            'requirements_en' => urlencode($request->requirements_en),
            'conditions_ru'   => urlencode($request->conditions_ru),
            'conditions_kz'   => urlencode($request->conditions_kz),
            'conditions_en'   => urlencode($request->conditions_en),
            'duties_ru'       => urlencode($request->duties_ru),
            'duties_kz'       => urlencode($request->duties_kz),
            'duties_en'       => urlencode($request->duties_en),
            'schedule'        => $request->schedule,
            'wage'            => $request->wage,
            'branch_code'     => $request->branch_code,
            'file'            => $fileName,
        ]);

        return redirect()->route('admin.vacancies.index')->with('status', $updated ? 'Вакансия обновлена' : 'Ошибка обновления');
    }

    public function delete(Request $request): RedirectResponse
    {
        $vacancy = Vacancy::query()->find($request->id);

        return redirect()
            ->route('admin.vacancies.index')
            ->with('status', $vacancy->delete() ? 'Вакансия удалена' : 'Ошибка удаления');
    }
}
