<?php
namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\News;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        if (!$request->hasFile('pictures')) {
            return back()->with('status', 'Пожалуйста, загрузите картинки.');
        }

        $this->validate($request, [
            'pictures[]' => 'mimes:jpeg,png,bmp,tiff,gif|max:4096',
        ], [
            'mimes' => 'Загрузите, пожалуйста, картинки. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
            'max'   => 'Размер файла великоват. Надо до 4 Мб.',
        ]);

        $pictures = $request->file('pictures');

        foreach ($pictures as $pic) {
            $extension = $pic->extension();
            $hash      = md5($pic->getClientOriginalName() . rand(0, 10000));
            Storage::disk('public_uploads')->putFileAs('/images/gallery/', $pic, $hash . "." . $extension);
            $gallery          = new Gallery;
            $gallery->img_src = $hash . "." . $extension;
            $gallery->save();
        }

        return redirect()->route('admin.gallery')->with('status', 'Картинки успешно добавлены!');
    }

    /**
     * Главная страница галереи
     *
     * @param  Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $images = Gallery::query()->orderBy('id', 'desc')->paginate(15);

        return view('admin.gallery', compact('images'));
    }

    /**
     * Удаление картинки
     *
     * @param  Request          $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        $photo   = Gallery::query()->find($request->id);
        $deleted = false;

        if (Storage::disk('public_uploads')->delete('/images/gallery/' . $photo->img_src)) {
            $deleted = $photo->delete();
        }

        return redirect()->back()
            ->with('status', $deleted ? 'Картинка успешно удалена!' : 'Ошибка при удалении');
    }
}
