<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageController extends Controller
{
    /**
     * Индексная страница
     *
     * @param  Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Страница изменения
     *
     * @param  Request $request
     * @return View
     */
    public function edit(Request $request): View
	{
	    $page = Page::query()->find($request->id);

	    if (!$page) {
	        abort(404, 'Not found');
	    }

        $categories = Category::all();

	    return view('admin.pages.edit', compact(['page', 'categories']));
	}


	public function PageSetting(Request $request)
    {
        $page = Page::find($request->id);
        if(!isset($page))
        {
            return redirect()->route('admin.pages');
        }
        return view('admin.page', compact(['page']));
    }

    /**
     * Страница создания записи
     *
     * @return View
     */
    public function create(): View
    {
        $categories = Category::all();

        return view('admin.pages.create', compact('categories'));
    }

    /**
     * Обновляет запись
     *
     * @param  Request          $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        $deleted = Page::query()->find($request->id)->delete();

        return redirect()->back()->with('status', $deleted ? 'Страница удалена!' : 'Ошибка при удалении');
    }

    /**
     * Обновляет запись
     *
     * @param  Request          $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $page = Page::query()->find($request->id);

        $page->code        = Str::slug($request->name_ru);
        $page->category_id = $request->category_id;
        $page->name_ru     = $request->name_ru;
        $page->name_kz     = $request->name_kz;
        $page->name_en     = $request->name_en;
        $page->content_ru  = urlencode($request->content_ru);
        $page->content_kz  = urlencode($request->content_kz);
        $page->content_en  = urlencode($request->content_en);

        $page->save();

        return redirect()->back()->with('status', 'Информация по странице успешно обновлена!');
    }

    /**
     * Создает запись
     *
     * @param  Request          $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $page = new Page();

        $page->code        = Str::slug($request->name_ru);
        $page->category_id = $request->category_id;
        $page->name_ru     = $request->name_ru;
        $page->name_kz     = $request->name_kz;
        $page->name_en     = $request->name_en;
        $page->content_ru  = urlencode($request->content_ru);
        $page->content_kz  = urlencode($request->content_kz);
        $page->content_en  = urlencode($request->content_en);

        return redirect()
            ->back()
            ->with('status', $page->save() ? 'Страница успешно добавлена!' : 'Ошибка при добавлении страницы');
    }
}
