<?php
namespace App\Http\Controllers\Admin;

use App\Models\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::query()->paginate(15);

        return view('admin.news.index', compact(['news']));
    }

    public function show(Request $request)
    {
        if ($request->id === null) {
            abort(404);
        }

        $post = News::query()->find($request->id);

        if (!$post) {
            abort(404);
        }

        return view('news.show', compact('post'));
    }

    /**
     * Создает запись
     *
     * @param  Request             $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $post = new News;

        $post->title          = $request->title;
        $post->title_en       = $request->title_en;
        $post->title_kz       = $request->title_kz;
        $post->description    = $request->description;
        $post->description_en = $request->description_en;
        $post->description_kz = $request->description_kz;
        $post->details        = urlencode($request->details);
        $post->details_en     = urlencode($request->details_en);
        $post->details_kz     = urlencode($request->details_kz);
        $post->slug           = Str::slug($request->title);

        if ($request->has('picture')) {
            $this->validate($request, [
                'picture' => 'mimes:jpeg,png,bmp,tiff,gif|max:4096',
            ], [
                'mimes' => 'Неверный формат. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max'   => 'Размер файла великоват. Надо до 4 Мб.',
            ]);

            $picture   = $request->file('picture');
            $extension = $picture->extension();
            $hash      = md5($picture->getClientOriginalName() . rand(0, 10000));

            Storage::disk('public_uploads')->putFileAs('/images/news/', $picture, $hash . '.' . $extension);
            $post->img = $hash . '.' . $extension;
        }

        return redirect()->route('admin.news.index')->with('status', $post->save()
            ? 'Новость успешно добавлена!'
            : 'Ошибка при добавлении новости'
        );
    }

    public function create()
    {
        return view('admin.news.create');
    }

    public function edit(Request $request)
    {
        $post = News::query()->find($request->id);

        if (!$post) {
            abort(404);
        }

        return view('admin.news.edit', compact('post'));
    }

    /**
     * Обновляет новость.
     *
     * @param  Request             $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request): RedirectResponse
    {
        $post = News::query()->find($request->id);

        if (!$post) {
            abort(404);
        }

        $post->title          = $request->title;
        $post->title_en       = $request->title_en;
        $post->title_kz       = $request->title_kz;
        $post->description    = $request->description;
        $post->description_en = $request->description_en;
        $post->description_kz = $request->description_kz;
        $post->details        = urlencode($request->details);
        $post->details_en     = urlencode($request->details_en);
        $post->details_kz     = urlencode($request->details_kz);

        if ($request->hasFile('picture')) {
            $this->validate($request, [
                'picture' => 'mimes:jpeg,png,bmp,tiff,gif|max:4096',
            ], [
                'mimes' => 'Неверный формат. Допустимые форматы: jpeg, gif, png, bmp, tiff.',
                'max'   => 'Размер файла великоват. Надо до 4 Мб.',
            ]);

            $picture   = $request->file('picture');
            $extension = $picture->extension();
            $hash      = md5($picture->getClientOriginalName() . rand(0, 10000));

            Storage::disk('public_uploads')->putFileAs('/images/news/', $picture, $hash . "." . $extension);
            $post->img = $hash . "." . $extension;
        }

        return redirect()->back()->with('status', $post->save()
            ? 'Информация по новости успешно обновлена!'
            : 'Ошибка при обновлении');
    }

    /**
     * Удаляет новость.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request): RedirectResponse
    {
        $deleted = News::query()->find($request->id)->delete();

        return redirect()->route('admin.news.index')
            ->with('status', $deleted ? 'Новость успешно удалена!' : 'Ошибка при удалении');
    }
}
