<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Page;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function show(Request $request)
    {
        $category = Category::query()->where('code', '=', 'branch')->first();
        $branch   = Page::query()
            ->where('category_id', $category->id)
            ->where('code', $request->code)
            ->first();

        if (!$branch) {
            abort(404);
        }

        return view('branch', compact('branch'));
    }
}
