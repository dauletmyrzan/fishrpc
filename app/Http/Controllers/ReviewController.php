<?php
namespace App\Http\Controllers;

use App\Models\Review;

class ReviewController
{
    public function index()
    {
        $reviews = Review::all();

        return view('reviews', compact('reviews'));
    }
}
