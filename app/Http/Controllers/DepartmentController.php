<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Page;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function show(Request $request)
	{
        $category   = Category::query()->where('code', '=', 'department')->first();
        $department = Page::query()
            ->where('category_id', $category->id)
            ->where('code', $request->code)
            ->first();

        if (!$department) {
            abort(404);
        }

        return view('department', compact('department'));
	}
}
