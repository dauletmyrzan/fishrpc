<?php

namespace App\Exports;

use App\Followers;
use Maatwebsite\Excel\Concerns\FromCollection;

class FollowersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Followers::all();
    }
}
