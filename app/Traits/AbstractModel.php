<?php
namespace App\Traits;

use Jenssegers\Date\Date;

trait AbstractModel
{
    public function getCreatedAtAttribute($createdAt): string
    {
        return Date::parse($createdAt)->format('j F Y h:i');
    }
}
