<?php
namespace App\Models;

use App\Traits\AbstractModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacancy extends Model
{
    use AbstractModel, SoftDeletes;

    protected $table = 'jobs';

    protected $fillable = [
        'position_ru',
        'position_kz',
        'position_en',
        'requirements_ru',
        'requirements_kz',
        'requirements_en',
        'conditions_ru',
        'conditions_kz',
        'conditions_en',
        'duties_ru',
        'duties_kz',
        'duties_en',
        'wage',
        'schedule',
        'branch_code',
        'file',
    ];

    public function getPositionWithLangAttribute()
    {
        if (app()->getLocale() === 'ru') {
            return $this->position_ru;
        } elseif (app()->getLocale() === 'en') {
            return $this->position_en;
        }

        return $this->position_kz;
    }

    public function getConditionsWithLangAttribute()
    {
        if (app()->getLocale() === 'ru') {
            return $this->conditions_ru;
        } elseif (app()->getLocale() === 'en') {
            return $this->conditions_en;
        }

        return $this->conditions_kz;
    }

    public function getRequirementsWithLangAttribute()
    {
        if (app()->getLocale() === 'ru') {
            return $this->requirements_ru;
        } elseif (app()->getLocale() === 'en') {
            return $this->requirements_en;
        }

        return $this->requirements_kz;
    }

    public function getDutiesWithLangAttribute()
    {
        if (app()->getLocale() === 'ru') {
            return $this->duties_ru;
        } elseif (app()->getLocale() === 'en') {
            return $this->duties_en;
        }

        return $this->duties_kz;
    }

    public function getBranch()
    {
        $branch = Page::query()->where('code', $this->branch_code)->first();

        if (!$branch) {
            return $this->branch_code;
        }

        return $branch->name;
    }
}
