<?php
namespace App\Models;

use App\Traits\AbstractModel;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use AbstractModel;

    /**
	 * Возвращает атрибут details.
	 *
	 * @param  string $details
	 * @return string
	 */
	public function getDetailsAttribute(string $details): string
	{
        if (app()->getLocale() === 'ru') {
            return urldecode($details);
        } elseif (app()->getLocale() === 'en') {
            return urldecode($this->details_en);
        }

        return urldecode($this->details_kz);
	}

    public function getTitleWithLangAttribute()
    {
        if (app()->getLocale() === 'ru') {
            return $this->title;
        } elseif (app()->getLocale() === 'en') {
            return $this->title_en;
        }

        return $this->title_kz;
    }
}
