<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function getNameAttribute()
    {
        if (app()->getLocale() === 'ru') {
            return $this->name_ru;
        } elseif (app()->getLocale() === 'en') {
            return $this->name_en;
        }

        return $this->name_kz;
    }

    /**
     * Возвращает атрибут details.
     *
     * @return string
     */
    public function getContentAttribute(): string
    {
        if (app()->getLocale() === 'ru') {
            return urldecode($this->content_ru);
        } elseif (app()->getLocale() === 'en') {
            return urldecode($this->content_en);
        }

        return urldecode($this->content_kz);
    }

    /**
     * Возвращает атрибут content_kz.
     *
     * @param $content_kz
     * @return string
     */
    public function getContentKzAttribute($content_kz): string
    {
        return urldecode($content_kz);
    }

    /**
     * Возвращает атрибут content_ru.
     *
     * @param $content_ru
     * @return string
     */
    public function getContentRuAttribute($content_ru): string
    {
        return urldecode($content_ru);
    }

    /**
     * Возвращает атрибут content_en.
     *
     * @param $content_en
     * @return string
     */
    public function getContentEnAttribute($content_en): string
    {
        return urldecode($content_en);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
