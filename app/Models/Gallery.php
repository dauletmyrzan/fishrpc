<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Gallery extends Model
{
    public function getCreatedAtAttribute($createdAt): string
    {
        return Date::parse($createdAt)->format('j F Y h:i');
    }
}
