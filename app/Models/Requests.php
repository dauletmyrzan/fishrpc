<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    public static function Requests($id)
    {
    	return DB::table("requests")->select("id", "name", "email", "phone", "message")->where("id", $id)->get();
    }
}
