<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	/**
	 * Возвращает атрибут details.
	 *
	 * @param  string $content
	 * @return string
	 */
	public function getContentAttribute(string $content): string
	{
		return urldecode($content);
	}

	/**
	 * Возвращает атрибут details.
	 *
	 * @param  string $content
	 * @return string
	 */
	public function getContentKzAttribute(string $content): string
	{
		return urldecode($content);
	}

	/**
	 * Возвращает атрибут details.
	 *
	 * @param  string $content
	 * @return string
	 */
	public function getContentEnAttribute(string $content): string
	{
		return urldecode($content);
	}
}
