/**
 * Template Name: NewBiz - v4.5.0
 * Template URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
 * Author: BootstrapMade.com
 * License: https://bootstrapmade.com/license/
 */
(function () {
    "use strict";

    /**
     * Initiate portfolio lightbox
     */
    const portfolioLightbox = GLightbox({
        selector: '.portfolio-lightbox',
    });
})();
