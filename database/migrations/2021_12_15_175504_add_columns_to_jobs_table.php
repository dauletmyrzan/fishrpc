<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->string('city_ru');
            $table->string('city_kz');
            $table->string('city_en');
            $table->text('duties_ru');
            $table->text('duties_kz');
            $table->text('duties_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('city_ru');
            $table->dropColumn('city_kz');
            $table->dropColumn('city_en');
            $table->dropColumn('duties_ru');
            $table->dropColumn('duties_kz');
            $table->dropColumn('duties_en');
        });
    }
}
