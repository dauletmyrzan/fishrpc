<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKzColumnsToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->text('position_kz');
            $table->renameColumn('position', 'position_ru');
            $table->text('requirements_kz');
            $table->renameColumn('requirements', 'requirements_ru');
            $table->string('schedule_kz');
            $table->renameColumn('schedule', 'schedule_ru');
            $table->text('conditions_kz');
            $table->renameColumn('conditions', 'conditions_ru');
            $table->dropColumn('wage_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            //
        });
    }
}
