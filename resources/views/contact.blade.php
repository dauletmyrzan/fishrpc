@extends('layouts.app')

@section('content')
    <style>
        .accordion-header {
            margin-bottom: 0!important;
        }
    </style>
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <h1 class="fw-bold">Контакты</h1>
            <div class="row gy-4">
                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <div class="accordion mb-5" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSix">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseTen" aria-expanded="false"
                                            aria-controls="collapseTen">
                                        @lang('head.management')
                                    </button>
                                </h2>
                                <div id="collapseTen" class="accordion-collapse collapse " aria-labelledby="headingSix"
                                     data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="col-lg-12">
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('head.director')
                                                            : </strong>@lang('head.directorname')</li>
                                                    <li><strong>@lang('head.phone'): </strong>+7 (727) 383-15-05</li>
                                                    <li><strong>Email: </strong>isbekov@fishrpc.kz</li>
                                                </ul>
                                            </div>
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('head.zamdirector')
                                                            : </strong>@lang('head.zamdirectorname')</li>
                                                    <li><strong>@lang('head.phone'): </strong>+7 (727) 383-17-14</li>
                                                    <li><strong>Email: </strong>assylbekova@fishrpc.kz</li>
                                                </ul>
                                            </div>
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('head.sekretar')
                                                            : </strong>@lang('head.sekretarname')</li>
                                                    <li><strong>@lang('head.phone'): </strong>+7 (727) 390-95-75</li>
                                                    <li><strong>Email: </strong>mukhramova@fishrpc.kz</li>
                                                </ul>
                                            </div>
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('head.sovetnik')
                                                            : </strong>@lang('head.sovetnikname')</li>
                                                    <li><strong>@lang('head.phone'): </strong>8-701-566-70-07</li>
                                                    <li><strong>Email: </strong>romashov@fishrpc.kz</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="portfolio-description">
                                                <h2>@lang('parts.address')</h2>
                                                <h2>@lang('parts.fax'): 8 (727) 383-17-15</h2>
                                                <h2>Email: info@fishrpc.kz</h2>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 mb-4">
                                            <div class="portfolio-description">
                                                <iframe
                                                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5809.477185667892!2d76.94651!3d43.277852!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836c24436e4c47%3A0xf19d4979104d6276!2z0L_RgNC-0YHQvy4g0KHRg9GO0L3QsdCw0Y8gODksINCQ0LvQvNCw0YLRiyAwNTAwNDY!5e0!3m2!1sru!2skz!4v1635872172595!5m2!1sru!2skz"
                                                    width="600" height="450" style="border:0;" allowfullscreen=""
                                                    loading="lazy"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSeven">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseSeven" aria-expanded="false"
                                            aria-controls="collapseSeven">
                                        @lang('testing.head')
                                    </button>
                                </h2>
                                <div id="collapseSeven" class="accordion-collapse collapse"
                                     aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('testing.manager')
                                                        : </strong>@lang('testing.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 383-15-06</li>
                                                <li><strong>Email: </strong>aitkaliyeva@fishrpc.kz</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingEight">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseEight" aria-expanded="false"
                                            aria-controls="collapseEight">
                                        @lang('parts.labs')
                                    </button>
                                </h2>
                                <div id="collapseEight" class="accordion-collapse collapse"
                                     aria-labelledby="headingEight" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('ichthyology.manager')
                                                        : </strong>@lang('ichthyology.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8(727) 383-17-18</li>
                                                <li><strong>Email: </strong>sansyzbayev@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('hydrobiology.manager')
                                                        : </strong>@lang('hydrobiology.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 383-15-03</li>
                                                <li><strong>Email: </strong>mazhibayeva@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('aquaculture.manager')
                                                        : </strong>@lang('aquaculture.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 390-95-65</li>
                                                <li><strong>Email: </strong>abilov@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('genetics.manager')
                                                        : </strong>@lang('genetics.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 220-98-13</li>
                                                <li><strong>Email: </strong>shalgimbayeva@fishrpc.kz</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingNine">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseNine" aria-expanded="false"
                                            aria-controls="collapseNine">
                                        @lang('parts.deps')
                                    </button>
                                </h2>
                                <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine"
                                     data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('financial.manager')
                                                        : </strong>@lang('financial.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 390-95-74</li>
                                                <li><strong>Email: </strong>uldakhanova@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('innovation.manager')
                                                        : </strong>@lang('innovation.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 383-15-06</li>
                                                <li><strong>Email: </strong>bolatbekova@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('administrative.manager')
                                                        : </strong>@lang('administrative.managername')</li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 390-95-81</li>
                                                <li><strong>Email: </strong>jumakanov@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('legal.manager'): </strong>@lang('legal.managername')
                                                </li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 383-15-02</li>
                                                <li><strong>Email: </strong>lawyerniirh@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('parts.manager'): </strong>@lang('parts.managername')
                                                </li>
                                                <li><strong>@lang('head.phone'): </strong>8 (727) 383-17-16</li>
                                                <li><strong>Email: </strong>abdumalikova@fishrpc.kz</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <div class="accordion mb-5" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                            aria-expanded="false" aria-controls="collapseOne">
                                        @lang('atyrau.head')
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse "
                                     aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="col-lg-12">
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('contact.seo'): </strong>Кадимов Ерболат Латифович</li>
                                                    <li><strong>@lang('contact.priemnaya'): </strong>8 (712-2) 36-69-30</li>
                                                    <li><strong>Email: </strong>atyrau@fishrpc.kz</li>
                                                    <li><strong>@lang('contact.address'): </strong>Ул. Бергалиева, 80. г. Атырау</li>
                                                    <li><strong>@lang('contact.index'): </strong>060027</li>
                                                </ul>
                                            </div>
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('contact.buh'): </strong>Байдербесова Гульнар</li>
                                                    <li><strong>@lang('contact.phone'): </strong>8 (712-2) 36-69-58</li>
                                                </ul>
                                            </div>
                                            <div class="portfolio-info my-4">
                                                <ul>
                                                    <li><strong>@lang('contact.zav'): </strong>Камиева Тныштык Наурызгалиевна
                                                    </li>
                                                    <li><strong>@lang('contact.phone'): </strong>8 (712-2) 36-69-30</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                        @lang('aral.head')
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse"
                                     aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.seo'): </strong>Баракбаев Тынысбек Темирханович</li>
                                                <li><strong>@lang('contact.priemnaya'): </strong>8(7242) 23-02-04, 40-09-17</li>
                                                <li><strong>Бух.тел: </strong>8(7242)23-02-06, 40-09-21</li>
                                                <li><strong>Email: </strong>aral@fishrpc.kz</li>
                                                <li><strong>@lang('contact.address'): </strong>ул. Желтоксан 46/48, г.Кызылорда</li>
                                                <li><strong>@lang('contact.index'): </strong>120014</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.buh'): </strong>Куттымуратова Алмагуль
                                                    Амитовна
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(7242)-23-02-06</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zavaral'): </strong>Самбаев
                                                    Нурлан Серикбаевич
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(72433)2-18-40</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTree">
                                    <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseTree"
                                            aria-expanded="false" aria-controls="collapseTree">
                                        @lang('west.head')
                                    </button>
                                </h2>
                                <div id="collapseTree" class="accordion-collapse collapse"
                                     aria-labelledby="headingTree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.seo'): </strong>Туменов Артур Насибуллаулы</li>
                                                <li><strong>@lang('contact.priemnaya'): </strong>8(7112) 31-05-29</li>
                                                <li><strong>Бух.тел: </strong>8(7112) 31-05-29</li>
                                                <li><strong>Email: </strong>uralsk@fishrpc.kz</li>
                                                <li><strong>@lang('contact.address'): </strong>ул. Жангир хана,45.зд. бакалавриата,
                                                    г.Уральск
                                                </li>
                                                <li><strong>@lang('contact.index'): </strong>090009</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.buh'): </strong>Хайрушева Анжела
                                                    Кумаровна
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(7112) 31-05-29</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav'): </strong>Ким Аркадий Игнатьевич
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(7112) 31-05-29</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingFour">
                                    <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                        @lang('altai.head')
                                    </button>
                                </h2>
                                <div id="collapseFour" class="accordion-collapse collapse"
                                     aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.seo'): </strong>Аубакиров Бауржан Саветович</li>
                                                <li><strong>@lang('contact.priemnaya'): </strong>8(7232)21-13-41</li>
                                                <li><strong>Бух.тел: </strong>8(7232)51-07-45</li>
                                                <li><strong>Email: </strong>altay@fishrpc.kz</li>
                                                <li><strong>@lang('contact.address'): </strong>ул.Протазанова,83. г.
                                                    Усть-Каменогорск
                                                </li>
                                                <li><strong>@lang('contact.index'): </strong>070004</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.buh'): </strong>Жамантикова Гульфайроз
                                                    Мамашевна
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(7232)51-07-45</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav_altay_lab'): </strong>Касымханов Айбек Махамбетович
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(7232)52-71-10</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav_altay_strong'): </strong>Кабдолов
                                                    Жаркын Русланович
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8-707-195-57-53</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingFive">
                                    <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                            aria-expanded="false" aria-controls="collapseFive">
                                        @lang('balkhash.head')
                                    </button>
                                </h2>
                                <div id="collapseFive" class="accordion-collapse collapse"
                                     aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.seo'): </strong>Куматаев Еркынбек Ерикович</li>
                                                <li><strong>@lang('contact.priemnaya'): </strong>8(71036) 4-14-74</li>
                                                <li><strong>Бух.тел: </strong>8(71036) 4-14-74</li>
                                                <li><strong>Email: </strong>balkhash@fishrpc.kz</li>
                                                <li><strong>@lang('contact.address'): </strong>Ул.Желтоксана 20, г.Балхаш</li>
                                                <li><strong>@lang('contact.index'): </strong>100300</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.buh'): </strong>Жаумбаева Гульсим
                                                    Абиловна
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8 (710-36) 4-14-74</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav_altay_lab'): </strong>Шарипова Ольга Александровна
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8(71036)4-14-74</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSix">
                                    <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                        @lang('north.head')
                                    </button>
                                </h2>
                                <div id="collapseSix" class="accordion-collapse collapse"
                                     aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.seo'): </strong>Шуткараев Азис Васильевич</li>
                                                <li><strong>@lang('contact.priemnaya'): </strong>8(7172) 50-10-79</li>
                                                <li><strong>Бух.тел: </strong>8(7172) 57-63-71</li>
                                                <li><strong>Email: </strong>astana@fishrpc.kz</li>
                                                <li><strong>@lang('contact.address'): </strong>ул.Кенесары 43, кабинет 36,
                                                    г.Нур-Султан
                                                </li>
                                                <li><strong>@lang('contact.index'): </strong>010000</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.buh'): </strong>Бексултанова Гульнара
                                                    Кайнуллиновна
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8 (7172) 57-63-71</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav_petropavl_strong'): </strong>Фефелов Виктор Владимироваич
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8 705-650-15-65</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav'): </strong>Куржыкаев Жумагазы
                                                    Кузембаевич
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8-701-366-47-60</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav_karagenda_strong'): </strong>Крайнюк
                                                    Владимир Николаевич
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8-707-671-37-71</li>
                                            </ul>
                                        </div>
                                        <div class="portfolio-info my-4">
                                            <ul>
                                                <li><strong>@lang('contact.zav_kostanay_strong'): </strong>Попов
                                                    Владимир Анатольевич
                                                </li>
                                                <li><strong>@lang('contact.phone'): </strong>8-702-857-61-50</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Portfolio Details Section -->
@endsection
