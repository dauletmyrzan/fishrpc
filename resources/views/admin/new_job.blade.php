@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новая вакансия</h2>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                	<form action="{{ route('job.add') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                    <div class="col-md-6">
                		<div class="form-group">
                			<label for="position">Позиция</label>
	                		<input type="text" id="position" class="form-control" name="position" required>
                		</div>
                		<div class="form-group">
                			<label for="requirements">Требования</label>
	                		<textarea id="requirements" class="form-control" name="requirements" required></textarea>
                		</div>
                        <div class="form-group">
                            <label for="schedule">График работы</label>
                            <input type="text" id="schedule" class="form-control" name="schedule" required>
                        </div>
                        <div class="form-group">
                            <label for="conditions">Условия</label>
                            <input type="text" id="conditions" class="form-control" name="conditions">
                        </div>
                		<div class="form-group">
                			<label for="wage">Зар.плата</label>
	                		<input type="text" id="wage" class="form-control" name="wage" required>
                		</div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="position_en">Position</label>
                            <input type="text" id="position_en" class="form-control" name="position_en" required>
                        </div>
                        <div class="form-group">
                            <label for="requirements_en">Requirements</label>
                            <textarea id="requirements_en" class="form-control" name="requirements_en" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="schedule_en">Schedule</label>
                            <input type="text" id="schedule_en" class="form-control" name="schedule_en" required>
                        </div>
                        <div class="form-group">
                            <label for="conditions_en">Conditions</label>
                            <input type="text" id="conditions_en" class="form-control" name="conditions_en">
                        </div>
                        <div class="form-group">
                            <label for="wage_en">Wage</label>
                            <input type="text" id="wage_en" class="form-control" name="wage_en" required>
                        </div>
                    </div>
                    <div class="col-md-12">
						<button class="mt-3 btn btn-success">Добавить</button>
						<a href="{{ route('admin.vacancies') }}" class="mt-3 btn btn-primary">Отмена</a>
                    </div>
                	</form>
                </div>
            </div>
        </div>
    </div>
@endsection
