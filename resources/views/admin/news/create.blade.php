@extends('layouts.admin2')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<a href="/admin/news" class="btn btn-primary mb-3">Назад</a>
            <h2 class="mb-3">Создание новости</h2>
			<form action="{{ route('admin.news.store') }}" method="POST" enctype="multipart/form-data">
				@csrf
                <div class="mb-5">
                    <label for="picture">Загрузите картинку</label>
                    <input type="file" class="form-control" name="picture" id="picture" required>
                </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" data-bs-toggle="tab"
                                data-bs-target="#home" type="button" role="tab" aria-controls="home"
                                aria-selected="true">Русский язык</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile"
                                type="button" role="tab" aria-controls="profile" aria-selected="false">Казахский
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact"
                                type="button" role="tab" aria-controls="contact" aria-selected="false">
                            Английский
                        </button>
                    </li>
                </ul>
				<div class="p-4">
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="form-group">
								<label>Заголовок:</label>
								<input type="text" class="form-control" name="title" required>
							</div>
							<div class="form-group">
								<label>Описание:</label>
								<textarea class="form-control" style="min-height: 100px;" name="description" required></textarea>
							</div>
							<div class="form-group">
								<label>Детали:</label>
								<textarea class="form-control summernote" style="min-height: 150px;" name="details" required></textarea>
							</div>
						</div>
						<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<input type="hidden" name="id">
							<div class="form-group">
								<label>Title:</label>
								<input type="text" class="form-control" name="title_en" required>
							</div>
							<div class="form-group">
								<label>Description:</label>
								<textarea class="form-control" style="min-height: 100px;" name="description_en" required></textarea>
							</div>
							<div class="form-group">
								<label>Details:</label>
								<textarea class="form-control summernote" style="min-height: 150px;" name="details_en" required></textarea>
							</div>
						</div>
						<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
							<input type="hidden" name="id">
							<div class="form-group">
								<label>Title:</label>
								<input type="text" class="form-control" name="title_kz" required>
							</div>
							<div class="form-group">
								<label>Description:</label>
								<textarea class="form-control" style="min-height: 100px;" name="description_kz" required></textarea>
							</div>
							<div class="form-group">
								<label>Details:</label>
								<textarea class="form-control summernote" style="min-height: 150px;" name="details_kz" required></textarea>
							</div>
						</div>
					</div>
					<button class="btn btn-success btn-lg my-5">Создать новость</button>
				</div>
			</form>
		</div>
	</div>
@endsection
