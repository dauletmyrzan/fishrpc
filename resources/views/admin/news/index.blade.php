@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новости</h2>
            <a href="{{ route('admin.news.create') }}" class="btn btn-primary">Добавить новость</a><br><br>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th style="width:40px;">#</th>
                                <th style="width:25%;">Заголовок</th>
								<th></th>
                            </thead>
                            <tbody>
                                @foreach($news as $key => $post)
                                <tr class="item-row">
                                    <td class="item-id">{{ $key + $news->firstItem() }}</td>
                                    <td class="item-name">{{ $post->title }}</td>
									<td>
                                        <a href="/admin/news/{{ $post->id }}" class="btn btn-success">Изменить</a>
                                        <form action="{{ route('admin.news.delete') }}" method="post" onsubmit="return confirm('Вы действительно хотите удалить?');">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $post->id }}">
                                            <button class="mt-3 btn btn-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="card-footer">
					{{ $news->links() }}
				</div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("select[name='category_id']").on("change", function(){
			window.location.href = $.query.SET('category', $(this).val());
        });
    });
</script>
@endsection
