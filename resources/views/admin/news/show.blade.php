@extends('layouts.admin2')

@section('content')
	<div class="row">
        <div class="col-md-12">
			<a href="/admin/news" class="btn btn-primary mb-3">Назад</a>
			<h3>Новость #{{ $post->id }}</h3>

			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Русский язык</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">English</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Қазақша</a>
				</li>
			</ul>
			<form action="{{ route('admin.news.update') }}" method="POST">
				@csrf
				<div class="p-4">
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<input type="hidden" name="id" value="{{ $post->id }}">
							<div class="form-group">
								<label>Заголовок:</label>
								<input type="text" class="form-control" name="title" value="{{ $post->title }}" required>
							</div>
							<div class="form-group">
								<label>Описание:</label>
								<textarea class="form-control" style="min-height: 100px;" name="description" required>{!! $post->description !!}</textarea>
							</div>
							<div class="form-group">
								<label>Детали:</label>
								<textarea class="form-control summernote" style="min-height: 150px;" name="details" required>{!! $post->details !!}</textarea>
							</div>
						</div>
						<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<input type="hidden" name="id" value="{{ $post->id }}">
							<div class="form-group">
								<label>Title:</label>
								<input type="text" class="form-control" name="title_en" value="{{ $post->title_en }}" required>
							</div>
							<div class="form-group">
								<label>Description:</label>
								<textarea class="form-control" style="min-height: 100px;" name="description_en" required>{!! $post->description_en !!}</textarea>
							</div>
							<div class="form-group">
								<label>Details:</label>
								<textarea class="form-control summernote" style="min-height: 150px;" name="details_en" required>{!! $post->details_en !!}</textarea>
							</div>
						</div>
						<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
							<input type="hidden" name="id" value="{{ $post->id }}">
							<div class="form-group">
								<label>Title:</label>
								<input type="text" class="form-control" name="title_kz" value="{{ $post->title_kz }}" required>
							</div>
							<div class="form-group">
								<label>Description:</label>
								<textarea class="form-control" style="min-height: 100px;" name="description_kz" required>{!! $post->description_kz !!}</textarea>
							</div>
							<div class="form-group">
								<label>Details:</label>
								<textarea class="form-control summernote" style="min-height: 150px;" name="details_kz" required>{!! $post->details_kz !!}</textarea>
							</div>
						</div>
					</div>
					<button class="btn btn-success btn-lg my-5">Сохранить новость</button>
				</div>
			</form>
        </div>
		<hr>
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-danger">Опасная зона</h2>
                </div>
                <div class="card-body">
                    <p>Внимание! Удалив новость, вы уже не сможете его восстановить.</p>
                    <form action="{{ route('news.delete') }}" method="POST" id="remove_news_form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $post->id }}">
                        <button class="btn btn-danger">Удалить новость</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<!-- <script type="text/javascript">
	$(document).ready(function(){
        $("#remove_item_form").find("button").on("click", function(){
            if(confirm('Вы действительно хотите удалить товар?')){
                $("#remove_item_form").submit();
            }
            return false;
        });
	});
</script> -->
@endsection
