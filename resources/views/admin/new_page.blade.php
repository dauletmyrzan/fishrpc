@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новая страница</h2>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('page.create') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Родитель</label>
                            <select name="parent" class="form-select" aria-label="Default select example">
                                <option selected>...</option>
                                <option value="departments">Отдел</option>
                                <option value="branches">Филиал</option>
                                <option value="laboratories">Лаборатории</option>
                            </select>
                        </div>
                        <div>
                            <label for="pictures">Загрузите картинки</label>
                            <input type="file" class="form-control" name="pictures[]" id="pictures">
                            <p class="text-muted my-2" style="font-size: 9pt;"><span
                                    class="nc-icon nc-alert-circle-i"></span> Для сохранения эстетичности сайта
                                загрузите квадратные изображения.</p>
                        </div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                        data-bs-target="#home" type="button" role="tab" aria-controls="home"
                                        aria-selected="true">Русский
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile"
                                        type="button" role="tab" aria-controls="profile" aria-selected="false">Казахский
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false">
                                    Английский
                                </button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Название</label>
                                        <input type="text" id="title" class="form-control" name="name_ru" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Детали</label>
                                        <textarea id="details" class="form-control" name="content_ru"
                                                  required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title">Название</label>
                                            <input type="text" id="title" class="form-control" name="name_kz" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Детали</label>
                                            <textarea id="details" class="form-control" name="content_kz"
                                                      required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title">Название</label>
                                            <input type="text" id="title" class="form-control" name="name_en" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Детали</label>
                                            <textarea id="details" class="form-control" name="content_en"
                                                      required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="mt-3 btn btn-success">Добавить</button>
                        <a href="{{ route('admin.news.index') }}" class="mt-3 btn btn-primary">Отмена</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
