@extends('layouts.admin2')

@section('content')
    <style>
        .news-img {
            width: 250px;
            height: 150px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
    <h2 class="my-1">Фотоотзывы</h2>
    <div class="row">
        <div class="col-md-12 my-3">
            <div class="card">
                <div class="card-body">
                	<form action="{{ route('admin.reviews.store') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                        <div>
                            <label for="pictures">Загрузите картинки</label>
                            <input type="file" class="form-control" name="pictures[]" id="pictures" required multiple>
                            <p class="text-muted my-2" style="font-size: 9pt;"><span class="nc-icon nc-alert-circle-i"></span> Для сохранения эстетичности сайта загрузите квадратные изображения.</p>
                        </div>
                        <button class="mt-3 btn btn-success">Добавить</button>
                	</form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th>Картинка</th>
                                <th>Дата загрузки</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach($reviews as $image)
                                <tr class="item-row cursor-pointer" data-id="{{ $image->id }}">
                                    <td class="item-pic">
                                        <a href="{{ asset('images/reviews/' . $image->img_src) }}" data-gallery="portfolioGallery" class="portfolio-lightbox link-preview">
                                            <div class="news-img" style="background-image:url({{ asset('images/reviews/' . $image->img_src) }})"></div>
                                        </a>
                                    </td>
                                    <td class="item-name">{{ $image->created_at }}</td>
                                    <td class="item-name">
                                        <form action="{{ route('admin.reviews.delete') }}" method="post" onsubmit="return confirm('Вы действительно хотите удалить?');">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $image->id }}">
                                            <button class="mt-3 btn btn-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
