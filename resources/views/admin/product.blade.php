@extends('layouts.admin2')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <h2></h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>{{ $product->name }}</h3>
                        	<form action="{{ route('update_product') }}" method="POST">
                                @csrf
                                <div class="col-md-6">
                                    <h3>Рус</h3>
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <div class="form-group">
                                        <label>Название:</label>
                                        <input type="text" class="form-control" name="name" value="{{ $product->name }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Формула:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="formula" required>{!! $product->formula !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Свойства:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="properties" required>{!! $product->properties !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Внешний вид:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="appearance" required>{!! $product->appearance !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Применение:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="application" required>{!! $product->application !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Рекомендации:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="recommendations" required>{!! $product->recommendations !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Нормы применения:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="rate" required>{!! $product->rate !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Упаковка и хранение:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="packaging" required>{!! $product->packaging !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Eng</h3>
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="text" class="form-control" name="name_en" value="{{ $product->name_en }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Formula:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="formula_en" required>{!! $product->formula_en !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Properties:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="properties_en" required>{!! $product->properties_en !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Appearance:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="appearance_en" required>{!! $product->appearance_en !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Application:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="application_en" required>{!! $product->application_en !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Recommendations:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="recommendations_en" required>{!! $product->recommendations_en !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Rate:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="rate_en" required>{!! $product->rate_en !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Packaging and storage:</label>
                                        <textarea class="form-control" style="min-height: 150px;" name="packaging_en" required>{!! $product->packaging_en !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                        <label>Имя картинки:</label>
                                        <input type="text" class="form-control" name="img_src" value="{{ $product->img_src }}" required>
                                    </div>
                                </div>
                                <button class="btn btn-success" style="margin-left: 15px;">Применить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-danger">Опасная зона</h2>
                </div>
                <div class="card-body">
                    <p>Внимание! Удалив продукт, вы уже не сможете его восстановить.</p>
                    <form action="{{ route('product.delete') }}" method="POST" id="remove_news_form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $product->id }}">
                        <button class="btn btn-danger">Удалить продукт</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<!-- <script type="text/javascript">
	$(document).ready(function(){
        $("#remove_item_form").find("button").on("click", function(){
            if(confirm('Вы действительно хотите удалить товар?')){
                $("#remove_item_form").submit();
            }
            return false;
        });
	});
</script> -->
@endsection
