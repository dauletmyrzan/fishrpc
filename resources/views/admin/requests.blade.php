@extends('layouts.admin2')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <h2>Заявки</h2>
            <div class="card">
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Id</th>
                                <th>Имя</th>
                                <th>Телефон</th>
                                <th>Email</th>
                                <th>Сообщение</th>
                            </thead>
                            <tbody>
                            	@foreach($requests as $request)
                                <tr>
                                	<td>{{ $request->id }}</td>
                                    <td>{{ $request->name }}</td>
                                    <td>{{ $request->phone }}</td>
                                    <td>{{ $request->email }}</td>
                                    <td>{{ $request->message }}</td>
                                </tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("select[name='status_id']").on("change", function(){
            var query = $.query.SET('status', $(this).val());
            window.location.href = query;
        });
    });
</script>
@endsection
