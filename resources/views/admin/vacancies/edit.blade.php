@extends('layouts.admin2')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.vacancies.index') }}" class="btn btn-primary mb-3">Назад</a>
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.vacancies.update') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ $vacancy->id }}">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" data-bs-toggle="tab"
                                        data-bs-target="#home" type="button" role="tab" aria-controls="home"
                                        aria-selected="true">Русский язык</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile"
                                        type="button" role="tab" aria-controls="profile" aria-selected="false">Казахский
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false">
                                    Английский
                                </button>
                            </li>
                        </ul>
                        <div class="p-4">
                            <div class="pb-4">
                                <div class="form-group">
                                    <label>График работы: *</label>
                                    <input type="text" class="form-control" name="schedule" required value="{{ $vacancy->schedule }}">
                                </div>
                                <div class="form-group">
                                    <label>Заработная плата: *</label>
                                    <input type="text" class="form-control" name="wage" required value="{{ $vacancy->wage }}">
                                </div>
                                <div class="form-group">
                                    <label for="branch_code">Филиал: *</label>
                                    <select name="branch_code" id="branch_code" class="form-control" required>
                                        <option selected disabled value="">Выберите филиал</option>
                                        @foreach ($branches as $branch)
                                            <option {{ $branch->code === $vacancy->branch_code ? 'selected' : '' }}
                                                    value="{{ $branch->code }}">{{ $branch->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="file">Файл</label>
                                    <input type="file" name="file" class="form-control form-control-file">
                                    <div class="mt-3">
                                        @if ($vacancy->file)
                                            <a href="/files/{{ $vacancy->file }}" target="_blank">Скачать файл</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="form-group">
                                        <label>Позиция:</label>
                                        <input type="text" class="form-control" name="position_ru" value="{{ $vacancy->position_ru }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Требования:</label>
                                        <textarea class="summernote" name="requirements_ru" id="requirements_ru" required>{!! urldecode($vacancy->requirements_ru) !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Обязанности:</label>
                                        <textarea class="summernote" name="duties_ru" id="duties_ru">{!! urldecode($vacancy->duties_ru) !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Условия:</label>
                                        <textarea class="summernote" name="conditions_ru" id="conditions_ru">{!! urldecode($vacancy->conditions_ru) !!}</textarea>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="form-group">
                                        <label>Position:</label>
                                        <input type="text" class="form-control" name="position_en" value="{{ $vacancy->position_en }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Requirements:</label>
                                        <textarea class="summernote" name="requirements_en" id="requirements_en" required>{!! urldecode($vacancy->requirements_en) !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Duties:</label>
                                        <textarea class="summernote" name="duties_en" id="duties_en">{!! urldecode($vacancy->duties_en) !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Conditions:</label>
                                        <textarea class="summernote" name="conditions_en" id="conditions_en">{!! urldecode($vacancy->conditions_en) !!}</textarea>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="form-group">
                                        <label>Лауазымы:</label>
                                        <input type="text" class="form-control" name="position_kz" value="{{ $vacancy->position_kz }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Талаптар:</label>
                                        <textarea class="summernote" name="requirements_kz" id="requirements_kz" required>{!! urldecode($vacancy->requirements_kz) !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Міндеттер:</label>
                                        <textarea class="summernote" name="duties_kz" id="duties_kz">{!! urldecode($vacancy->duties_kz) !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Еңбек жағдайы:</label>
                                        <textarea class="summernote" name="conditions_kz" id="conditions_kz">{!! urldecode($vacancy->conditions_kz) !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-lg">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-danger">Опасная зона</h2>
                </div>
                <div class="card-body">
                    <p>Внимание! Удалив вакансию, вы уже не сможете его восстановить.</p>
                    <form action="{{ route('admin.vacancies.delete') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $vacancy->id }}">
                        <button class="btn btn-danger">Удалить вакансию</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
