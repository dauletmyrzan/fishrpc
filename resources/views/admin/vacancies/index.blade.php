@extends('layouts.admin2')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Вакансии</h2>
            <a href="{{ route('admin.vacancies.create') }}" class="btn btn-primary">Добавить вакансию</a><br><br>
            <div class="card">
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                            	<th style="width: 40px;">ID</th>
                                <th>Позиция</th>
                                <th>Зар.плата</th>
                                <th></th>
                            </thead>
                            <tbody>
                            	@foreach($vacancies as $vacancy)
								<tr class="item-row cursor-pointer" data-id="{{ $vacancy->id }}" onclick="window.location.href='/admin/vacancies/edit/{{$vacancy->id}}'">
									<td class="item-id">{{ $vacancy->id }}</td>
                                    <td class="item-name">{{ $vacancy->positionWithLang }}</td>
                                    <td class="item-code">{{ $vacancy->wage }}</td>
                                    <td>
                                        <a href="/admin/vacancies/edit/{{ $vacancy->id }}" class="btn btn-success">Изменить</a>
                                    </td>
								</tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$("select[name='category_id']").on("change", function(){
            var query = $.query.SET('category', $(this).val());
            window.location.href = query;
        });
	});
</script>
@endsection
