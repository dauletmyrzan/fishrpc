@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.vacancies.index') }}" class="btn btn-primary mb-3">Назад</a>
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.vacancies.store') }}" method="POST">
                        @csrf
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" data-bs-toggle="tab"
                                        data-bs-target="#home" type="button" role="tab" aria-controls="home"
                                        aria-selected="true">Русский язык</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile"
                                        type="button" role="tab" aria-controls="profile" aria-selected="false">Казахский
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false">
                                    Английский
                                </button>
                            </li>
                        </ul>
                        <div class="p-4">
                            <div class="tab-content" id="myTabContent">
                                <div class="pb-4">
                                    <div class="form-group">
                                        <label>График работы: *</label>
                                        <input type="text" class="form-control" name="schedule" required value="Пн-Пт, 9:00-18:00">
                                    </div>
                                    <div class="form-group">
                                        <label>Заработная плата: *</label>
                                        <input type="text" class="form-control" name="wage" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="branch_code">Филиал: *</label>
                                        <select name="branch_code" id="branch_code" class="form-control" required>
                                            <option selected disabled value="">Выберите филиал</option>
                                            @foreach ($branches as $branch)
                                            <option value="{{ $branch->code }}">{{ $branch->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="form-group">
                                        <label>Позиция: *</label>
                                        <input type="text" class="form-control" name="position_ru" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Требования: *</label>
                                        <textarea class="summernote" name="requirements_ru" id="requirements_ru" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Обязанности:</label>
                                        <textarea class="summernote" name="duties_ru" id="duties_ru"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Условия:</label>
                                        <textarea class="summernote" name="conditions_ru" id="conditions_ru"></textarea>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="form-group">
                                        <label>Position: *</label>
                                        <input type="text" class="form-control" name="position_en" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Requirements: *</label>
                                        <textarea class="summernote" name="requirements_en" id="requirements_en" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Duties:</label>
                                        <textarea class="summernote" name="duties_en" id="duties_en"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Conditions:</label>
                                        <textarea class="summernote" name="conditions_en" id="conditions_en"></textarea>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="form-group">
                                        <label>Лауазымы: *</label>
                                        <input type="text" class="form-control" name="position_kz" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Талаптар: *</label>
                                        <textarea class="summernote" name="requirements_kz" id="requirements_kz" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Міндеттер:</label>
                                        <textarea class="summernote" name="duties_kz" id="duties_kz"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Еңбек жағдайы:</label>
                                        <textarea class="summernote" name="conditions_kz" id="conditions_kz"></textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-lg">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
