@extends('layouts.admin2')

@section('content')
    <style>
        ul {
            list-style-type: none;
        }
    </style>
    <div class="container-fluid">
        <h1>Головное подразделение</h1>
        <div class="card">
            <div class="card-body">
                <div class="row gy-4">
                    <div class="col-lg-12">
                        <div class="portfolio-description">
                            <div class="accordion mb-5" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingSix">
                                        <button class="accordion-button collapsed colored" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                                aria-expanded="false" aria-controls="collapseSix">
                                            @lang('head.management')
                                        </button>
                                    </h2>
                                    <div id="collapseSix" class="accordion-collapse collapse " aria-labelledby="headingSix"
                                         data-bs-parent="#accordionExample">
                                        <div class="accordion-body row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4 text-center" data-aos="fade-down">
                                                <img class="img-fluid w-50 mt-5" src="{{ asset('vendor/img/director.jpg') }}">
                                                <div class="form-group my-2">
                                                    <form action="" enctype="multipart/form-data" method="POST" class="form-inline">
                                                        @csrf
                                                        <input type="file" class="form-control form-control-file" required>
                                                        <button class="btn ms-2 btn-success">Загрузить</button>
                                                    </form>
                                                </div>
                                                <div class="portfolio-info">
                                                    <ul>
                                                        <li>
                                                            <strong>@lang('head.director')</strong><br>@lang('head.directorname')
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4 text-center" data-aos="fade-down">
                                                <img class="img-fluid w-50 mt-5" src="{{ asset('vendor/img/sekretar.jpg') }}">
                                                <div class="form-group my-2">
                                                    <form action="" enctype="multipart/form-data" method="POST" class="form-inline">
                                                        @csrf
                                                        <input type="file" class="form-control form-control-file" required>
                                                        <button class="btn ms-2 btn-success">Загрузить</button>
                                                    </form>
                                                </div>
                                                <div class="portfolio-info">
                                                    <ul>
                                                        <li>
                                                            <strong>@lang('head.sekretar')</strong><br>@lang('head.sekretarname')
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 text-center" data-aos="fade-down">
                                                <img class="img-fluid w-50 mt-5" src="{{ asset('vendor/img/zamdirector.jpg') }}">
                                                <div class="form-group my-2">
                                                    <form action="" enctype="multipart/form-data" method="POST" class="form-inline">
                                                        @csrf
                                                        <input type="file" class="form-control form-control-file" required>
                                                        <button class="btn ms-2 btn-success">Загрузить</button>
                                                    </form>
                                                </div>
                                                <div class="portfolio-info">
                                                    <ul>
                                                        <li>
                                                            <strong>@lang('head.zamdirector')</strong><br>@lang('head.zamdirectorname')
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 text-center" data-aos="fade-down">
                                                <img class="img-fluid w-50 mt-5"
                                                     src="{{ asset('vendor/img/sovetnik-director.jpg') }}">
                                                <div class="form-group my-2">
                                                    <form action="" enctype="multipart/form-data" method="POST" class="form-inline">
                                                        @csrf
                                                        <input type="file" class="form-control form-control-file" required>
                                                        <button class="btn ms-2 btn-success">Загрузить</button>
                                                    </form>
                                                </div>
                                                <div class="portfolio-info">
                                                    <ul>
                                                        <li>
                                                            <strong>@lang('head.sovetnik')</strong><br>@lang('head.sovetnikname')
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingSeven">
                                        <button class="accordion-button collapsed colored" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseSeven"
                                                aria-expanded="false" aria-controls="collapseSeven">
                                            @lang('head.history')
                                        </button>
                                    </h2>
                                    <div id="collapseSeven" class="accordion-collapse collapse"
                                         aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <textarea name="history" id="history" class="summernote" required>@lang('head.historytext')</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingEight">
                                        <button class="accordion-button collapsed colored" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseEight"
                                                aria-expanded="false" aria-controls="collapseEight">
                                            @lang('head.mission')
                                        </button>
                                    </h2>
                                    <div id="collapseEight" class="accordion-collapse collapse"
                                         aria-labelledby="headingEight" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <textarea name="mission" id="mission" class="summernote" required>@lang('head.missiontext')</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingNine">
                                        <button class="accordion-button collapsed colored" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseNine"
                                                aria-expanded="false" aria-controls="collapseNine">
                                            @lang('head.cooperation')
                                        </button>
                                    </h2>
                                    <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine"
                                         data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <textarea name="cooperation" id="cooperation" class="summernote" required>@lang('head.cooperationtext')</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTen">
                                        <button class="accordion-button collapsed colored" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseTen"
                                                aria-expanded="false" aria-controls="collapseTen">
                                            @lang('head.works')
                                        </button>
                                    </h2>
                                    <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen"
                                         data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <textarea name="works" id="works" class="summernote" required>@lang('head.workstext')</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingEleven">
                                        <button class="accordion-button collapsed colored" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseEleven"
                                                aria-expanded="false" aria-controls="collapseEleven">
                                            @lang('head.research')
                                        </button>
                                    </h2>
                                    <div id="collapseEleven" class="accordion-collapse collapse"
                                         aria-labelledby="headingEleven" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <textarea name="research" id="research" class="summernote" required>@lang('head.researchtext')</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
