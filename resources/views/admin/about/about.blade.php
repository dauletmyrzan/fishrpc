@extends('layouts.admin2')

@section('content')
    <h1>О нас</h1>
    <div class="row">
        <div class="col-md-4">
            <a href="/admin/about/head-office"><div class="card">
                <div class="card-body py-5 text-center">
                    <h4>Головное подразделение</h4>
                </div>
            </div></a>
        </div>
        <div class="col-md-4">
            <a href="/admin/about/information-base"><div class="card">
                <div class="card-body py-5 text-center">
                    <h4>Информационная база</h4>
                </div>
            </div></a>
        </div>
        <div class="col-md-4">
            <a href="/admin/about/achievements"><div class="card">
                <div class="card-body py-5 text-center">
                    <h4>Достижения, гранты МОН</h4>
                </div>
            </div></a>
        </div>
    </div>
@endsection
