@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Продукты</h2>
            <a href="{{ route('admin.new_product') }}" class="btn btn-primary">Добавить продукт</a><br><br>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th style="width: 40px;">ID</th>
                                <th>Название</th>
                                <th>Формула</th>
                                <th>Свойства</th>
                                <th>Внешний вид</th>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr class="item-row cursor-pointer" data-id="{{ $product->id }}" onclick="window.location.href='/admin/product/{{$product->id}}'">
                                    <td class="item-id">{{ $product->id }}</td>
                                    <td class="item-name">{{ $product->name }}</td>
                                    <td class="item-picture">{{ $product->formula }}</td>
                                    <td class="item-picture">{{ $product->properties }}</td>
                                    <td class="item-picture">{{ $product->appearance }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("select[name='category_id']").on("change", function(){
            var query = $.query.SET('category', $(this).val());
            window.location.href = query;
        });
    });
</script>
@endsection
