@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новая новость</h2>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                	<form action="{{ route('news.add') }}" method="POST" enctype="multipart/form-data">
                		@csrf
                        <div class="col-md-6">
                		<div class="form-group">
                			<label for="title">Заголовок</label>
	                		<input type="text" id="title" class="form-control" name="title" required>
                		</div>
                		<div class="form-group">
                			<label for="description">Описание</label>
	                		<textarea id="description" class="form-control" name="description" required></textarea>
                		</div>
                        <div class="form-group">
                            <label for="description">Детали</label>
                            <textarea id="details" class="form-control" name="details" required></textarea>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="title_en">Title</label>
                            <input type="text" id="title_en" class="form-control" name="title_en" required>
                        </div>
                        <div class="form-group">
                            <label for="description_en">Description</label>
                            <textarea id="description_en" class="form-control" name="description_en" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description">Details</label>
                            <textarea id="details_en" class="form-control" name="details_en" required></textarea>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="title_kz">Тақырып</label>
                            <input type="text" id="title_kz" class="form-control" name="title_kz" required>
                        </div>
                        <div class="form-group">
                            <label for="description_kz">Сипаттама</label>
                            <textarea id="description_kz" class="form-control" name="description_kz" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description">Толығырақ</label>
                            <textarea id="details_kz" class="form-control" name="details_kz" required></textarea>
                        </div>
                        </div>
                        <div>
                            <label for="pictures">Загрузите картинки</label>
                            <input type="file" class="form-control" name="pictures[]" id="pictures" required multiple>
                            <p class="text-muted my-2" style="font-size: 9pt;"><span class="nc-icon nc-alert-circle-i"></span> Для сохранения эстетичности сайта загрузите квадратные изображения.</p>
                        </div>
						<button class="mt-3 btn btn-success">Добавить</button>
						<a href="{{ route('admin.news.index') }}" class="mt-3 btn btn-primary">Отмена</a>
                	</form>
                </div>
            </div>
        </div>
    </div>
@endsection
