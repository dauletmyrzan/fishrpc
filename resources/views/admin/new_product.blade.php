@extends('layouts.admin2')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product.add') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-sm-6">
                            <h4>Рус</h4>
                            <div class="form-group">
                                <label for="name">Название</label>
                                <input type="text" id="name" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="formula">Формула</label>
                                <textarea id="formula" class="form-control" name="formula" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="properties">Свойства</label>
                                <textarea id="properties" class="form-control" name="properties" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="appearance">Внешний вид</label>
                                <textarea id="appearance" class="form-control" name="appearance" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="application">Применение</label>
                                <textarea id="application" class="form-control" name="application" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recommendations">Рекомендации</label>
                                <textarea id="recommendations" class="form-control" name="recommendations"
                                          required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="rate">Нормы применения</label>
                                <textarea id="rate" class="form-control" name="rate" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="packaging">Упаковка и хранение</label>
                                <textarea id="packaging" class="form-control" name="packaging" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Eng</h4>
                            <div class="form-group">
                                <label for="name_en">Name</label>
                                <input type="text" id="name_en" class="form-control" name="name_en" required>
                            </div>
                            <div class="form-group">
                                <label for="formula_en">Formula</label>
                                <textarea id="formula_en" class="form-control" name="formula_en" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="properties_en">Properties</label>
                                <textarea id="properties_en" class="form-control" name="properties_en"
                                          required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="appearance_en">Appearance</label>
                                <textarea id="appearance_en" class="form-control" name="appearance_en"
                                          required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="application_en">Application</label>
                                <textarea id="application_en" class="form-control" name="application_en"
                                          required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="recommendations_en">Recommendations</label>
                                <textarea id="recommendations_en" class="form-control" name="recommendations_en"
                                          required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="rate_en">Rate</label>
                                <textarea id="rate_en" class="form-control" name="rate_en" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="packaging_en">Packaging and storage:</label>
                                <textarea id="packaging_en" class="form-control" name="packaging_en"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px; margin-bottom: 20px;">
                            <div class="form-group">
                                <label for="img_src">Картинка</label>
                                <input type="text" id="img_src" class="form-control" name="img_src" required>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px; margin-bottom: 50px;">
                            <button class="mt-3 btn btn-success">Добавить</button>
                            <a href="{{ route('admin.news.index') }}" class="mt-3 btn btn-primary">Отмена</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
