@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="mt-1 mb-3">Страницы</h2>
            <a href="{{ route('admin.page.create') }}?category=lab" class="btn btn-primary">Добавить новую лабораторию</a>
            <a href="{{ route('admin.page.create') }}?category=branch" class="btn btn-primary">Добавить новый филиал</a>
            <a href="{{ route('admin.page.create') }}?category=department" class="btn btn-primary">Добавить новый отдел</a><br><br>
{{--            <a href="/admin/about" class="btn btn-success">Страница "О нас"</a>--}}
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" style="min-height:300px;">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th style="width: 40px;">ID</th>
                                <th>Категория</th>
                                <th>Название</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach($pages as $page)
                                <tr class="item-row cursor-pointer">
                                    <td class="item-id">{{ $page->id }}</td>
                                    <td class="item-name">{{ $page->category->name }}</td>
                                    <td class="item-name">{{ $page->name_ru }}</td>
                                    <td>
                                        <a href="/admin/pages/{{ $page->id }}" class="btn btn-success w-50">Изменить</a>
                                        <form action="/admin/pages/delete" method="post" class="mt-2">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $page->id }}">
                                            <button class="btn btn-danger w-50">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
