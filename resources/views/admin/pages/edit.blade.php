@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="/admin/pages" class="btn btn-primary mb-3">Назад</a>
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.page.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $page->id }}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="title">Раздел на сайте:</label>
                                    <select name="category_id" class="form-select">
                                        @foreach ($categories as $category)
                                            <option {{ $category->id === $page->category_id ? 'selected' : '' }}
                                                    value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                        data-bs-target="#home" type="button" role="tab" aria-controls="home"
                                        aria-selected="true">Русский
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile"
                                        type="button" role="tab" aria-controls="profile" aria-selected="false">Казахский
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact"
                                        type="button" role="tab" aria-controls="contact" aria-selected="false">
                                    Английский
                                </button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Название</label>
                                        <input type="text" id="title" class="form-control" name="name_ru" required
                                               value="{{ $page->name_ru }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Детали</label>
                                        <textarea id="details" class="form-control summernote" name="content_ru" required
                                                  value="{{ $page->content_ru }}">{!! urldecode($page->content_ru) !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title">Название</label>
                                            <input type="text" id="title" class="form-control" name="name_kz" required
                                                   value="{{ $page->name_kz }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Детали</label>
                                            <textarea id="detail" class="form-control summernote" name="content_kz" required
                                                      value="{{ $page->content_kz }}">{!! urldecode($page->content_kz) !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title">Название</label>
                                            <input type="text" id="title" class="form-control" name="name_en" required
                                                   value="{{ $page->name_en }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Детали</label>
                                            <textarea id="detail" class="form-control summernote" name="content_en" required
                                                      value="{{ $page->content_en }}">{!! urldecode($page->content_en) !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="mt-3 btn btn-success">Добавить</button>
                        <a href="{{ route('admin.news.index') }}" class="mt-3 btn btn-primary">Отмена</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
