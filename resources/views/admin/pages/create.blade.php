@extends('layouts.admin2')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-1">Новая страница</h2>
            <a href="/admin/pages" class="btn btn-primary mb-3">Назад</a>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.page.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Раздел на сайте:</label>
                            <select name="category_id" class="form-select">
                                @foreach ($categories as $category)
                                    <option {{ isset($_GET['category']) && $category->code === $_GET['category'] ? 'selected' : '' }}
                                            value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Русский</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Казахский</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Английский</button>
                            </li>
                        </ul>
                        <div class="tab-content pt-3" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="description">Название</label>
                                        <input type="text" id="title" class="form-control" name="name_ru" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Детали</label>
                                        <textarea id="details" class="form-control summernote" name="content_ru" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title">Название</label>
                                            <input type="text" id="title" class="form-control" name="name_kz" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Детали</label>
                                            <textarea id="details" class="form-control summernote" name="content_kz" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title">Название</label>
                                            <input type="text" id="title" class="form-control" name="name_en" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Детали</label>
                                            <textarea id="details" class="form-control summernote" name="content_en" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="mt-3 btn btn-success">Добавить</button>
                        <a href="{{ route('admin.news.index') }}" class="mt-3 btn btn-primary">Отмена</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
