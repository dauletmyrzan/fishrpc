@extends('layouts.admin2')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <h2>Подписки на новости</h2>
            <!-- <a href="{{ url('downloadFollowers/xls') }}" class="btn btn-primary">Скачать в Excel</a><br><br> -->
            <div class="card">
                <div class="card-body">
                	<div class="table-responsive" style="min-height:300px;">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Id</th>
                                <th>Email</th>
                            </thead>
                            <tbody>
                            	@foreach($followers as $follower)
                                <tr>
                                	<td>{{ $follower->id }}</td>
                                    <td>{{ $follower->email }}</td>
                                </tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("select[name='status_id']").on("change", function(){
            var query = $.query.SET('status', $(this).val());
            window.location.href = query;
        });
    });
</script>
@endsection
