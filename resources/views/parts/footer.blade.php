<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-md-6 footer-info">
                    <h3>@lang('parts.too')</h3>
                </div>

                <!-- <div class="col-lg-2 col-md-6 footer-links">
                  <h4>Useful Links</h4>
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Terms of service</a></li>
                    <li><a href="#">Privacy policy</a></li>
                  </ul>
                </div> -->

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>@lang('parts.contacts')</h4>
                    <p>
                        @lang('parts.address')<br>
                        <strong>Телефон:</strong> +7 (727) 383-17-15<br>
                        <strong>Email:</strong> info@fishrpc.kz<br>
                    </p>

                    <div class="social-links">
                        <a target="_blank" href="https://ru-ru.facebook.com/kazniirh/" class="facebook"><i
                                class="bi bi-facebook"></i></a>
                        <a target="_blank" href="https://www.instagram.com/fishrpc/?utm_medium=copy_link"
                           class="instagram"><i class="bi bi-instagram"></i></a>
                        <a target="_blank" href="https://twitter.com/fishrpc_kz" class="twitter"><i
                                class="bi bi-twitter"></i></a>
                        <a target="_blank" href="https://www.youtube.com/channel/UCuS5M4RaYq2Kt-qUtNYDcpQ"
                           class="youtube"><i class="bi bi-youtube"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>@lang('parts.newsletter')</h4>
                    <form action="/subscribe" class="form-inline" method="post">
                        @csrf
                        <input type="email" required name="email"><input type="submit" value="@lang('parts.subscribe')">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            &copy; 2021. @lang('parts.rights').
        </div>
        <div class="credits">
            <!--LiveInternet counter--><a href="https://www.liveinternet.ru/click"
                                          target="_blank"><img id="licntA04C" width="88" height="31" style="border:0"
                                                               title="LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня"
                                                               src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAIBTAA7"
                                                               alt=""/></a><script>(function(d,s){d.getElementById("licntA04C").src=
                    "https://counter.yadro.ru/hit?t14.6;r"+escape(d.referrer)+
                    ((typeof(s)=="undefined")?"":";s"+s.width+"*"+s.height+"*"+
                        (s.colorDepth?s.colorDepth:s.pixelDepth))+";u"+escape(d.URL)+
                    ";h"+escape(d.title.substring(0,150))+";"+Math.random()})
                (document,screen)</script><!--/LiveInternet-->

        </div>
    </div>
</footer><!-- End Footer -->
