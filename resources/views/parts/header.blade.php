<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center @if (isset($header)) header-dark @endif">
    <div class="container d-flex justify-content-between">

        <div class="logo">
            <a href="/"><img src="{{ asset('vendor/img/logo.png') }}" alt="" class="img-fluid"></a>
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="/">@lang('parts.home')</a></li>
                <li class="dropdown"><a href=""><span>@lang('parts.about')</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="/head-office">@lang('parts.head')</a></li>
                        <li><a href="/information-base">@lang('parts.base')</a></li>
                        <li><a href="/achievements">@lang('parts.grants')</a></li>
                    </ul>
                </li>
                <li><a class="nav-link scrollto" href="/structure">@lang('parts.structure')</a></li>
                <li><a class="nav-link scrollto" href="/news">@lang('parts.news')</a></li>
                <li><a class="nav-link scrollto " href="/gallery">@lang('parts.gallery')</a></li>
                <li><a class="nav-link scrollto " href="/reviews">@lang('titles.reviews')</a></li>
                <li><a class="nav-link" href="{{ route('vacancies.index') }}">@lang('parts.vacancies')</a></li>
                <li class="dropdown"><a href="#"><span>@lang('parts.labs')</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        @if (isset($labs))
                            @foreach ($labs as $lab)
                                <li><a href="/lab/{{ $lab->code }}">{{ $lab->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li class="dropdown"><a href="#"><span>@lang('parts.deps')</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        @if (isset($departments))
                            @foreach ($departments as $department)
                                <li><a href="/department/{{ $department->code }}">{{ $department->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li class="dropdown"><a href="#"><span>@lang('parts.branch')</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        @if (isset($branches))
                            @foreach ($branches as $branch)
                                <li><a href="/branch/{{ $branch->code }}">{{ $branch->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li><a class="nav-link scrollto" href="/contact">@lang('parts.contacts')</a></li>
                <li class="dropdown"><a href=""><span>@lang('parts.language')</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="/locale/kz">Қазақша</a></li>
                        <li><a href="/locale/en">English</a></li>
                        <li><a href="/locale/ru">Русский</a></li>
                    </ul>
                </li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
    </div>
</header>
<!-- #header -->
