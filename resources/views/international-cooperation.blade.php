@extends('layouts.app')

@section('content')

<!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Международное сотрудничество</h2>
        </div>

      </div>
    </section><!-- Breadcrumbs Section -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">


          <div class="col-lg-12" data-aos="fade-left">
            <!-- <div class="portfolio-info">
              <ul>
                <li><strong>Заведующий лабораторией</strong>: Абилов Бердибек Ибрагимович</li>
              </ul>
            </div> -->
            <div class="portfolio-description">
              <p> ТОО «НПЦ РХ» осуществляет международное сотрудничество с ведущими научными учреждениями дальнего и ближнего зарубежья по вопросам экологии водной среды, сохранения биоразнообразия ценных,   редких и исчезающих видов рыб , популяционно-генетическим исследованиям  ихтиоценозов, аквакультуре и т.п. (Россия, Азербайджан, Великобритания, Китай, Иран, Израиль и др.) ведется работа с международными организациями и программами:
            <br><br>
              Конвенция о Международной Торговле Редкими Видами Диких Животных и Растений (СИТЕС),
            <br><br>
              Продовольственная и сельскохозяйственная организация Объединенных наций (ФАО),
            <br><br>
              Программа развития Организации Объединенных наций в РК (ПРООН),
            <br><br>
              Международный фонд спасения Арала(МФСА),
            <br><br>
              Консорциум «NorthCaspianOperatingCompany» (NCOC),
            <br><br>
              Мировое сообщество по сохранению осетровых видов рыб «WordSturgeonConservationSocicty» (WSCS),
            <br><br>
              Всемирный фонд дикой природы (WWF «WordWildFoud»), участие в Комиссии по водным биоресурсам Каспийского моря.
              </p>
            </div>
          </div>
         
        </div>

      </div>
    </section><!-- End Portfolio Details Section -->
@endsection