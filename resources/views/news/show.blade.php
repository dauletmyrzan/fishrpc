@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <div class="bread-crumbs mb-4">
                <a href="/">@lang('parts.home')</a> /
                <a href="{{ route('news.index') }}">@lang('news.title')</a> /
                <span class="text-muted">{{ $post->titleWithLang }}</span>
            </div>
            <h1 class="fw-bold">{{ $post->titleWithLang }}</h1>
            <div class="row gy-4">
                <div class="col-lg-6">
                    <div class="portfolio-details-slider swiper">
                        <div class="swiper-wrapper align-items-center">
                            <div class="col-lg-12 mb-5">
                                <img class="img-fluid" width="200"
                                     src="{{ asset('images/news/'  . $post->img) }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 mt-0">
                    <div class="portfolio-description pt-0" style="margin-bottom:100px;">
                        {!! $post->details !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
