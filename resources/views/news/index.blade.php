@extends('layouts.app', [
    'header' => 'dark'
])

@section('content')
    <section id="portfolio" class="section-bg mt-3">
		<div class="container">
			<h1 class="fw-bold">@lang('news.title')</h1>
			<div class="row portfolio-container">
				@foreach($news as $post)
					<div class="col-lg-4 col-md-6 portfolio-item filter-app">
						<a href="{{ url('news/' . $post->slug) }}">
							<div class="news-img" style="background-image:url({{ asset('images/news/' . $post->img) }})"></div>
							<div class="mt-3" style="height:90px;border-bottom:1px #ddd solid;">{{ $post->titleWithLang }}</div>
                            <span class="post-datetime mt-2 d-block">{{ $post->created_at }}</span>
						</a>
					</div>
				@endforeach
			</div>
		</div>
    </section>
@endsection
