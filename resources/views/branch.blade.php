@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <h1 class="fw-bold">{{ $branch->name }}</h1>
            {!! $branch->content !!}
        </div>
    </section>
@endsection
