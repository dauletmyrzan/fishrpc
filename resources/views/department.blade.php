@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <h1 class="fw-bold">{{ $department->name }}</h1>
            {!! $department->content !!}
        </div>
    </section>
@endsection
