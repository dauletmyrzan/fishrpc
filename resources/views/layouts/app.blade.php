<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>ТОО «Научно-производственный центр рыбного хозяйства»</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <script src="{{ asset('vendor/vendor/tree/tree_maker-min.js') }}"></script>
    <link href="{{ asset('vendor/img/logo.png') }}" rel="icon">
    <link href="{{ asset('vendor/img/logo.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('vendor/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/tree/tree_maker-min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('vendor/css/style.css') }}" rel="stylesheet">
</head>
<body>
    @include('parts.header')
    @yield('content')
    @include('parts.footer')

    <script src="{{ asset('vendor/vendor/aos/aos.js') }}"></script>

    <script src="{{ asset('vendor/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('vendor/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('vendor/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ asset('vendor/vendor/purecounter/purecounter.js') }}"></script>
    <script src="{{ asset('vendor/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments);
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a);
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(86719989, "init", {
            clickmap:            true,
            trackLinks:          true,
            accurateTrackBounce: true,
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/86719989" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <!-- Template Main JS File -->
    <script src="{{ asset('vendor/js/main.js') }}"></script>
</body>
</html>
