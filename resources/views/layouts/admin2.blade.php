<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">
    <title>
        FishRPC Dashboard
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('/css/soft-ui-dashboard.css?v=1.0.3') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/vendor/summernote/summernote-lite.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
</head>
<body class="g-sidenav-show  bg-gray-100">
    <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-white" id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="/admin">
                <img src="{{ asset('/img/logo.png') }}" class="navbar-brand-img h-100" alt="main_logo">
                <span class="ms-1 font-weight-bold">FishRPC</span>
            </a>
        </div>
        <hr class="horizontal dark mt-0">
        <div class="collapse navbar-collapse w-auto max-height-vh-100 h-100" id="sidenav-collapse-main">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/news') || \Request::is('admin') ? 'active' : '' }}" href="{{ route('admin.news.index') }}">
                        <div style="width:20px;text-align:center">
                            <span class="fa fa-newspaper"></span>
                        </div>
                        <span class="nav-link-text ms-2">Новости</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/pages') ? 'active' : '' }}" href="{{ route('admin.pages') }}">
                        <div style="width:20px;text-align:center">
                            <span class="fa fa-copy"></span>
                        </div>
                        <span class="nav-link-text ms-2">Страницы</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/gallery') ? 'active' : '' }}" href="{{ route('admin.gallery') }}">
                        <div style="width:20px;text-align:center">
                            <span class="fa fa-images"></span>
                        </div>
                        <span class="nav-link-text ms-2">Галерея</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/reviews') ? 'active' : '' }}" href="{{ route('admin.reviews') }}">
                        <div style="width:20px;text-align:center">
                            <i class="fa fa-file-image"></i>
                        </div>
                        <span class="nav-link-text ms-2">Отзывы</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/vacancies') ? 'active' : '' }}" href="{{ route('admin.vacancies.index') }}">
                        <div style="width:20px;text-align:center">
                            <i class="fa fa-briefcase"></i>
                        </div>
                        <span class="nav-link-text ms-2">Вакансии</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/translations">
                        <div style="width:20px;text-align:center">
                            <i class="fa fa-language"></i>
                        </div>
                        <span class="nav-link-text ms-2">Переводы</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/requests') ? 'active' : '' }}" href="{{ route('admin.requests') }}">
                        <div style="width:20px;text-align:center">
                            <i class="fa fa-list-ol"></i>
                        </div>
                        <span class="nav-link-text ms-2">Заявки</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ \Request::is('admin/subscribers') ? 'active' : '' }}" href="{{ route('admin.subscribers') }}">
                        <div style="width:20px;text-align:center">
                            <i class="fa fa-users"></i>
                        </div>
                        <span class="nav-link-text ms-2">Подписчики</span>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
            <div class="container-fluid py-1 pr-3 ps-0">
                <nav aria-label="breadcrumb">
                    <h6 class="font-weight-bolder mb-0">FishRPC Dashboard</h6>
                </nav>
                <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center"></div>
                    <ul class="navbar-nav  justify-content-end">
                        <li class="nav-item d-flex align-items-center">
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="cursor-pointer btn btn-white">
                                    <i class="fa fa-sign-out me-sm-1"></i>
                                    <span class="d-sm-inline d-none">Выйти</span>
                                </button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="container-fluid">
            @yield('content')
        </div>
    </main>
    <script src="{{ asset('/js/plugins/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/smooth-scrollbar.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('vendor/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('vendor/vendor/summernote/summernote-lite.min.js') }}"></script>
    <script src="{{ asset('/js/soft-ui-dashboard.min.js?v=1.0.3') }}"></script>
    <script src="{{ asset('vendor/admin/js/main.js') }}"></script>
    @if (session('status'))
        <script>
            Swal.fire({
                text:              "{{ session('status') }}",
                icon:              'info',
                confirmButtonText: 'Хорошо'
            });
        </script>
    @endif
    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                height: 350,
            });
            /**
             * Initiate portfolio lightbox
             */
            const portfolioLightbox = GLightbox({
                selector: '.portfolio-lightbox',
            });
        });
    </script>
</body>
</html>
