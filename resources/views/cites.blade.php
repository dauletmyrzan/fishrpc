@extends('layouts.app')

@section('content')
    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details" style="min-height:700px;">
        <div class="container">
            <div class="row gy-4">
                <h1 class="fw-bold">CITES</h1>
                <div class="col-lg-12">
                    <div class="portfolio-description p-0">
                        <div class="accordion mb-5" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header mb-0" id="headingSix">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseSix" aria-expanded="false"
                                            aria-controls="collapseSix">
                                        @lang('cites.title1')
                                    </button>
                                </h2>
                                <div id="collapseSix" class="accordion-collapse collapse " aria-labelledby="headingSix"
                                     data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="col-lg-12">
                                            <div class="portfolio-info my-4">
                                                <p>
                                                    @lang('cites.text1')
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h2 class="accordion-header mb-0" id="headingSeven">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseSeven" aria-expanded="false"
                                            aria-controls="collapseSeven">
                                        @lang('cites.title2')
                                    </button>
                                </h2>
                                <div id="collapseSeven" class="accordion-collapse collapse"
                                     aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="portfolio-info my-2">
                                            <div class="col-lg-12" data-aos="fade-left">
                                                <div class="portfolio-description p-0">
                                                    <p>@lang('cites.text2')</p>
                                                    <h2><a target="_blank"
                                                           href="{{ asset('vendor/files/1-Правила-выдачи-Ситес-v2000020856.10-06-2020.rus_.pdf') }}">1
                                                            Правила выдачи Ситес v2000020856.10-06-2020</a></h2>
                                                    <h2><a target="_blank"
                                                           href="{{ asset('vendor/files/2-Заявление-в-Научный-орган-СИТЕС-Форма.docx') }}">2
                                                            Заявление в Научный орган СИТЕС Форма</a></h2>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="portfolio-description">
                                                    <div class="col-lg-8 mb-5">
                                                        <img class="img-fluid"
                                                             src="{{ asset('vendor/img/cites/cites1.jpg') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="portfolio-description">
                                                    <div class="col-lg-8 mb-5">
                                                        <img class="img-fluid"
                                                             src="{{ asset('vendor/img/cites/cites2.jpg') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="portfolio-description">
                                                    <div class="col-lg-8 mb-5">
                                                        <img class="img-fluid"
                                                             src="{{ asset('vendor/img/cites/cites3.jpg') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="portfolio-description">
                                                    <div class="col-lg-8 mb-5">
                                                        <img class="img-fluid"
                                                             src="{{ asset('vendor/img/cites/cites4.jpg') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="portfolio-description">
                                                    <div class="col-lg-8 mb-5">
                                                        <img class="img-fluid"
                                                             src="{{ asset('vendor/img/cites/cites5.jpg') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
    </section><!-- End Portfolio Details Section -->
@endsection
