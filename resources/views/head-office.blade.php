@extends('layouts.app')

@section('content')

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <h1 class="fw-bold">Головное подразделение</h1>
            <div class="row gy-4">
                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <div class="accordion mb-5" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSix">
                                    <button class="accordion-button collapsed colored" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                        @lang('head.management')
                                    </button>
                                </h2>
                                <div id="collapseSix" class="accordion-collapse collapse " aria-labelledby="headingSix"
                                     data-bs-parent="#accordionExample">
                                    <div class="accordion-body row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4 text-center" data-aos="fade-down">
                                            <img class="img-fluid mt-5" src="{{ asset('vendor/img/director.jpg') }}">
                                            <div class="portfolio-info">
                                                <ul>
                                                    <li>
                                                        <strong>@lang('head.director')</strong><br>@lang('head.directorname')
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4 text-center" data-aos="fade-down">
                                            <img class="img-fluid mt-5" src="{{ asset('vendor/img/sekretar.jpg') }}">
                                            <div class="portfolio-info">
                                                <ul>
                                                    <li>
                                                        <strong>@lang('head.sekretar')</strong><br>@lang('head.sekretarname')
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 text-center" data-aos="fade-down">
                                            <img class="img-fluid mt-5" src="{{ asset('vendor/img/zamdirector.jpg') }}">
                                            <div class="portfolio-info">
                                                <ul>
                                                    <li>
                                                        <strong>@lang('head.zamdirector')</strong><br>@lang('head.zamdirectorname')
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 text-center" data-aos="fade-down">
                                            <img class="img-fluid mt-5"
                                                 src="{{ asset('vendor/img/sovetnik-director.jpg') }}">
                                            <div class="portfolio-info">
                                                <ul>
                                                    <li>
                                                        <strong>@lang('head.sovetnik')</strong><br>@lang('head.sovetnikname')
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingSeven">
                                    <button class="accordion-button collapsed colored" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseSeven"
                                            aria-expanded="false" aria-controls="collapseSeven">
                                        @lang('head.history')
                                    </button>
                                </h2>
                                <div id="collapseSeven" class="accordion-collapse collapse"
                                     aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>
                                            @lang('head.historytext')
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingEight">
                                    <button class="accordion-button collapsed colored" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseEight"
                                            aria-expanded="false" aria-controls="collapseEight">
                                        @lang('head.mission')
                                    </button>
                                </h2>
                                <div id="collapseEight" class="accordion-collapse collapse"
                                     aria-labelledby="headingEight" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>
                                            @lang('head.missiontext')
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingNine">
                                    <button class="accordion-button collapsed colored" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseNine"
                                            aria-expanded="false" aria-controls="collapseNine">
                                        @lang('head.cooperation')
                                    </button>
                                </h2>
                                <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine"
                                     data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>
                                            @lang('head.cooperationtext')
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTen">
                                    <button class="accordion-button collapsed colored" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseTen"
                                            aria-expanded="false" aria-controls="collapseTen">
                                        @lang('head.works')
                                    </button>
                                </h2>
                                <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen"
                                     data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>
                                            @lang('head.workstext')
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingEleven">
                                    <button class="accordion-button collapsed colored" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseEleven"
                                            aria-expanded="false" aria-controls="collapseEleven">
                                        @lang('head.research')
                                    </button>
                                </h2>
                                <div id="collapseEleven" class="accordion-collapse collapse"
                                     aria-labelledby="headingEleven" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>
                                            @lang('head.researchtext')
                                        </p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
