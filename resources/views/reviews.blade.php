@extends('layouts.app')

@section('content')
    <section id="portfolio" class="section-bg" style="min-height:700px;">
        <div class="container">
            <h1 class="fw-bold">@lang('titles.reviews')</h1>
            <div class="row portfolio-container">
                @if (count($reviews) === 0)
                    <div class="h4">Нет отзывов.</div>
                @endif
                @foreach($reviews as $review)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                        <a href="{{ asset('images/reviews/' . $review->img_src) }}" data-gallery="portfolioGallery"
                           class="portfolio-lightbox link-preview">
                            <div class="portfolio-wrap">
                                <div class="news-img" style="background-image:url({{ asset('images/reviews/' . $review->img_src) }})"></div>
                                <div class="portfolio-info"></div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
