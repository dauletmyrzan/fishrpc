@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <h1 class="fw-bold">{{ $lab->name }}</h1>
            {!! $lab->content !!}
        </div>
    </section>
@endsection
