@extends('layouts.app')

@section('content')
<section id="products">
   <div class="container">
      <div class="section-header text-left wow fadeInUp mb-5">
         <h2 class="font-weight-bold">@lang('home.catalog')</h2>
      </div>
      @foreach ($products as $product)
      <div class="row">
         <div class="col-md-6 col-lg-6">
            <div class="box wow fadeInLeft">
               <div class="icon"></div>
               <h4 class="title">{{ (App::getLocale() == 'ru') ? $product->name : $product->name_en }}</h4>
               <p class="description mb-4 mb-4">{{ (App::getLocale() == 'ru') ? $product->properties : $product->properties_en }}</p>
               <a href="{{ url('product/' . $product->id)  }}" class="btn btn-primary btn-more active" role="button">Подробнее</a>
            </div>
         </div>
         <div class="col-md-6 col-lg-6">
            <div class="box-img">
               <img class="img-fluid" src="{{ asset('vendor/img/products/' . $product->img_src)  }}" alt="">
            </div>
         </div>
      </div>
      @endforeach
   </div>
</section>
@endsection