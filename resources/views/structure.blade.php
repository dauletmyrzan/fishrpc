@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <h1 class="fw-bold my-4">@lang('structure.head')</h1>
            <div class="row gy-4 pb-5">
                <div class="col-lg-12">
                    <div class="portfolio-info text-center box-org-head">
                        <h2>@lang('structure.structure1')</h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="portfolio-info text-center box-org-head">
                        <h2>@lang('structure.structure2')</h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="portfolio-info text-center box-org-head">
                        @lang('structure.structure3')
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="portfolio-info text-center box-org px-3">
                        @foreach ($branches as $branch)
                            <a href="/branch/{{ $branch->code }}" class="btn btn-org my-1">{{ $branch->name }}</a>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="portfolio-info text-center box-org px-3">
                        @foreach ($labs as $lab)
                            <a href="/lab/{{ $lab->code }}" class="btn btn-org my-1">{{ $lab->name }}</a>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="portfolio-info text-center box-org px-3">
                        @foreach ($departments as $department)
                            <a href="/department/{{ $department->code }}" class="btn btn-org my-1">{{ $department->name }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
