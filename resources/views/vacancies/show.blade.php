@extends('layouts.app')

@section('content')
    <section id="job" style="margin-top:130px;min-height:800px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4">
                    <div class="bread-crumbs">
                        <a href="/">@lang('parts.home')</a> /
                        <a href="{{ route('vacancies.index') }}">@lang('parts.vacancies')</a> /
                        <span class="text-muted">{{ $job->positionWithLang }}</span>
                    </div>
                </div>
                <div class="mb-4">
                    <h1 class="fw-bold">{{ $job->positionWithLang }}</h1>
                    <h5 class="text-black">{{ $job->wage }}</h5>
                    <div style="color:cornflowerblue">
                        <i class="bi bi-pin"></i> {{ $job->getBranch() }}
                    </div>
                </div>
                <div class="col-lg-6 content">
                    <h4 class="fw-bold">@lang('home.requirements')</h4>
                    <div class="mb-4">
                        {!! urldecode($job->requirementsWithLang) !!}
                    </div>

                    <h4 class="fw-bold">@lang('home.conditions')</h4>
                    <div class="mb-4">{!! urldecode($job->conditionsWithLang) !!}</div>

                    @if ($job->dutiesWithLang)
                    <h4 class="fw-bold">@lang('home.duties')</h4>
                    <div class="mb-4">
                        {!! urldecode($job->dutiesWithLang) !!}
                    </div>
                    @endif

                    @if ($job->schedule)
                        <h4 class="fw-bold">@lang('home.schedule')</h4>
                        <div class="mb-4">{!! $job->schedule !!}</div>
                    @endif
                </div>
                <div class="col-lg-6 content">
                    <h4 class="fw-bold">@lang('home.hr')</h4>
                    <ul class="ps-4">
                        <li>@lang('home.phone'): <a href="tel:87273831715">8 (727) 383-17-15</a></li>
                        <li>Email: <a href="mailto:info@fishrpc.kz">info@fishrpc.kz</a></li>
                    </ul>
                    @if ($job->file && $job->file !== '')
                        <a href="/files/{{ $job->file }}" class="btn btn-success">Скачать файл</a>
                    @endif
                </div>
                <hr style="border: 1px solid #ccc;">
                <div class="post-datetime mb-5" style="font-size:10pt;">{{ $job->created_at }}</div>
            </div>
        </div>
    </section>
@endsection
