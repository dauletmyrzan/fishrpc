@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details" style="min-height:800px;">
        <div class="container">
            <h1 class="fw-bold">@lang('parts.vacancies')</h1>
            <div class="row gy-4">
                <div class="col-lg-12">
                    @foreach ($jobs as $job)
                        <div class="border rounded shadow-sm bg-white p-3 mb-4">
                            <h4><a href="/vacancies/{{ $job->id }}">{{ $job->positionWithLang }}</a></h4>
                            <div class="mt-3 post-datetime">{{ $job->created_at }}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
