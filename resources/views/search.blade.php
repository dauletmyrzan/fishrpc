@extends('layouts.app')

@section('content')
    <section class="breadcrumbs">
        <div class="container"></div>
    </section>

    <section id="portfolio" class="section-bg" style="min-height:500px;">
        <div class="container">
            <h1 class="fw-bold">Поиск</h1>
            <div class="row mb-4">
                <div class="col-md-4">
                    <form action="/search" class="form-inline">
                        <input type="text" class="form-control d-inline-block w-auto" required name="query" value="{{ $_GET['query'] ?? '' }}">
                        <button class="btn btn-success" style="margin-top:-3px;width:100px;">Поиск</button>
                    </form>
                </div>
            </div>
            @if ($posts && $posts->count() > 0)
                <div class="row">
                    @foreach ($posts as $post)
                        <div class="col-md-3">
                            <a href="/news/{{ $post->slug }}">
                                <div class="icon-box">
                                    <div class="news-img" style="background-image:url({{ asset('images/news/' . $post->img_src->img_src) }})"></div>
                                    <div class="mt-2 title"><a href="{{ url('news/' . $post->id)  }}">{{ $post->titleWithLang }}</a></div>
                                    <span class="post-datetime">{{ $post->created_at }}</span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <h2 class="fw-bold">По вашему запросу ничего не найдено.</h2>
            @endif
        </div>
    </section>
@endsection
