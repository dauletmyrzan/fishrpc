@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-6">
                    <div class="portfolio-details-slider swiper">
                        <div class="swiper-wrapper align-items-center">
                            <div class="col-lg-8 mb-5">
                                <img class="img-fluid"
                                     src="{{ asset('vendor/img/departments/administrative_department.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="portfolio-info">
                        <ul>
                            <li><strong>@lang('administrative.manager')</strong><br>@lang('administrative.managername')
                            </li>
                        </ul>
                    </div>
                    <div class="portfolio-description">
                        <p>
                            @lang('administrative.text1')
                        </p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="portfolio-description">
                        @lang('administrative.text2')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
