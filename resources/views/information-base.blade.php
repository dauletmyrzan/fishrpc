@extends('layouts.app')

@section('content')
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('head.cooperation')</h2>
                        <p>@lang('base.base_first_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.base_title')</h2>
                        <p>@lang('base.base_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.normative_title')</h2>
                        <p>@lang('base.normative_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_title')</h2>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2011')</h2>
                        <p>@lang('base.publications_2011_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2012')</h2>
                        <p>@lang('base.publications_2012_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2013')</h2>
                        <p>@lang('base.publications_2013_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2014')</h2>
                        <p>@lang('base.publications_2014_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2015')</h2>
                        <p>@lang('base.publications_2015_text')</p>
                    </div>
                </div>


                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2016')</h2>
                        <p>@lang('base.publications_2016_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2017')</h2>
                        <p>@lang('base.publications_2017_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2018')</h2>
                        <p>@lang('base.publications_2018_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2019')</h2>
                        <p>@lang('base.publications_2019_text')</p>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>@lang('base.publications_2020')</h2>
                        <p>@lang('base.publications_2020_text')</p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Details Section -->
@endsection
