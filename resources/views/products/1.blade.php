@extends('layouts.app')

@section('content')
    <section id="product" class="wow fadeInUp">
      <div class="container">
        <div class="row">
            <div class="col-lg-12 section-header text-left wow fadeInUp mb-5">
               <h2 class="font-weight-bold">@lang('product1.title').</h2>
            </div>
          <div class="col-lg-6 product-img mb-5">
            <img class="img-fluid" src="{{ asset('vendor/img/products/card1.jpg') }}" alt="">
          </div>

          <div class="col-lg-6 content">
            <h3 class="font-weight-bold">@lang('product1.formula-title')</h3>
            <ul>
              <li>@lang('product1.formula-text')</li>
            </ul>
            <h3 class="font-weight-bold">@lang('product1.properties-title')</h3>
            <ul>
              <li>@lang('product1.properties-text')</li>
            </ul>
            <h3 class="font-weight-bold">@lang('product1.appearance-title')</h3>
            <ul>
              <li>@lang('product1.appearance-text')</li>
            </ul>
            <h3 class="font-weight-bold">@lang('product1.application-title')</h3>
            <ul>
              <li>@lang('product1.application-text1')</li>
              <li>@lang('product1.application-text2')</li>
              <li>@lang('product1.application-text3')</li>
              <li>@lang('product1.application-text4')</li>
            </ul>
          </div>
        </div>

      </div>
    </section><!-- #product -->
@endsection