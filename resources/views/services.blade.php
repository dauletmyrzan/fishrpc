@extends('layouts.app')

@section('content')
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>@lang('parts.services')</h2>
            </div>
        </div>
    </section>

    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-12" data-aos="fade-left">
                    <div class="portfolio-description">
                        <h2></h2>
                        <p>@lang('services.text')</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
