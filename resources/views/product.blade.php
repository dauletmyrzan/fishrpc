@extends('layouts.app')

@section('content')
    <section id="product" class="wow fadeInUp">
      <div class="container">
        <div class="row">
            <div class="col-lg-12 section-header text-left wow fadeInUp mb-5">
               <h2 class="font-weight-bold">{{ $product->name }}</h2>
            </div>
          <div class="col-lg-6 product-img mb-5">
            <img class="img-fluid" src="{{ asset('vendor/img/products/' . $product->img_src)  }}">
          </div>

          <div class="col-lg-6 content">
            <h3 class="font-weight-bold">@lang('product.formula-title')</h3>
            <ul>
              <li>{{ $product->formula }}</li>
            </ul>
            <h3 class="font-weight-bold">@lang('product.properties-title')</h3>
            <ul>
              <li>{{ $product->properties }}</li>
            </ul>
            <h3 class="font-weight-bold">@lang('product.appearance-title')</h3>
            <ul>
              <li>{{ $product->appearance }}</li>
            </ul>
            <h3 class="font-weight-bold">@lang('product.application-title')</h3>
            <ul>{{ $product->application }}</ul>
          </div>
        </div>

      </div>
    </section><!-- #product -->
@endsection