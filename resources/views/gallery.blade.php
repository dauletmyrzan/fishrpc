@extends('layouts.app')

@section('content')
    <section id="portfolio" class="section-bg">
        <div class="container">
            <h1 class="fw-bold">@lang('titles.gallery')</h1>
            <div class="row portfolio-container">
                @foreach($images as $image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                        <a href="{{ asset('images/gallery/' . $image->img_src) }}" data-gallery="portfolioGallery"
                           class="portfolio-lightbox link-preview">
                            <div class="portfolio-wrap">
                                <div class="news-img" style="background-image:url({{ asset('images/gallery/' . $image->img_src) }})"></div>
                                <div class="portfolio-info"></div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
