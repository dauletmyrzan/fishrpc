<?php

return [
    'duties'       => 'Duties',
    'phone'        => 'Phone',
    'hr'           => 'Contacts',
    'conditions'   => 'Conditions',
    'wage'         => 'Salary',
    'schedule'     => 'Schedule',
    'requirements' => 'Requirements',
    'search_btn'   => 'Search',
    'welcome'      => 'LLP «Fisheries Research and Production Center»',
    'mission'      => 'To safe and restore fish resources of Kazakhstan to open new ways of fishery development.',
    'minsel'       => 'Ministry of Education and Science of the Republic of Kazakhstan',
    'rubkomitet'   => 'Fisheries Committee MEGNR RK',
    'meg'          => 'Ministry of Ecology, Geology and Natural Resources of the Republic of Kazakhstan',
    'feedback'     => 'Feedback',
    'btnmore'      => 'More',
    'calculator'   => 'Aquaculture Calculator',
    'gis'          => 'The GIS recommendations about fishery',
    'gis_text'     => 'Scientific and Practical Center for Fisheries LLP provides a spatial & digital database on scientific recommendations in the field of fishery and protection of fish stocks.',
    'citis'        => '',
    'citis_text'   => '«Fisheries Research and Production Center» LLP  is a scientific organization for fish and other aquatic animals (CITES)',
    'base'         => 'Information base',
    'base_text'    => 'In Scientific and Practical Center for Fisheries LLP (SPCF) the monitoring of a state scientific publications, standard and methodical recommendations etc. is carried out.',
    'service1'     => 'Researches of water reservoirs for the purpose of definition of possibility of their fishery use.',
    'service2'     => 'Development of recommendations and biological justifications on rational use of fish resources, stocking of reservoirs, acclimatization of fishes and fodder organisms.',
    'service3'     => 'Development of fish-breeding and biological justifications for progress of aquaculture.',
    'service4'     => 'Carrying out genetic researches, certification of fishes and other species of water animals.',
    'pavlodar'     => 'Pavlodar city',
    'kostanay'     => 'Kostanay city',
    'karaganda'    => 'Karaganda city',
    'petropavlsk'  => 'Petropavlsk city',
    'cites_title'  => 'What is CITES',
    'cites_text'   => 'CITES – the reduced name of the Convention on international trade in the types of wild fauna and flora which are under the threat of disappearance (Convention on International Trade in Endangered Species of 	Wild Fauna and Flora, СИТЕК in the Russian transcription of CITES) – the international government agreement signed as a result of the resolution of the International union of conservation of nature (IUCN) in 1973 in 	Washington. It entered into force on July 1, 1975. Also known as the «Convention on International Trade in Endangered Species of Wild Fauna and Flora.»<br><br>
		In order to preserve species of wild fauna and flora, and recognizing that international cooperation is necessary to protect certain species of wild fauna and flora from overexploitation in international trade, the Republic of Kazakhstan acceded to the Convention by Act No. 372-1 of 6 April 1999.
		The objective of the Convention is to ensure that the international trade in wild animals and plants does not endanger their survival; the agreement represents different degrees of protection for more than 33,000 species of animals and plants.<br><br>
		Preamble of the Convention:<br><br>
		«Contracting States,
		Recognizing that wild fauna and flora, in their many beautiful and diverse forms, are an indispensable part of the Earth`s natural systems, which must be protected for the present generation and for the future,
		Aware of the increasing value of wild fauna and flora in terms of aesthetics, science, culture, recreation and economics,
		Recognizing that peoples and States are and should be the best custodians of their own wild fauna and flora,
		Recognizing also that international cooperation is necessary to protect certain species of wild fauna and flora from overexploitation in international trade,
		Convinced of the need to take appropriate measures to that end, agreed on the following…»
		Threatened species are grouped in three Annexes according to the extent of their threat of extinction and the measures taken to trade them.<br><br>
		Annex I – about 800 species
		The annex includes all endangered species whose trade has or may have an adverse effect on their existence. Trade in specimens of these species should be particularly strictly regulated so as not to further jeopardize their survival and should be allowed only in exceptional circumstances. Among the species of this list in particular - gorilla (Gorilla gorilla), species of chimpanzees (Pan spp.), tiger (Panthera tigris), indian lion (Panthera leo persica), leopard (Panthera pardus), jaguar (Panthera onca), indian elephant (Elephas maximus), some populations of savannovy african elephant (Loxodonta africana), dugong (Dugong dugon), manatees (Trichechidae) and all species of rhinoceroses, except for some subspecies of South Africa.
		Annex II – about 32,500 species<br><br>
		The annex includes all species that, although not necessarily at risk of extinction at this time, may be at risk if trade in specimens of such species is not strictly regulated to prevent use that is incompatible with their survival; and other species that need to be regulated so that trade in samples of some species from the first list can be effectively controlled.Annex III – about 300 species.<br><br>
		Annex III shall include all types which, as defined by any Party, are subject to regulation within it’s jurisdiction to prevent or restrict exploitation and for which cooperation of other parties in control of trade is required.
		Since the signing of the convention in 1975, no one species under it’s protection has died out yet as a result of trade.
		In accordance with paragraph 3 of the Decree of the Government of the Republic of Kazakhstan of December 28, 1999 N 1994 «On measures to ensure the fulfillment by the Republic of Kazakhstan of obligations, arising from the Convention on international Trade in Species of Wild Flora and Fauna», the function of the scientific organization on Terrestrial Animal and Bird Species to fulfil the obligations under the Convention in the Republic of Kazakhstan is entrusted to the Republican state enterprise on the right of economic management «Institute of zoology» of the Committee of science of the Ministry of education and science of the Republic of Kazakhstan (RSE «Institute of zoology» KS MES RK).
		The main task of this scientific orgaisation is to provide scientific and methodological support for the implementation of the obligations of the Republic of Kazakhstan arising from the provisions of CITES, from the recommendations and resolutions of the Conferences of the Parties to the Convention, from the recommendations of the committees and the CITES Secretariat.',
];
