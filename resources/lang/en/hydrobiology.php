<?php 

return [
'head' => 'Laboratory of hydrobiology and gidroanalytics',
'manager' => 'Head of Laboratory',

'managername' => 'Mazhibaeva Zhanara Omirbekovna, PhD',

'text1' => '  <h2>Ongoing activities:</h2>
              <p> <br>• Monitoring food supply of fish Kapshagai reservoir and Alakol lakes, local waters of Almaty region;

                  <br>• Development of recommendations and biological studies on acclimatization of food organisms;

                  <br>• Evaluation of forage resources of water bodies and the identification of the degree of consumption of food organisms and fed the fish in natural water bodies and fish farms;

                  <br>• Development of biological studies and forecast production limit of Artemia cysts, and other valuable aquatic invertebrates;

                  </p>',

'text2' => '<p>
                <br>• Review of biological studies, scientific research and projects;

				<br>• Methodical assistance, advice on all matters of hydrobiology;

				<br>• Determination of the quality of water in reservoirs for contamination indicator organisms;

				<br>• Comprehensive survey of the different types of water bodies (streams, ponds, hot water, etc.) with a conclusion about the suitability of water for various household needs, including fisheries management on hydrochemical and toxicological indicators;

				<br>• Determination of hydrochemical indices of water; pH, dissolved gases, nutrients and organic matter, mineral salt with the forecast exposure ponds kill phenomena;

				<br>• Determination of the content of toxic substances (metals, pesticides, etc.) In water fishery waters;

				<br>• Determination of toxic substances (pesticides, heavy metals, etc.) with the conclusion of conformity to sanitary and hygienic norms level of accumulation of toxicants in fish.
              </p>',


'text3' => '<h2>Directions of scientific and technical activities</h2>
              <p>
                Laboratory employs 9 staff with extensive experience. Staff carry out most of the work on the processing of samples, analysis of the material, the compilation of reports.
                <br><br>
				The laboratory is equipped with all necessary equipment, scientific and methodological literature, specialized software for data analysis.
				<br><br>
				The laboratory staff constantly improve their qualifications studying scientific literature, participation in scientific conferences, seminars, pass training on business lines.
				<br><br>
				The laboratory operates on the basis of evidence on the evaluation of the status of measuring devices in the laboratory, number 99/15 and 98/15 from 12.25.2015, issued by Almaty branch of JSC “National expertise and certification center”.
				<br><br>
				The staff of the laboratory work carried out on the orders of state bodies of Kazakhstan, such as:
				<br><br>
				• Determination of fish productivity of fishery water bodies and / or their parts, the development of biological studies of maximum permissible amounts of withdrawal of fish resources and other aquatic animals and issuing recommendations on the treatment and management of fisheries in the waters of international, national and local significance;
				<br><br>
				• Applied research in the field of fisheries for the conservation and restoration of fish resources and other aquatic organisms in the water bodies of international and republican significance Republic of Kazakhstan;
				<br><br>
				• under economic contracts.
              </p>',

];

?>