<?php 

return [
'head' => 'Laboratory of genetics of hydrobionts',
'manager' => 'Head of the laboratory',
'managername' => 'Shalgimbaeva Gulmira Mukhametkalievna',

'text1' => '<h2>Types of work being performed:</h2>
              <p> 
				<br>• conducting research on the assessment of the population-genetic structure of the species – the objects of the fishery;
				<br>• Conducting genetic analysis of aquatic organisms to determine the species, population and individual genetic variability;
				<br>• Conducting genetic passportization of breeders of sturgeon broodstock, development of genetic passports for breed objects of  artificial reproduction;
				<br>• Conducting scientific analysis of aquatic biological resources and products from  them  to determine the species composition and origin of the material submitted by the customer.
				<br>• carrying out genetic certification of objects, the import and export of which  is governed by CITES rules (Convention International Tradein Endangered Species of Wild Fauna and Flora)
                  </p>',
'text2' => '<p> The laboratory is equipped with all necessary equipment, scientific and methodical literature, specialized programs for data analysis.
                  <br><br>
                  Laboratory staff by studying the scientific literature, participating in scientific conferences, workshops, doing traineeship in directions of activity constantly improve their qualification.
                  <br><br>
                  The laboratory has been operating since 2017. 
                  <br><br>
                  Laboratory staff fulfil work on the orders of Kazakhstan government authorities.
              </p>',
];

?>

