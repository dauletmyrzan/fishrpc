<?php 

return [
'head' => 'Financial – economic department',
'manager' => 'Chief Accountant',
'managername' => 'Uldakhanova Aina Kamalovna',

'text1' => 'The Finance Department is a structural subdivision of LLP “KazNIIRH” and is guided by the Law of RK “On Accounting and Financial Statements” on 28.02.2007.  № 234-111, the SBU, the Code of RK “On taxes and other obligatory payments to the budget” (Tax Code), the Accounting policy and Regulation of financial economic department',

'text2' => '<h2>Tasks</h2>
              <p>
             The main objectives of the department are: Organization of the financial activities of LLP “KazNIIRH” for the purpose of efficient use of financial resources, provision of financial and economic activities of the organization, the formation of complete and accurate information about the financial position, performance and changes in financial position of the organization, providing the necessary financial intelligence , regular and analytical information to internal and external users, prevention of negative results of operations and identify internal reserves to maintain financial stability, control of the presence and movement of property, use of material, labor and financial resources, as well as ensuring timely settlements with creditors, the budget, off-budget funds , insurance funds, bank loans and borrowings, enforcement of financial discipline, timely and complete fulfillment of contractual obligations, expenditure and receipt of revenue.
              </p>',


'text3' => '<h2>Functions</h2>
              <p>
              In accordance with the tasks of Financial Economic Department performs the following functions: maintaining management accounting of the LLP, the formation of a regular management reports, the development and implementation of standards, norms, rules and regulations on management accounting, the formation of accounting policies in accordance with the needs of the LLP, the formation of complete and reliable information about business processes and financial results of the LLP, accrual and transfer of taxes to the republican and local budgets, preparation of tax returns,control of external liabilities of finance, accounting of fixed assets working capital and cash LLP, for timely inventory of cash, inventory items, ensuring documenting of accounts and transactions involving the movement of funds, representing the interests of Ltd. with the relevant authorities when dealing with issues related to accounting, reporting and financial control.
              	<br><br>
				The activity of the financial and economic department carries out in close cooperation with structural subdivisions of LLP “KazNIIRH”, as well as within its competence, in cooperation with other departments of the Ministry of Agriculture, Ministry of Education and Science of the Republic of Kazakhstan, etc.
              </p>',
];

?>