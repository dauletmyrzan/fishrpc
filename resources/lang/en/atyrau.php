<?php 

return [
'head' => 'Atyrau branch',
'manager' => 'Director of the Atyrau branch',
'managername' => 'Kadimov Erbolat Latifovich',

'title1' => 'General information',
'text1' => 'Atyrau branch of LLP “FishRPC” – is a subsidiary of the head of the Institute of LLP “FishRPC” Almaty and the country’s only academic institution that develop biological basis of total allowable catches of bioresources of Ural-Caspian basin.
Implementing the program of basic and applied research for the scientific support agribusiness development branch successfully conducts research in all fisheries waters of the Ural-Caspian basin.
The primary goals of the branch for 50 years is the study of biological resources, stock and reproduction, forecasting production of commercial fish.',

'text2' => 'During the study period 1964 -2015. researchers of the branch of environmental monitoring is carried out in the district. Ural, r.Kigach and the Caspian Sea with a view to the systematic study of hydrology, hydrochemistry, toxicology, forage fish, fish food, fish fauna, as well as the development of practical measures and recommendations to reduce the adverse effects of human activities.
In all years, and so far focused on the conservation of sturgeons. Annually, there are observations of the distribution and the formation of the sturgeon and semi-fish species, their specific structure, feeding conditions. It determined by the dynamics of the spawning run manufacturers of commercial fish in the river. Ural, evaluated the effectiveness of natural reproduction of sturgeons and semi-migratory fish.',


'title3' => 'The direction of the research activities',
'text3' => '<br>• Using the results of fundamental and applied research in the last 30 years, researchers of the Atyrau branch carried out the following activities:

<br>• Continuous monitoring of the biological and ecological processes in fish ponds Ural River, a river discharge r.Kigach with their spaces.

<br>• Designed and made changes to the Fisheries Regulation – setting restrictions and prohibitions on the use of fauna.

<br>• The recommendations on the use of biological resources and fishery regulation with the definition of fish productivity of fishing grounds in the Ural-Caspian basin.

<br>• Recommended measures for fisheries development plan.

<br>• Recommended measures to restore spawning sturgeon of the Ural river.

<br>• Spend certification of spawning sturgeon.

<br>• Conducted research on the size and condition of seals in the North Caspian

<br>• Conducted joint research with Russian, Azerbaijani, Iranian and American scientists',


'title4' => 'Provided by the branch Services',
'text4' => '<br>• Development of recommendations on the development of bio-resources in the North Caspian

<br>• Development of recommendations for land reclamation works in the Ural-Caspian basin.

<br>• Development of recommendations on the evaluation of the Fund spawning sturgeon in the river. Urals, r. Kigach.

<br>• Monitoring study on the definition and status of biological resources in the North Caspian (fish fauna, hydrobiology, fish food).

<br>• Monitoring studies on the number and condition of seals in the North Caspian.

<br>• The definition of damage to fish stocks and the development of compensatory measures',
];

?>