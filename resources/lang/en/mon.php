<?php 

return [
'head' => 'Achievements MES grants',
'head_text' => 'Over the past five years (2016-2020), scientific officers of the FishRPC received 50 security documents for utility models and inventions, more than 170 copyright certificates, there are 2 certificates of state registration of intellectual property (computer programs).',

'title1' => 'Organizational and legal documentation',
'text1' => '<br><br>— The Charter of «FishRPC» LLP, approved by the Ministry of Ecology, Geology and Natural Resources of the Republic of Kazakhstan, order No. 315-P dated 10.12.2020;

			<br><br>— Certificate of Accreditation Limited Liability Company «Fisheries Research and Production Center» “as the subject of scientific and (or) scientific and technical activities, a series of MK № 005483 from 20.03.2019, issued by the Ministry of Education of the Republic of Kazakhstan

			<br><br>— State license No. 02072P dated March 28, 2019 for the performance of work and the provision of services in the field of environmental protection;
			The accreditation certificate of LLР «Fisheries Research and Production Center» is accredited in the accreditation system of the Republic of Kazakhstan for compliance with the requirements of GOST ISO / IEC 17025-2009 
			“General requirements for the competence of testing and calibration laboratories”, No. KZ.T.02.1929 from September 13, Date of change certificate of accreditation February 28, 2019.

			<br><br>— Certificate No. 11/19 about the assessment of the state of measurements of aquaculture laboratory (certification) of the (issued February 18, 2019 by the Almaty branch of the National Center for Expertise and Certification, (valid until December 26, 2021).

			<br><br>— Certificate No. 13/19 about the assessment of the state of measuring instruments (certification) of the ichthyology laboratory (issued December 20, 2018 by the Almaty branch of the National Center for Expertise and Certification, (valid until December 26, 2021).

			<br><br>— Certificate No. 14/19 about the assessment of the state of measuring instruments (certification) of hydroanalytics laboratory (issued February 18, 2019 by the Almaty branch of the National Center for Expertise and Certification, (valid until December 26, 2021)

			<br><br>— Certificate No. 15/19 about the assessment of the state of measuring instruments (certification) of laboratory of hydrobiology (issued February 20, 2019 by the Almaty branch of the National Center for Expertise and Certification, (valid until December 26, 2021)

			<br><br>— Certificate No. 12/19 about the assessment of the state of measuring instruments (certification)of laboratory genetics of hydrobionts (issued February 20, 2019 by the Almaty branch of the National Center for Expertise and Certification, (valid until December 26, 2021).

			<br><br>— The certificate of conformity No. KZ 7500175.07.03.00614 from 22.11.2019  to the requirements of ST RK ISO 9001-2016 “Quality Management System” issued by LLP Kazakhstan Quality Center.

			<br><br>— The certificate of conformity No. KZ 7500175.07.03.00615 from 22.11.2019  to the requirements of ST RK ISO14001-2016 “Environmental management system” issued by LLP Kazakhstan Quality Center.

			<br><br>— The certificate of conformity No. KZ 7500175.07.03.00616 from 22.11.2019  to the requirements of РКOHSAS 18001-2008 ” Occupational Health and Safety Management System” issued by LLP Kazakhstan Quality Center.',

'title2' => 'Patents',
'text2' => '<br><br><h2>2017 year</h2>

			<br>1) Cage for cultivation with the dried cleared small-blade cloth. Patent for utility model No. 2139 of the Republic of Kazakhstan
			<br>2) Deep hydroacoustic fish protection device for dams of small hydropower plants. Patent for utility model No. 2150 of the Republic of Kazakhstan
			<br>3) The method of growing juvenile carp and pikeperch. Patent for utility model No. 2159 of the Republic of Kazakhstan
			<br>4) Device for catching and transporting fish from fixed cages. Patent for utility model No. 2511 of the Republic of Kazakhstan
			<br>5) Device for catching young fish from fry ponds. Patent for utility model No. 2512 of the Republic of Kazakhstan
			<br>6) Artificial breeding ground for sturgeon. Patent for utility model No. 2456 of the Republic of Kazakhstan
			 

			<br><br><h2>2018 year</h2>

			<br>1) Compound feed for trout fry with probiotic preparation. Patent for invention No. 32671 of the Republic of Kazakhstan
			<br>2) Deep fish protection device for flooded intakes of coastal water intakes. Patent for utility model No. 2724 of the Republic of Kazakhstan
			<br>3) Device for incubating fertilized sturgeon roe in natural water conditions. Patent for utility model No. 2709 of the Republic of Kazakhstan
			<br>4) Method of monitoring habitat and protection of valuable species of fish using unmanned aerial vehicles. Patent for utility model No. 2681 of the Republic of Kazakhstan
			<br>5) Fish protection device for small water intakes. Patent for utility model No. 2762 of the Republic of Kazakhstan
			<br>6) Fish protection device for the water discharge structures of hydroelectric dams and reservoirs. Patent for utility model No. 2763 of the Republic of Kazakhstan
			<br>7) Device for degassing and aeration of water in fish plants. Patent for utility model No. 2891 of the Republic of Kazakhstan
			<br>8) The method of incubation of carp / carp caviar. Patent for utility model No. 2925 of the Republic of Kazakhstan
			<br>9) The method of growing fish stock of stellate sturgeon. Patent for utility model No. 3077 of the Republic of Kazakhstan
			<br>10) Device for transporting live perch. Patent for utility model No. 3078 of the Republic of Kazakhstan
			<br>11) Artemia egg collecting device. Patent for utility model No. 3079 of the Republic of Kazakhstan
			<br>12) The method of growing fish stock of Russian sturgeon. Patent for utility model No. 3138 of the Republic of Kazakhstan
			<br>13) The method of estimating the number of fish echo sounder. Patent for utility model No. 3242 of the Republic of Kazakhstan
			<br>14) Improved double-walled fishing net. Patent for utility model No. 3404 of the Republic of Kazakhstan
			<br>15) The method of calculating the number of migratory fish in rivers. Patent for utility model No. 3379 of the Republic of Kazakhstan
			<br>16) A cage for rearing young common fish. Patent for utility model No. 3474 of the Republic of Kazakhstan
			<br>17) The method of estimating the amount of fish caught by recreational fishing. Patent for utility model No. 3475 of the Republic of Kazakhstan
			 

			<br><br><h2>2019 year</h2>

			<br>1) CDevice for rearing trout with direct-flow water supply system. Patent for utility model No. 3603 of the Republic of Kazakhstan
			<br>2) DMethod of obtaining planting material for grass carp in 3-4 fish breeding zone: Patent for utility model No. 3888 of the Republic of Kazakhstan
			<br>3) DMethod for obtaining and incubating fertilized pike perch eggs: Patent for utility model No. 3889 of the Republic of Kazakhstan
			<br>4) MMullet fishing device: Patent for utility model No. 3926 of the Republic of Kazakhstan
			<br>5) FMethod for growing two-year-old pike perch in ponds: Patent for utility model No. 3927 of the Republic of Kazakhstan
			<br>6) FMethod of cultivation of vinegar eel (Turbatrix aceti) as a starter feed for juvenile fish: Patent for utility model No. 4073 of the Republic of Kazakhstan
			<br>7) DInstallation with closed water supply for incubation of carp and herbivorous fish eggs: Patent for utility model No. 4075 of the Republic of Kazakhstan
			<br>8) TMethod of transportation of carp / carp of different ages: Patent for utility model No. 4231 of the Republic of Kazakhstan
			<br>9) TImproved pond for raising early juvenile carp: Patent for utility model No. 4232 of the Republic of Kazakhstan
			<br>10) An improved method for monitoring and protecting large fishery reservoirs: Patent for utility model No. 4236 of the Republic of Kazakhstan
			<br>11) Method of making collectible specimens of fish and other cold-blooded vertebrates: Patent for utility model No. 4404 of the Republic of Kazakhstan
			<br>12) Method of making exhibits of fish, amphibians, reptiles: Patent for utility model No. 4405 of the Republic of Kazakhstan
			<br>13) Channel spawning complex for reproduction of pike perch: Patent for utility model No. 4485 of the Republic of Kazakhstan
			<br>14) Device for mounting the echo sounder sensor: Patent for utility model No. 4493 of the Republic of Kazakhstan
			<br>15) Fishing cap net with constant fishing area: Patent for utility model No. 4486 of the Republic of Kazakhstan
			<br>16) Device for taking benthic samples in reservoirs with different types of bottom relief: Patent for utility model No. 4499 of the Republic of Kazakhstan

			<br><br><h2>2020 year</h2>

			<br>1) CMethod of growing three-year-old pike perch in ponds: Patent for utility model No. 4967 of the Republic of Kazakhstan, А01 / К 61/00 (2006.01), within the framework of project No. АР05130395 (MES)
			<br>2) DImproved removable container for transportation of live juvenile carp: Patent for a useful model No. 4968 of the Republic of Kazakhstan, A01 / K 61/00 (2017.01), in project No. 0147-18-GK (JSC Science Foundation)
			<br>3) DMethod of cultivation of white enchitrea (Enchytraeus albidus) as a live starter food for juvenile fish: Patent for utility model No. 5065 of the Republic of Kazakhstan, A23 / K 1/00 ​​A01K 61/00 (2017.01)
			<br>4) MImproved rakolovka-trap: Patent for utility model No. 5164 of the Republic of Kazakhstan, А01К 69/08 (2006.01)
			<br>5) FImproved hydrobiological beam trail for catching bottom and bottom organisms: Patent for utility model No. 5179 of the Republic of Kazakhstan, A01M 23/02 (2006.01)
			<br>6) FImproved pond for keeping juveniles of pike perch: Patent for utility model No. 5163 of the Republic of Kazakhstan, А01К 61/00 (2006.01), within the framework of project No. 0008-17-ГК (JSC Science Fund)
			<br>7) DMethod for determining the number of fish in water bodies of local importance: Patent for utility model No. 5219 of the Republic of Kazakhstan, А01К 69/00 (2006.01)
			<br>8) TAn improved method for determining the catchability of fixed nets in scientific fishing: Patent for utility model No. 5220 of the Republic of Kazakhstan, А01К 61/00 (2006.01)
			<br>9) TAn improved way of growing carp underyearlings in ponds: Patent for a useful model No. 5221 of the Republic of Kazakhstan, A01K 61/00 (2006.01), in raikas of project No. 0147-18-GK (JSC Science Fund)
			<br>10) Method for pasting sturgeon caviar: Patent for utility model No. 5223 of the Republic of Kazakhstan, А01К 61/00 (2006.01)
			<br>11) Method for combined estimation of fish abundance with fixed nets and echo sounder: Patent for utility model No. 5222 of the Republic of Kazakhstan, А01К 69/00 (2006.01)
			<br>12) An improved method for breeding crayfish in shallow lakes: Patent for utility model No. 5189 of the Republic of Kazakhstan, A01K 61/00 (2006.01)
			<br>13) A method for increasing the efficiency of stocking of reservoirs: Patent for a useful model No. 5273 of the Republic of Kazakhstan, MPK A01K 61/00 (2006.01)
			<br>14) Method for obtaining starting compound feed for larvae and juvenile zander: Patent for invention No. 34532, IPC А01К 61/10 (2017.01), А23К 50/80 (2016.01), А23К 40/25 (2016.01)
			<br>15) Improved hydrobiological bucket bottom grab: Patent for utility model No. 5266 of the Republic of Kazakhstan, MPK А01М 23/02 (2006.01)
			<br>16) Mobile installation with closed water supply for rearing juveniles of valuable fish species: Patent for utility model No. 5333 of the Republic of Kazakhstan, IPC А01К 61/00 (2006.01)
			<br>17) Eco-friendly backwater fishing seine: Patent for utility model No. 5618 of the Republic of Kazakhstan, MPK А01К 69/00 (2006.01)
			<br>18) Within the framework of the commercialization project No. 0147-18-ГК (JSC Science Fund)',

'title3' => 'Scientific publications',
'text3' => '<h2>Scientific publications in journals, indexed in the Scopus and Web of Science databases</h2>
                      <br><br><h2>2016 year</h2>
                      Study of the influence of combined feed of domestic production of trout micro flora// Shalgimbayeva Saule, Isbekov Kuanysh, Assylbekova Saule, Sadvakasova Asem, Akmuhanova Nurziya, Koishybayeva Saya // journal of biotechnology/Том:Стр.:S5051.Приложение:S.
                      <br><br>Kurzhykayev Zhumagazy., Akhmedinov Serikbay N. // Quantitave development and distribution of zooplankton in medium lakes of the kostanay reqion (north Kazakhstan reqion) // International. journal.of environmetal. et science education 2016.-vol.ll.-№ 15. – 8193-8210
                      
                      <br><br><h2>2017 year</h2>

                      Levin, Boris,   Levina, Marina,   Interesova, Elena,  Pilin D.V. Phylogeny and phylogeography of the roaches, genus Rutilus (Cyprinidae), at the Eastern part of its range as inferred from mtDNA analysis// Hydrobiologia (2017) -Т.788. –P.33-46 ISSN:0018-8158
                      <br><br>Assessment of impacts and potential mitigation for icebreaking vessels transiting pupping areas of an ice-breeding seal//Susan C. Wilsona,⁎, Irina Trukhanovab, Lilia Dmitrievac, Evgeniya Dolgovad, Imogen Crawforda,Mirgaliy Baimukanove, Timur Baimukanove, Bekzat Ismagambetove, Meirambek Pazylbekovf,Mart Jüssig, Simon J. Goodmanc,// Biological Conservation 214, October 2017, Pages 213–222
                      <br><br>Comparative assessment of food resources of valuable Sander lucioperca m the natural habitat and in ponds of the south-eastern Kazakhstan// Zhanara Mazhibayeva, Sayle Asylbekova, Larisa Kovaleva, Tynysbek Barakbayev and Luidmila Scharapova / EM International. Ecology, Environment and Conservation Paper. Vol 23, Issue 3, 2017; Page No.(l728-1737).
                      <br><br>Тhe results of nile tilapia (Оreochromis niloticus l.) Breeding in pond farm of almaty region using locally made experimental productive food// Damir Kayerkeldiyevich Zharkenov, Kuanysh Baybolatovich Isbekov, Toleukhan Sadykulovich Sadykulov, Jоzsef Pekli and Nina Sergeyevna Badryzlova // Ecology environment and conservation journal 0971765X- India Vol 23, Issue 3, 2017; Page No.(1273-1280)
                      <br><br>Risk assessment of pet-traded decapod crustaceans in the Republic of Kazakhstan, the leading country in Central Asia//Uderbayev, T (Uderbayev, Talgat)[ 1 ] ; Patoka, J (Patoka, Jiri)[ 2 ] ; Beisembayev, R (Beisembayev, Ruslan)[ 1 ] ; Petrtyl, M (Petrtyl, Miloslav)[ 2 ] ; Blaha, M (Blaha, Martin)[ 3 ] ; Kouba, A (Kouba, Antonin)[ 3 ]/ KNOWLEDGE AND MANAGEMENT OF AQUATIC ECOSYSTEMS//Выпуск:418, Номер статьи:30,
                       
                      <br><br><h2>2018 year</h2>

                      <br><br>Structural Indicators of Zooplankton in the Shardara Reservoir (Kazakhstan) and the Main Influencing Factors/ Elena Krupa, Sophia Barinova, Saule Assylbekova, Kuanysh Isbekov// Turkish Journal of Fisheries and Aquatic Sciences 18: 659-669 р. (2018) trjfas.org
                      <br><br>The use of zooplankton distribution maps for assessment of ecological status of the Shardara reservoir (Southern Kazakhstan)// Elena Krupa, Sophia Barinova, Kuanysh Isbekov, Saule Assylbekova//Ecohydrology & Hydrobiology, Volume 18, Issue 1, January 2018, Pages 52-65.
                      <br><br>Morfometric parameters of a three-year-old pikeperch (Stizostedion lucioperca) grown in pond farmin in the Almaty region in a polyculture with carp and herbivorous fish// Saya Koishybayeva, Shokhan Alpeisov, Saule Assylbekova, Tynysbek Barakbayev //EurAsian Journal of BioSciences Eurasia J Biosci 12, 69-75 (2018)
                      <br><br>The application of phytoplankton in ecological assessment of the balkhash lake (Kazakhstan). BARINOVA, S. – KRUPA, E. – TSOY, V. – PONAMAREVA, L., Applied Ecology and Environmental Research 16(3) · June 2018 with197 Reads DOI: 10.15666/aeer/1603_20892111
                      
                      <br><br><h2>2019 year</h2> 

                      <br><br>Growth and length-weight relationships of Aral Sazan Cyprinus Carpio Aralensis Spishakow, 1935 (Cyprinidae; Osteichthyes) in the Sarysu River Watershed//Vladimir Krainyuk, Saule Assylbekova, Olga Kirichenko, Kuanysh Isbekov, Talgat Abzhanov// Eurasian Journal of Biosciences, 2019 — Volume 13 Issue 1, pp. 533-539
                      <br><br>Assessment of the production potential of two-year-old pike-perch cultivated in ponds for the formation of RBS// Nina Badryzlova, Saya Koishybayeva, Saule  Assylbekova, Kuanysh Isbekov// Eurasian Journal of Biosciences, 2019 — Volume 13 Issue 1, pp. 409-417
                      <br><br>Influence of different feeds and feed additives on fish-breeding and biological indicators at rearing rainbow trout// A. A. Aitkaliyeva, Sh. A. Alpeisov, K. B. Isbekov, S. Zh. Assylbekova, N. S. Badryzlova// Eurasian Journal of Biosciences, 2019 — Volume 13 Issue 1, pp. 437-442
                      <br><br>Impacts of water level changes in the fauna, flora and physical properties over the Balkhash Lake watershed /B. Isbekov, Kuanysh & N. Tsoy, Vyacheslav & Cretaux, J & V. Aladin, Nikolai & Plotnikov, Igor & Clos, Guillaume & Bergé-Nguyen, Muriel & Zh. Assylbekova, Saule. (2019). Lakes & Reservoirs: Research & Management. 10.1111/lre.12263. Lakes & Reservoirs: Research & Management. 10.1111/lre.12263. (2019). Lakes & Reserv. 2019;00:1–14., © 2019 John Wiley & Sons Australia, Ltd. wileyonlinelibrary.com/journal/lre.
                      <br><br>Caddisfly Assemblages in Metal Contaminated Rivers of the Tikhaya Basin, East Kazakhstan / Anna A. Evseeva, Liubov V. Yanyginа// Bulletin of Environmental Contamination and Toxicology. – Том: 102. – Выпуск: 3. – Стр.: 316-322,https://doi.org/10.1007/s00128-019-02561-w, https://rdcu.be/blBUV.
                      <br><br>Actual status of fishing reserves of the Yesil River // Zhumagazy Kurzhykayev, Kuanysh Syzdykov, Ainur Assylbekova, Dinara Sabdinova, Viktor Fefelov // ZOOLOGIA. – Том 36: 1-9, № e30437, ISSN 1984-4689 (online),   https://zoologia.pensoft.net/article/30437/.
                      
                      <br><br><h2>2020 year</h2>

                      <br><br>Метод учета и определения линейных размеров каспийских тюленей (Pusa caspica) на лежбищах с помощью мультикоптеров // М. Т. Баймуканов, Л. А. Жданко, Т. Т. Баймуканов, Е. С. Дауенев, С. Е. Рыскулов, А. М. Баймуканова // Зоологический журнал. – 2020. — Том 99. – № 2. – С. 215–222.
                      <br><br>Evaluation of the habitat state of the Zhaiyk River Ichthyofauna in modern conditions and its influence on the impacts of anthropogenic factors //  Saule Assylbekova, Kuanysh Isbekov, Damir Zharkenov, Yevgeniy Kulikov, Yerbolat Kadimov, Olga Sharipova// Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 467-473.
                      <br><br>Technology of formation of replacement-brood stock of pikeperch in conditions of fish farms in Kazakhstan // Nina Badryzlova, Saya Koishybayeva, Saule Assylbekova, Kuanysh Isbekov// Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 441-447.
                      <br><br>Prospects for growing juveniles and rearing fingerlings of pikeperch (Sander lucioperca) in cages in the conditions of fish farming of Almaty region // Gulmira M. Ablaisanova, Saule Zh. Assylbekova, Adilkhan Ab. Sambetbaev, Piotr J. Gomulka, Kuanysh B. Isbekov, Nina S. Badryzlova, Saya K. Koishybayeva // Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 293-299.
                      <br><br>Technology of cultivation of feeder fish for culturing tilapia (Tilapia) and clarid catfish (Clarias gariepinus) in the VI fish-breeding zone of Kazakhstan // Z.T. Bolatbekova, S.Zh. Assylbekova, B.T. Kulatayev, T. Policar, K.B. Isbekov, S.К. Koishybayeva // Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 475-481.
                      <br><br>Use of domestic starter feeds for culturing clarid catfish and tilapia // Alyona Mukhramova, Saule Assylbekova, Adilkhan Sambetbaev, Tomáš Policar, Kuanysh Isbekov, Saya Koishybayeva, Nina Badryzlova // Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 453-458.
                      <br><br>A new strain group of common carp: The genetic differences and admixture events between Cyprinus carpio breeds // Nedoluzhko, Artem; Slobodova, Natalia; Sharko, Fedor; Shalgimbayeva, Gulmira; Tsygankova, Svetlana; Boulygina, Eugenia; Jeney, Zsigmond; Nguyen, Van Quan; Nguyen, Đức Thế Thế; Phạm, Thế Thư; Volkov, Alexander; Fernandes, Jorge; Rastorguev, Sergey // Ecology and Evolution, 2020;00:1–9.
                      
                      <br><br><h2>2021 year</h2> 

                      Abdybekova AM, Assylbekova SZh, Abdibayeva AA, Zhaksylykova AA, Barbol BI, Aubakirov MZh, Torgerson PR (2021). Studies on the population biology of helminth parasites of fish species from the Caspian Sea drainage basin. Journal of Helminthology 95, e12, 1–8. https://doi.org/10.1017/S0022149X2100002X
                      <br><br>Aubakirova, M.; Krupa, E.; Mazhibayeva, Z.; Isbekov, K.; Assylbekova, S. The Role of External Factors in the Variability of the Structure of the Zooplankton Community of Small Lakes (South-East Kazakhstan). Water 2021, 13, 962. https://doi.org/10.3390/w13070962',


'title4' => 'Commercialization',
'text4' => '<br><br><h2>2017 year</h2>

                      	In 2017, FishRPC LLP developed a technology for the production of fish planting material and commercial product of pike perch, which has a world-wide novelty. The pilot project “An artificial reproduction, a cultivation and a sale of a fish stock of a pike perch for stocking of natural reservoirs” to obtain pike perch fry for stocking natural reservoirs is implemented by research associates of FishRPC in Kyzylorda region. The design capacity of 240 thousand perch of year perch.

						The implementation of the project on commercialization of pike perch will help maintain the commercial stocks of this fishery object, stabilize catches and supply of commercial products of perch to the domestic and foreign markets, which will enhance the export opportunities of Kazakhstan. This project is being implemented with the support of JSC “Science Foundation”.

                      <br><br><h2>2018 year</h2>

                    	In 2018, the institute received a grant through the Science Fund of the Ministry of Education and Science of the Republic of Kazakhstan for the project: «An artificial reproduction and cultivation of fish stock carp for the purpose of its further implementation for stocking natural reservoirs of the Republic of Kazakhstan».

						The stocking of natural reservoirs of Kazakhstan with viable fish stock of carp will help maintain the commercial stocks of this fishing object, stabilize catches and supply of carp products to the domestic market of Kazakhstan. The proposed technology of breeding and growing fish stock carp is effective for use in fish farms in Kazakhstan.',

'title5' => 'Grant from MES of the Republic of Kazakhstan (KMU) for study Artemia franciscana on 2021-2023 years.',
'text5' => '<h2>IRN AP09058158 Study of export-oriented bioresource — Artemia franciscana for biological justification development on their introduction, to increase productivity of RK saline waterbodies.</h2>
                      <h2>Relevance.</h2>
                      This work will provide an opportunity to increase the productivity of bitter-salt water reservoirs of the country by implementation a new biological species for Kazakhstan - Artemia franciscana. The novelty of the work lies in the fact that there has been no work aimed at increasing the stocks and quality of Artemia’s cysts so far this year in Kazakhstan.
                      <br><br>
                      <h2>Target.</h2>
                      To study opportunities, the main criteria of crustacean Artemia franciscana survival and to compare productional characteristics of two populations of valuable bioresource - Artemia parthenogenetica, Artemia franciscana, for the purpose of increase in productivity bitter-salty RК reservoirs, for receiving high-quality competitive products for implementation in the internal and external markets.
                      <br><br>
                      <h2>Expected results.</h2>

	                    The implementation of the tasks will help to reveal the interpopulation competitiveness of two populations of crustaceans, the degree of their survival, as a consequence, which will determine the prevalence of more productive populations. There are practically no studies that determine the economic aspects of the cultivation of Artemia in the regions of the Republic of Kazakhstan, and therefore, there is a need to conduct work to assess the influence of various factors on the productional population characteristics of Artemia franciscana during the introduction of Artemia cysts into natural water bodies.
                      <br><br>
                      <h2>The achieved results for half a year.</h2>

                      We searched for suppliers and signed contracts for the supply of equipment and consumables, the equipment and consumables for implementation of the project are purchased. A survey of the physical-geographical and morphological characteristics of the bitter-saline reservoirs of the Petropavlovsk (Mengesor and Stanovoe lakes) and Pavlodar (Sharbakty, Seyten, Kazy and Kalatuz lakes) regions with use of navigation devices, unmanned aerial vehicles, etc. has begun. For each reservoir, coordinates, measurements of depths, water salinity, temperature and water transparency were received. An assessment of the hydrochemical regime of the selected reservoirs of Kazakhstan was carried out, the material on hydrobiology, hydrochemistry, genetics and biotechnology was collected. 
                      <br><br>
                      <h2>Full names of members of the research team with their identifiers (Scopus Author ID, Researcher ID, ORCID, if any) and links to the corresponding profiles:</h2>
                      <br><br> <h2>Barakbayev T.T.,</h2> doctor of philosophy (PhD), director of the Aral branch of SPCF LLP, project manager, https://orcid.org/0000-0002-9047-5274, https://www.scopus.com/authid/detail.uri?authorId=56974343200;
                      <br><br> <h2>Mazhibaeva Zh.O.,</h2> doctor of philosophy (PhD), head of the laboratory of hydrobiology and hydroanalytics of SPCF LLP, https://orcid.org/0000-0002-5013-0503, https://www.scopus.com/authid/detail.uri?authorId=57200420063;
                      <br><br> <h2>Fefelov V.V.,</h2> head of the support base of Kostanay city, https://orcid.org/0000-0002-1916-393X, https://www.scopus.com/authid/detail.uri?authorId=57211521877;
                      <br><br> <h2>Minat A.,</h2> head of the expeditionary team of the laboratory of hydrobiology and hydroanalytics of SPCF LLP, master of degree;
                      <br><br> <h2>Kozhizhanova B.A.,</h2> science researcher of laboratory of hydrobiology and hydroanalytics of SPCF LLP, master of degree, https://orcid.org/0000-0002-9012-0406;
                      <br><br> <h2>Молдрахман А.С.,</h2> science researcher of laboratory of hydrobiology and hydroanalytics of SPCF LLP, performer, undergraduate, https://orcid.org/0000-0002-9619-4262;
                      <br><br> <h2>Bolatbekova Z.T.,</h2> head of the information and innovation department of SPCF LLP, 3rd year doctoral candidate, https://orcid.org/0000-0001-8766-662X, https://www.scopus.com/authid/detail.uri?authorId=57216550134;
                      <br><br> <h2>Bocharova E.S.,</h2> specialist geneticist of VNIRO, https://www.scopus.com/redirect.uri?url=https://orcid.org/0000-0001-9978-3006, https://www.scopus.com/authid/detail.uri?authorId=54683458400.
                      <br><br>
                      <h2>Information for potential users.</h2>
                      	The work on the project is aimed at increasing the stocks and quality of Artemia cysts, which has not been available in the Republic of Kazakhstan until now. The implementation of the project is important for the national economy.Kazakhstan possesses 10-15% of artemia world reserves, the production limit cyst from republic reservoirs for 2019 is set in the amount of 1492.81 tons (limit/quota) [3, 4]. The main part of the specified volume is made by Pavlodar region lakes (1116.0 tons per year). The development and increase in the productivity of the biological resource of Artemia cysts in the salt water bodies of Kazakhstan can be one of the main directions of the development of domestic production of starter bio-feeds of aquatic origin for the needs of fish farming and aquaculture.
                      <br><br>',

'title6' => 'Grant from MES of the Republic of Kazakhstan (KMU) for development of the fish-protecting device on 2021-2023 years.',
'text6' => '<h2>IRN АР09058066 «Development of the active mobile hydroacoustic fish-protecting device for protection of juveniles from hit in spillways of HPS dams, reservoirs»</h2>

                      <h2>Рremises to development of the project.</h2>
                      Now effective fish-protecting devices are not made for protection of fishes and juveniles of fishes against hit in water waste constructions of dams of large hydroelectric power stations and water reservoirs. The water waste constructions of large water-engineering systems, are characterized ultrahigh (up to 500 m 3 /s) volumes of the dumped water. And dumping of water can go in the volley way - when there is technical need, for short period large amount of water is dumped. At the same time on the big site before spillway quickly there is powerful current of the flowing-away water which tightens through water waste the channel all fish and whitebaits. Traditional types of fish protection are of little use here, in view of extreme conditions of volley spillway. The scientific novelty of the project consists in implementation of new approach in the field of fish-protecting devices - the active mobile high-intensity acoustic fish protection capable to effectively protect fishes and juveniles on spillways of dams of large hydroelectric power stations and water reservoirs.
                      <br><br>
                      <h2>The project purpose.</h2>
                      	Тo develop the effective fish-protecting device (further FPD), for protection of fishes and juveniles of fishes from hit in water waste constructions of hydroelectric power station and other large water-engineering systems.
                      <br><br>
                      <h2>The expected results.</h2>
                      	А development of the effective fish-protecting device for large water-engineering systems with volley spillways, production and testing of prototype, execution of the package of documents for production and operation.
                      <br><br>
                      <h2>The achieved results for half a year.</h2>

                     	The research of hydrological and icthyological conditions of a reservoir and spillway of a dam is conducted. For these purposes in quality of a model reservoir the Kirov reservoir UKOOS in the West Kazakhstan region is chosen. As a model spillway the dam of the Kirov reservoir was chosen. According to the plan of performance of hydrological researches 75 measurements of depths and 85 measurements of speed of a current, on the water area of a reservoir and around a water waste construction of a dam were carried out. According to the plan of performance of icthyological researches 10 scientific lov were carried out, concentration and ways of migrations of fishes were established. Were taken on determination of specific, age and dimensional and weight structure of a fish fauna of 320 copies of fishes. To determine the concentration, migrations and downstream migration of juvenile fish, 20 samples of early juveniles were taken with the help of the Russ’s circle, and 10 samples of late juveniles with the help of juvenile drag device. As a result of the conducted researches, basic hydrological and icthyological data in order that it was possible to make the correct decisions on a design, complete set and mode of operation of the active mobile hydroacoustic fish-protecting device were obtained.
                      <br><br>
                      As a result of the research, the initial hydrological and ichthyological data were obtained for making the right decisions on the design, configuration and operation of an active mobile hydroacoustic fish protection device.
                      <br><br>

                      <h2>Full names of members of the research team with their identifiers (Scopus Author ID, Researcher ID, ORCID, if any) and links to the corresponding profiles:</h2>

                      <br><br> Tumenov A.N., director of the West Kazakhstan branch of SPCF, https://orcid.org/0000-0001-7995-2001; Web of Science Researcher ID - AAU-4496-2020;https://app.webofknowledge.com/ author/#/record/10019388?lang=ru_RU&amp;SID=F43MOImdIjBQwD1lXBJ,https://www.scopus.com/authid/detail.uri?authorId=57193997565;
                      <br><br> Pilin D.V., senior science researcher of the West Kazakhstan branch of SPCF, https://orcid.org/0000-0003-4188-1185; Web of Science Researcher ID - AAK-9968-2020; http://apps.webofknowledge.com/WOS_GeneralSearch_input.do?product=WOS&amp;search_mode=GeneralSearch&amp;SID=E1oHp3RxGWKIOCrGl5N&amp;preferencesSaved=; https://www.scopus.com/authid/detail.uri?authorId=57191248333&amp;eid=2-s2.0-84988378030
                      <br><br> Mukhramova A.A., chief scientific secretary, https://orcid.org/0000-0002-4701-6195; Web of Science Researcher ID — AAU-9605-2020; https://www.scopus.com/authid/detail.uri?authorId=57216540827
                      <br><br> Tuleuov A.M., head of the expeditionary team of the West Kazakhstan branch of SPCF LLP, https://orcid.org/0000-0003-3313-5203
                      <br><br> Bulekov N.U., science reasearcher of the West Kazakhstan branch of SPCF LLP;
                      <br><br> Bayandina A.M., head economist, https://orcid.org/0000-0003-4975-0681/

                      <h2>Information for potential users.</h2>

                      	This type of fish protection device can be used in the field of water and agriculture for water users, at large hydroelectric power plants and reservoirs, to increase the safety level of fish fry. According to the existing Water code of the Republic of Kazakhstan operation of water intakes without FPD is not allowed. This development is directed to the solution of pressing problem of equipment of spillways of dams of large hydroelectric power stations and water reservoirs (for example, a cascade of hydroelectric power plants on the Ertys river, Kapshagai hydroelectric power station, dams of reservoirs of the Aktobe Sea, Shardara, UKOOS, etc.), effective domestic FPD.
                      <br><br>',

'title7' => 'Scientific and technical program &quot;Aquaculture&quot; (PCF) on 2021-2023 years.',
'text7' => '',

'title8' => 'Scientific and technical program &quot;Natural Resources&quot; (PCF) on 2021-2023 years.',
'management' => 'Руководство',
];

?>