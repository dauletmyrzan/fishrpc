<?php 

return [
'head' => 'North Kazakhstan branch',
'manager' => 'Director of the Northern Branch',
'managername' => 'Shutkarayev Azis Vasilyevich',

'title1' => 'General information',
'text1' => 'North Branch LLP “FishRPC” conducts research and development in the fisheries sector in all of Northern and Central Kazakhstan. Provides monitoring of the ecological status of water bodies of the North Kazakhstan, Kostanay, Akmola and Karaganda regions, conduct appraisal of reservoirs to determine their use of the fishery.

Specialists of the Northern Branch of “FishRPC” LLP are actively involved in international and national seminars and conferences that discuss the main issues – aquatic ecology, conservation, reproduction and rational use of fish and other aquatic biological resources fishery waters.',

'title2' => 'The direction of the research activities',
'text2' => '<br>• Monitoring of ecological status of water bodies of the North Kazakhstan, Akmola, Karaganda and Kostanay regions.

<br>• The study of the genetic structure of populations of key species of fish in the waters of the North Kazakhstan, Akmola, Karaganda and Kostanay regions.

<br>• The introduction of the achievements of science in fisheries production.

<br>• Passage of educational, scientific – practical training for students of the Kazakh Agrotechnical University S.Seifullin specialty “Fisheries and commercial fishing.”',

'title3' => 'Provided by the branch Services',
'text3' => '<br>• Conducting appraisal of reservoirs for the purpose of their use of the fishery.

<br>• Determination of damage to fisheries by human impact.

<br>• Development of recommendations and biological studies for stocking ponds in the region and acclimatization of commercial species of fish and food organisms.

<br>• Work on eco-design and standardization.

<br>• Development of biological studies of the total allowable catch of fish (TAC).

<br>• Development of recommendations on Fisheries device fishing grounds of nature.

<br>• Evaluation of forage resources reservoirs.

<br>• Development of biological studies and forecast production limit of Artemia cysts, and other valuable aquatic invertebrates.

<br>• Methodical assistance, advice on matters of Hydrobiology.

<br>• Development projects to identify standards for maximum permissible discharges (MPD) for enterprises and reproduction processing of the fishing industry.

<br>• Carrying out diagnostic and therapeutic – health work in the fish farms.

<br>• Scientific substantiation in the environmental certification of reservoirs.

<br>• Development of technical – economic feasibility studies for the development of fisheries and the organization of specially protected natural territories.

<br>• Complex scientific – methodological support and practical support for the organization of commercial fish farming biological rationale for the organization and management of commercial fish farming, with the definition of optimal directions and aquaculture technology.

<br>• Assistance in the preparation of the business – plan for hatcheries and farms.

<br>• Advisory services for bioengineering commodity cultivation sturgeon.

<br>• Consultation and participation of specialists in artificial reproduction of carp (carp), herbivorous and whitefish.

<br>• Advice for employers on the management of fish farms.

<br>• Engineering work on the creation of farming fish farms.',
];

?>