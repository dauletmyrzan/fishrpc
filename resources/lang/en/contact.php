<?php

return array (
    'seo' => 'Head',
    'priemnaya' => 'Reception',
    'buh' => 'Head accountant',
    'address' => 'Address',
    'index' => 'Zip code',
    'phone' => 'Phone',
    'zav' => 'Head of laboratory',
    'zavaral' => 'Head strong point of Aral branch',
    'zav_altay_lab' => 'Head of Integrated Fisheries Laboratory',
    'zav_altay_strong' => 'Head strong point Pavlodar s.',
    'zav_petropavl_strong' => 'Head of the reference point Petropavlsk',
    'zav_karagenda_strong' => 'Head of the reference point Karaganda',
    'zav_kostanay_strong' => 'Head of the reference point Kostanay',
);
