<?php 

return [
'head' => 'Altai branch',
'manager' => 'Director of the Altai branch',
'managername' => 'Aubakirov Baurzhan Savetovich',
'title1' => 'GENERAL INFORMATION',
'text1' => 'Altai branch LLP KazNIIRH provides scientific support for the intensification and sustainable development of fisheries in the region, biodiversity conservation reservoirs Zaisan-Irtysh basin and lake systems of the East Kazakhstan and Pavlodar regions.
<br><br>
The subject of activity of the branch is a research and scientific-organizational activities carried out on a commercial basis by the state order, and economic production and business and other income-generating activities and are not prohibited by the legislation of the Republic of Kazakhstan.',
'text2' => 'Scientists branch of complex scientific research is held annually in major fishing waters of the East Kazakhstan and Pavlodar regions.Due to market relations were the conditions for the development of entrepreneurship, provision of information, consulting and analytical services to interested businesses and individuals.The quality of our products is guaranteed by a specialized intellectual status of fisheries science, high professional qualification of performers, provided enough material and technical base for research and analysis.In the State of the branch there are scientific staff with expertise in hydrochemistry, hydrobiology, ichthyology, pisciculture, the scientific basis of environmental management, the bulk of the staff consists of young cadres.',


'title3' => 'AREAS OF SCIENTIFIC AND TECHNOLOGICAL ACTIVITIES',
'text3' => '<br>• environmental monitoring of biodiversity and aquatic habitat ponds Upper-Irtysh basin;

			<br>• definition fish productivity of reservoirs and their plots, development of biological studies of maximum permissible amounts of withdrawal of fish resources and other aquatic animals issuance Guiding the regime and management of fisheries and aquatic international national and local significance Zaisan-Irtysh basin;

			<br>• development of scientific bases of conservation and sustainable use of biological resources and fishery water bodies in the region of the gene pool of rare and most valuable species and species of fish;

			<br>• Development of the annual forecast of the biological status of the fish population and recommendations for regulating and improving the fishing conditions;

			<br>• improving the technology of artificial breeding of fish and normative and technical documentation;

			<br>• introduction of achievements of science in fisheries production;

			<br>• promoting the development of fisheries in all hatcheries, regardless of their form of ownership;

			<br>• providing scientific methodological and practical assistance to industry organizations and enterprises.',


'title4' => 'PROVIDED BY THE BRANCH SERVICES',
'text4' => '<br>• development of scientific bases of conservation and sustainable use of biological resources fishery waters;

<br>• preservation of the gene pool of rare and most valuable species and species of fish;

<br>• assessment of fish stocks fishery reservoirs and issuing recommendations on the sustainable use of biological resources;

<br>• study of the composition and the degree of development of the pelagic invertebrates and bottom with reconnaissance and monitoring of water bodies;

<br>• definition of productivity and food capacity for fish plankton and benthic communities of aquatic organisms;

<br>• analysis of the relationship of food fish in the pond is necessary for forecasting fish stocks;

<br>• study of water quality hydrochemical parameters (pH, dissolved gases, ion-salt composition, nutrients);

<br>• study materials for a comprehensive assessment of the suitability of water for different economic needs, taking into account the accepted international and national standards;

<br>• Development of commercial fish farming technology in ponds;

<br>• biological basis for the organization of new and reconstruction of existing fish farms;',
];

?>