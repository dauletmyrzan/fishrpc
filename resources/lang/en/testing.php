<?php 

return [
'head' => 'Testing center',
'manager' => 'Head of Testing Center',
'managername' => 'Aitkaliyeva Аigerim Aitkaliyevna',
'text1' => 'Test center (TC)  established in LLP ” Fisheries Research and Production Center” (Order No. 22 from May 3, 2016) and acts on the basis of “Regulations about the Testing center”.
<br><br>
			It has an accreditation certificate certifying that the test center meets the requirements of GOST ISO / IEC 17025-2009 and is accredited as a test center.
<br><br>
			Accreditation certificate № KZ.Т.02.1929 from September 13, 2017.
			In Test center in order to determine technical competence specially developed procedures are used.
<br><br>
			– A comprehensive expert assessment of all factors influencing the development of test data is carried out, based on the international standard ISO/МЭК 17025-2009, used to evaluate the test center worldwide. This standard is used by accreditation administrations to evaluate the following factors:</p>',

'text2' => '<p> <br>– staff competency;

                  <br>– validity and acceptability of test methods;

                  <br>– Serviceability of the test equipment;

                  <br>– environmental testing;

                  <br>–  selection, processing and transportation of the tested samples;

                  <br>– quality assurance of the test data.</p>  

              <p>Test center (TC) is equipped with all necessary measuring and testing equipment, has regulatory documentation.
                <br><br>
        Test center (TC)staff consists of highly qualified specialists with appropriate education, training and experience in testing.
        <br><br>
        In Test center (TC), based on the requirements of ISO/IEC 17025:2009, a quality system has been developed and implemented. Strict control of each element of the system from the moment of concluding treaties to the moment of issue of the test report prevents the possibility of issue erroneous results.</p>'
];

?>