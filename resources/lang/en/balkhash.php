<?php

return array (
    'head' => 'Balkhash branch',
    'manager' => 'Director of the Balkhash branch',
    'managername' => 'Kumataev Erkynbek Yerikovich',
    'title1' => 'General information',
    'text1' => 'Balkhash branch of LLP “FishRPC” constantly monitors the state of the Balkhash-Ile pool ( Balkhash, estuary r. Ile and r. Ile in the lower reaches) and water Karaganda region, developing a biological substantiation of rational use and reproduction of fish stocks, gives annual forecasts of volumes catch fish, proposals for rules, regulations and fishing mode.Every year, on the basis of a branch of scientific evidence comes out the Government Decision on limits of catch of fish from ponds Ile-Balkhash basin and Karaganda region. Accumulated many years of data bank on hydrology, hydrobiology, hydrochemistry and composition of fish fauna of the studied reservoirs.',
    'text2' => 'As part of the branch has a complex fishery laboratory with sectors: Sector for Hydrology, hydrochemistry, forage fish and ichthyology.
Experts Balkhash branch of LLP “FishRPC” take an active part in international and national seminars, conferences and exhibitions, highlighting the problems and the results of its operations.',
    'title3' => 'The direction of the research activities',
    'text3' => '<br>• Monitoring of ecological status of water bodies Ili-Balkhash basin and Karaganda region.

<br>• Study phenogenetically population structure of the main commercial species of fish in ponds Ile-Balkhash basin and Karaganda region.

<br>• Creating a scientific basis for the use of the precautionary approach to fisheries management in the waters of Kazakhstan.

<br>• Rationale for the creation and development of a network of protected areas for the conservation of fish biodiversity.

<br>• The introduction of the achievements of science in fisheries production.',
    'title4' => 'Provided by the branch Services',
    'text4' => '<br>• Conducting appraisal of reservoirs for the purpose of their use of the fishery.

<br>• Calculation of damage to fisheries as a result of economic activity.

<br>• Development of recommendations and biological studies for stocking ponds in the region, acclimatization of commercial species of fish and food organisms.

<br>• Comprehensive survey of the different types of water bodies (reservoirs, ponds, streams, etc.) with a conclusion about the suitability of water for various household needs, including fisheries management on hydrochemical and toxicological indicators.

<br>• Determination of hydrochemical indices of water; pH, dissolved gases, nutrients and organic matter, minerals, their compliance with rules of the MPC forecast exposure ponds kill phenomena.

<br>• Determination of the content of toxic substances (pesticides, heavy metals, etc.) in water and aquatic organisms with issuing opinions on the conformity level of accumulation of contaminants on the health and safety standards.

<br>• Development of biological justification of maximum permissible volume of fish catch (TAC).

<br>• Conducting monitoring studies of the state of the lower aquatic organisms – invertebrates and algae in different types of water bodies.

<br>• Quantitative assessment of the food resources of water bodies.

<br>• Identify the degree of consumption of food organisms and fed the fish in natural waters.

<br>• Expert assessment of the communities of the lower aquatic ponds on literary and archival materials.

<br>• Assessment of water pollution by organic substances as lower aquatic organisms – indicators saprobity water column and soil.

<br>• Review of biological studies, scientific research and projects polytypic reservoirs with the presence of hydrobiological direction.

<br>• Methodical assistance, advice on hydrology, hydrochemistry, hydrobiology, ichthyology.

<br>• Scientific substantiation in the environmental certification of reservoirs.

<br>• The study and evaluation of the epizootic situation of fishery water bodies and fish farms.

<br>• Development of feasibility studies for the development of fisheries and the organization of specially protected natural territories.',
);
