<?php

return [
    'home'   => 'Home',
    'about'  => 'About',
    'branch' => 'Branches',

    'services' => 'Services',
    'support'  => 'Support',

    'head'      => 'Head division',
    'base'      => 'Information base',
    'grants'    => 'Achievements MES grants',
    'vacancies' => 'Vacancies',

    'labs'           => 'Laboratories',
    'news'           => 'News',
    'structure'      => 'Structure',
    'gallery'        => 'Gallery',
    'deps'           => 'Departments',
    'contacts'       => 'Contacts',
    'language'       => 'Switch language',
    'testing'        => 'Testing center',
    'ichthyology'    => 'Laboratory of ichthyology',
    'aquaculture'    => 'Laboratory aquaculture',
    'hydrobiology'   => 'Laboratory of Hydrobiology and Hydroanalysts',
    'genetics'       => 'Laboratory of genetics of hydrobionts',
    'financial'      => 'Financial and economic department',
    'organizational' => 'Organizational and legal department',
    'information'    => 'Information and innovation department',
    'administrative' => 'Administrative department',

    'altai'           => 'Altai branch',
    'aral'            => 'Aral branch',
    'atyrau'          => 'Atyrau branch',
    'balkhash'        => 'Balkhash branch',
    'west-kazakhstan' => 'West Kazakhstan branch',
    'north'           => 'Northern branch',

    'newsletter' => 'NEWSLETTER',
    'subscribe'  => 'Subscribe',
    'too'        => 'LLP "RESEARCH AND PRODUCTION CENTER OF FISHERIES"',
    'address'    => 'Republic of Kazakhstan, 050016, Almaty, <br> Suyunbay Ave., 89 "A"',
    'rights'     => 'All rights reserved',
    'fax'        => 'Тel/fax',

    'manager'     => 'HR manager',
    'managername' => 'Abdumalikova Balzhan Tastemirovna',

];

?>

