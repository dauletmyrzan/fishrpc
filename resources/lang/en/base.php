<?php

return array (
    'base_title' => 'Information base',
    'base_text' => 'The LLP “KazNIIRH” monitors the state of scientific publications, normative – methodological recommendations, laws in the field of fisheries, fisheries, environmental protection. Formed database of scientific publications in ichthyology, hydrobiology, hydrochemistry and fish toxicology, pond culture, new objects of aquaculture, genetics and breeding of fish, biochemical methods of research of fish, fish diseases and others. There is a specialized scientific library, whose fund comprises more than 10 thousand. Copies of books, periodicals, regulatory guidelines, abstracts of doctoral and master’s theses, patents, copyright certificates, etc. in various fields of fisheries science. There is an archive of their own development. An electronic library catalog. There is a private electronic database for the main fishing ponds Kazakhstan.',
    'publications_title' => 'Scientific publications',
    'publications_text' => ' 21 KazNIIRH published collection of scientific papers, monographs:
              <br><br>
              1.Амиргалиев Н.А. Искусственные водные объекты Северного и Центрального Казахстана. — Алматы, 1998. — 191 с.;
              <br><br>
              2.Амиргалиев Н.А. Арало-Сырдаринский бассейн: гидрохимия, проблемы водной токсикологии.-Алматы, 2007.-224 с.;
              <br><br>
              3.Камелов А.К., Сокольский А.Ф., Альпейсов Ш.А. Современное состояние и подходы к восстановлению численности русского осетра Урало-Каспийского бассейна. -Алматы, 2005. — 208 с.;
              <br><br>
              4.Сисенгалиева Г.Ж., Бокова Е.Б., Джунусова Г. Атлас нерестилищ осетровых рыб реки Урал. — Атырау, 2004.-154с.;
              <br><br>
              5.Амиргалиев Н.А., Тимирханов С.Р., Альпейсов Ш.А./ Ихтиофауна и экология Алакольской системы озер. — Алматы, 2007. — 368 с.;
              <br><br>
              6.Краткий русско-казахско-английский словарь по рыбному хозяйству. — Алматы, 2005.-40 с.;
              <br><br>
              7.Баймуканов М.Т., Зинченко и др. / Фауна позвоночных животных Маркакольского заповедника.-Алматы, Бастау, 2008. — 100 с.;
              <br><br>
              8.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел I Озера Кокчетавской области (1964-1998г.г.) -Алматы, 2009. -70 с.;
              <br><br>
              9.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел II Озера Костанайской области (1958-2006г.г.).-Алматы, 2009. -88 с.;
              <br><br>
              10.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел III Озера Павлодарской области (1958-2008г.г.).-Алматы, 2010. -64 с.;
              <br><br>
              11.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел IV Озера Акмолинской области (1967-1999г.г.).-Алматы, 2011. -108 с.;
              <br><br>
              12.Горюнова А.И., Данько Е.К. Озерное рыбоводство Казахстана. Промысловое использование и рыбоводное освоение малых водоемов Казахстана. — Алматы, 2012. -19 с.;
              <br><br>
              13.Исбеков К.Б., Тимирханов С.Р. Редкие рыбы озера Балхаш. — Алматы, 2009. — 182 с.;
              <br><br>
              14.Жатканбаева Д.М. Основные болезни промысловых рыб Казахстана / ТОО КазНИИРХ. –Алматы, 2012. – 88с.;
              <br><br>
              15.Абдиев Ж.А., Коломин Ю.М.,Фефелов В.В., Хусаинов К.Ш. Водоемы Акмолинской области и их биологические ресурсы: Справочное пособие / Северный филиал ТОО «КазНИИРХ». –Кокшетау, 2013. -60 с. ;
              <br><br>
              16.Библиографический указатель трудов НПЦ РХ по прудовому и индустриальному рыбоводству, генетике и селекции рыб. — Алматы, 2004. — 24 с.;
              <br><br>
              17.Библиографический указатель научных трудов НПЦ РХ по вопросам гидрохимии и водной токсикологии (1966-2008 гг.). — Алматы, 2008.-30 с.;
              <br><br>
              18.Библиографический указатель трудов НПЦ РХ по озеру Балхаш (1959-2007 гг.).- Алматы, 2008.-32 с.
              <br><br>
              19.Асылбекова С.Ж., Исбеков К.Б., Куликов Е.В., Куликова Е.В. / Рекомендации для природопользователей и фермеров по организации и технологическому циклу озерно-товарного рыбоводного хозяйства (ОТРХ). Методическое пособие.- Алматы, 2014.- 132 с.;
              <br><br>
              20.Горюнова А.И., Данько Е.К./ Степные озера Северного Казахстана. Адаптации гидробионтов в условиях периодически высыхающих водоемов.-LAPLAMBERT AcademicPublishing.-2015.-291с.
              <br><br>
              21.Исбеков К.Б./ История Казахского научно-исследовательского института рыбного хозяйства.//- Астана: Жасыл Орда, 2016.- 282 с.
              <br><br>
              22.Нерестилища осетровых рыб р. Жайык. /Атлас/ Главный редактор: Исбеков К.Б. Авторы: Шалгимбаева Г.М., Асылбекова С.Ж., Бокова Е.Б., Ким А.И., Булавина Н.Б. — Алматы, 2017 г.-157 страниц и 200 иллюстраций.
              <br><br>
              23. АТЛАС объектов для проведения гидромелиоративных работ на основных рыбохозяйственных водоемах Республики Казахстан. — Алматы, 2017 г.-22 с.
              <br><br>
              24.Куржыкаев Ж./ Аквакультура-учебное пособие// г. Астана из-во КазАТУ им. С. Сейфуллина 2017 г, 262 стр.
              <br><br>
              25.Ручейники (Trichoptera) Казахстана/ Монография. Министерство образования и науки РК. Казахский национальный аграрный университет, Алматы 2017г.-С. 396 Министерство образования и науки РК// Альпейсов Ш.А., Гришаева О.В., Евсеева А.А., Крайнюк В.Н., Кушникова Л.Б., Пилин Д.В., Склярова О.Н., Смирнова Д.А., Тимирханов С.Р., Эпова Ю.В.
              <br><br>
              26.Акклиматизация рыб и водных беспозвоночных в водоемах Казахстана/С.Ж. Асылбекова, К.Б. Исбеков, Е.В. Куликов, А.Н. Неваленный.-Алматы,2018.-240 с.
              <br><br>
              27.Результаты научных исследований для сохранения и восстановления рыбных ресурсов в водоемах Казахстана/К.Б. Исбеков, С.Ж.Асылбекова, Е.В. Куликов, Д.К. Жаркенов, В.Н.Цой, А.И.Ким.-Алматы:Копиленд, 2018.-215 с.
              <br><br>
              28.Жеке ихтиология: оқулық/С.Ж.Асылбекова, Қ.Ш.Нұрғазы.–Алматы: Қазақ университеті, 2018.-203 бет.
              <br><br>
              29.Балық популяциясының генетикалық негіздері // Оқу құралы./Исбеқов Қ.Б., Қайроллаев К.Қ.-Алматы, 2019.-102 бет.
              <br><br>
              Scientists KazNIIRH participated in writing the book: – 5-volume edition Pisces Kazakhstan. – Alma-Ata: Science, 1986-1992; Modern ecological condition of Lake Balkhash basin. – Almaty, 2002.-388 p .; Problems of hydro-ecological sustainability in the basin of Lake Balkhash. – Almaty, 2003.-584 p .; . The Red Book of the Republic of Kazakhstan / 4th Edition, Volume 1: Animals; Part 11: Vertebrates. – Almaty: Nur-Print, 2008. -316s .; Successions biotsenozovBukhtarma reservoir / Bazhenova OP, EV Kulikov, and others -Omsk:. Izd.of FSEIHPE OmGAU, 2009. -244 p .; Water resources of Kazakhstan: assessment, prognosis, management (30 min.) -T.14. Fisheries Kazakhstan:.State and prospects / Amirgaliev NA, Timirkhanov SR, IsbekovK.B. -Karaganda: ARCO 2012 (ADI). – 620.
			  <br><br>(2010-2015) In the last 5 years developed 36 recommendations: including by growing sturgeon technology – 11, for the conservation and reproduction of food resources – 3, on fish diseases – 6, for conservation and sustainable use of biological resources – 6, and the restrictions and prohibitions on the use of the animal world – 6 recommendations.
			  <br><br>Specialists KazNIIRH published more than 2 thousand. Articles in various media of Kazakhstan, Russia and foreign countries. It is an electronic catalog of publications of the Institute scientists.',
    'normative_title' => 'Normative and methodical documentation',
    'normative_text' => '',
    'base_first_text' => 'LLP “KazNIIRH” carries out international cooperation with leading scientific institutions and foreign countries on environmental issues of water environment, conservation of valuable, rare and endangered species, population-genetic studies ichthyocenoses, aquaculture, etc. (Russia, Azerbaijan, Great Britain, China, Iran, Israel, etc.) is working with international organizations and programs:

The Convention on International Trade in Endangered Species of Wild Fauna and Flora (CITES),

The Food and Agriculture Organization of the United Nations (FAO),

United Nations Development Programme in Kazakhstan (UNDP),

International Fund for Saving the Aral Sea (IFAS),

Consortium «North Caspian Operating Company» (NCOC),

The world community to preserve «Word Sturgeon Conservation Socicty» sturgeon (WSCS),

World Wildlife Fund (WWF «Word Wild Foud»), participation in the Caspian Sea Water Bioresources Commission.',
);
