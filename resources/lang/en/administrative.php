<?php 

return [
'head' => 'Administrative – economic department',
'manager' => 'The chief administrative-economic department',
'managername' => 'Dzhumahanov Askar Kadyralievich',

'text1' => 'Maintenance department is one of the major structural units, providing guidance and organization of the current long-term business planning.The coordination of the interaction of structural divisions, content production and material base in good condition and accident-free, the adoption of measures to enhance economic independence, rational expenditure of funds and materials, organization of access control, safety and effective use of the property of the Institute.',


'text2' => '<h2>The main tasks and functions of the administrative and economic department:</h2>
              <p>
              The main tasks and functions of the maintenance department are: maintenance, repair and maintenance of buildings (premises) institute monitoring the serviceability of equipment ( Lighting, heating, ventilation, etc.), for the quality of the repair work and the reception of their implementation, the organization of work landscaping and cleaning the territory adjacent to the building, holiday decoration facade of the building, drawing up cost estimates for the maintenance of buildings and premises of the Institute.Execution of documents necessary for the conclusion of contracts for the purchase of equipment, office equipment, furniture, household goods, the organization of their delivery, reception and registration.Also providing structural units (laboratories) Transportation (experimental groups) during the field work, the necessary equipment, supplies, reagents, stationery and telephone.Ensuring the preservation of furniture, office equipment, household equipment, the adoption of measures for their restoration and repair in case of damage, the material – technical service meetings of the Coordinating Councils of Scientists and seminars held by the Institute.Fire safety and property of the Institute, participated in the development of measures to prevent occupational diseases and accidents at work, following the steps of the state supervisory authorities, departmental control over the observance of rules and regulations, standards, occupational health, safety, industrial hygiene and fire protection, briefings on health and safety protection.Providing accounting, safety and traffic, timely write-off of material and technical values, control over the execution of contractual obligations within the competence of the department.
              </p>',
];

?>