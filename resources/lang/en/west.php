<?php 

return [
'head' => 'West-Kazakhstan branch', 
'manager' => 'Director of the Western-Kazakhstan branch', 
'managername' => 'Tumenov Artur Nasibullauly, PhD', 
'title1' => 'General information', 
'text1' => 'West Kazakhstan branch of LLP “FishRPC” constantly monitors the state of ecosystems, water bodies of West Kazakhstan and Aktobe regions, developing a biological basis for the rational use and reproduction of fish stocks, gives annual forecasts of fish catch volumes, proposals for rules and mode of fishing. Every year, on the basis of a branch of scientific evidence goes Ordinance to catch limits of fish from the waters of the Government of West Kazakhstan and Aktobe regions. Accumulated many years of data bank on hydrology, hydrobiology, hydrochemistry and composition of fish fauna of the studied reservoirs.',  
'title3' => 'The direction of the research activities', 
'text3' => '<br>• Monitoring of ecological status of water bodies of West Kazakhstan and Aktobe regions.

<br>• Study phenogenetically structure of populations of the main commercial fish species in the waters of reservoirs West Kazakhstan and Aktobe regions.

<br>• Perfection of biotechnology breeding of valuable fish species in water bodies of Kazakhstan.

<br>• Creating a scientific basis for the use of the precautionary approach to fisheries management in the waters of Kazakhstan.

<br>• Rationale for the creation and development of a network of protected areas for the conservation of fish biodiversity.

<br>• The introduction of the achievements of science in fisheries production.

<br>• Passage of educational, scientific and industrial practice for students of West Kazakhstan Agrarian Technical University named Zhangir Khan in “Fisheries”.', 
'title4' => 'Provided by the branch Services', 
'text4' => '<br>• Conducting appraisal of reservoirs for the purpose of their use of the fishery.

<br>• Determination of damage to fisheries by human impact.

<br>• Development of recommendations and biological studies for stocking ponds in the region and acclimatization of commercial species of fish and food organisms.

<br>• Work on eco-design and standardization.

<br>• Development of biological studies of the total allowable catch of fish (TAC).

<br>• Development of recommendations on the device Fishery fishing grounds of nature.

<br>• Conducting monitoring studies of the state of the lower aquatic organisms – invertebrates in heterogeneous reservoirs.

<br>• Evaluation of forage resources reservoirs.

<br>• Identify the degree of consumption of food organisms and fed the fish in natural waters.

<br>• Expert assessment of the communities of the lower aquatic ponds on literary and archival materials.

<br>• Assessment of water pollution by organic substances as lower aquatic organisms – indicators saprobity water column and soil.

<br>• Development of biological studies and forecast production limit of Artemia cysts, and other valuable aquatic invertebrates.

<br>• Review of biological studies, scientific research and projects polytypic reservoirs with the presence of hydrobiological direction.

<br>• Methodical assistance, advice on matters of Hydrobiology.

<br>• Development projects to identify standards for maximum permissible discharges (MPD) for enterprises and reproduction processing of the fishing industry.

<br>• Carrying out diagnostic and therapeutic – health work in the fish farms.

<br>• Scientific substantiation in the environmental certification of reservoirs.

<br>• The study and evaluation of the epizootic situation of fishery water bodies and fish farms.

<br>• Development of technical – economic feasibility studies for the development of fisheries and the organization of specially protected natural territories.

<br>• Complex scientific – methodological support and practical support for the organization of commercial fish breeding: development of biological study on the organization and management of commercial fish farming, with the definition of optimal directions and aquaculture technology.

<br>• Mediation services in providing stocking material and special equipment.

<br>• Assistance in the preparation of the business – plan for hatcheries and farms.

<br>• Advisory services for bioengineering commodity cultivation sturgeon.

<br>• Consultation and participation of specialists in artificial reproduction of carp (carp) and herbivorous fish.

<br>• Advice for employers on the management of fish farms.

<br>• Engineering work on the creation of farming fish farms.', 
];

?>