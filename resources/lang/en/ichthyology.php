<?php 

return [
'head' => 'Laboratory of ichthyology',
'manager' => 'Head of Laboratory',
'managername' => 'Sansyzbayev Yerbol Tursynbekovich',

'title1' => 'Ongoingactivities',
'text1' => '• Monitoring the status of fish populations Kapshagai reservoir and Alakol lakes, local reservoirs Almaty and Zhambyl regions
			<br><br>		
			• Development of regulatory and scientific-methodical fisheries documents
			<br><br>				
			• Creating a scientific basis for the use of the precautionary approach to fisheries management in the Kazakh waters
			<br><br>				
			• Assessment of anthropogenic impacts on the ecology of water bodies of Kazakhstan
			<br><br>				
			• Calculation of damage to fisheries as a result of economic activity',
'text2' => '			
			• Development of recommendations and biological studies for stocking ponds, fish species introductions
			<br><br>				
			• Development of biological studies on translation and a change in the status of bodies of water fishery, the total allowable catch for the fish (TAC)
			<br><br>				
			• Development of recommendations on fishery unit of natural resources fishing grounds
			<br><br>				
			• Statistical processing and preparation of analytical reports on the study.',
'title3' => '',
'text3' => 'In the state of the laboratory including 20 people with extensive experience. The staff performed most of the work on the processing of samples, analysis of the material, report writing, and also consulted a variety of research projects for nature Kapshagai reservoir and Alakol lakes.
<br><br>
			The laboratory is equipped with all necessary equipment, scientific and methodological literature, specialized software for analyzing data ichthyology.
<br><br>
			Each year, the laboratory receives students from different universities of Kazakhstan to undergo scientific and practical training in the field – ichthyology, fisheries, etc. Specialists of the laboratory is published annually to 40 articles in various media of Kazakhstan and foreign countries. They also take an active part in working groups of international and national forums, symposiums, seminars, conferences, actively illuminate the problems and results of the work in the media.
<br><br>
			The laboratory operates on the basis of a certificate of assessment of the state of measurements in the laboratory, number 97/15 issued December 23, 2015, Almaty branch of JSC “National expertise and certification center».
<br><br>
			The staff of the laboratory work carried out on the orders of state bodies of Kazakhstan, such as:
<br><br>
			• Determination of fish productivity of fishery water bodies and / or their parts, the development of biological studies of maximum permissible amounts of withdrawal of fish resources and other aquatic animals and issuing recommendations on the treatment and management of fisheries in the waters of international, national and local significance;
<br><br>
			• Applied research in the field of fisheries for the conservation and restoration of fish resources and other aquatic organisms in the water bodies of international and republican significance Republic of Kazakhstan;
<br><br>
			• under economic contracts with the users of fishing areas, mining companies, etc.'
];

?>