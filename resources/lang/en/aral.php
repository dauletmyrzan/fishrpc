<?php 

return [
'head' => 'Aral branch',
'manager' => 'Director Aral branch',
'managername' => 'Barakbaev Tynysbek  Temirhanovich, PhD',
'title1' => 'GENERAL INFORMATION',
'text1' => 'Aral branch of LLP “FishRPC” constantly monitors the state of ecosystems, water bodies Kyzylorda and South Kazakhstan regions, developing a biological basis for the rational use and reproduction of fish stocks, gives annual forecasts of fish catch volumes, proposals for rules and mode of fishing.Every year, on the basis of a branch of scientific evidence goes Ordinance to catch limits of fish from the waters of the Government of Kyzylorda region of the Aral-Syrdarya basin.Accumulated many years of data bank on hydrology, hydrobiology, hydrochemistry and composition of fish fauna of the studied reservoirs.As part of the branch are integrated fisheries laboratory and the laboratory of hydrobiology and hydrochemistry.',


'title3' => 'The direction of the research activities',
'text3' => '<br>• Monitoring of ecological status of water bodies, Kyzylorda and South Kazakhstan regions. Study phenogenetically structure of populations of the main commercial fish species in the waters of the Kyzylorda and South Kazakhstan regions

<br>• Perfection of biotechnology breeding of valuable fish species in water bodies of Kazakhstan.

<br>• Creating a scientific basis for the use of the precautionary approach to fisheries management in the waters of Kazakhstan.

<br>• Rationale for the creation and development of a network of protected areas for the conservation of fish biodiversity.

<br>• The introduction of the achievements of science in fisheries production.

<br>• Passage of educational, scientific and industrial practice for students of West Kazakhstan Agrarian Technical University named Zhangir Khan in “Fisheries”.',


'title4' => 'PROVIDED BY THE BRANCH SERVICES',
'text4' => '<br>• Carrying out fundamental and applied research aimed at accelerating scientific and technical progress in fisheries.

<br>• Conducting appraisal of reservoirs for the purpose of their use of the fishery.

<br>• Determination of damage to fisheries by human impact.

<br>• Development of recommendations and biological studies for stocking ponds in the region and acclimatization of commercial species of fish and food organisms.

<br>• Work on eco-design and standardization.

<br>• Development of biological studies of the total allowable catch of fish (TAC).

<br>• Development of recommendations on the device Fishery fishing grounds of nature.

<br>• Conducting monitoring studies of the state of the lower aquatic organisms – invertebrates in heterogeneous reservoirs.

<br>• Evaluation of forage resources reservoirs.

<br>• Expert assessment of the communities of the lower aquatic ponds on literary and archival materials.

<br>• Assessment of water pollution by organic substances as lower aquatic organisms – indicators saprobity water column and soil.

<br>• Development of biological studies and forecast production limit of Artemia cysts, and other valuable aquatic invertebrates.

<br>• Review of biological studies, scientific research and projects polytypic reservoirs with the presence of hydrobiological direction.

<br>• Methodical assistance, advice on matters of hydrobiology.

<br>• Development projects to identify standards for maximum permissible discharges (MPD) for enterprises and reproduction processing of the fishing industry.

<br>• Carrying out diagnostic and therapeutic work in the fish farms.

<br>• Scientific substantiation in the environmental certification of reservoirs.

<br>• Development of feasibility studies for the development of fisheries and the organization of specially protected natural territories.

<br>• Complex scientific and methodological support and practical support for the organization of commercial fish breeding: development of biological study on the organization and management of commercial fish farming, with the definition of optimal directions and aquaculture technology.

<br>• Assistance in preparing a business plan for the hatcheries and farms.

<br>• Advisory services for bioengineering commercial breeding of valuable commercial fish species.

<br>• Consultation and participation of specialists in artificial reproduction of zander (carps) and herbivorous fish.

<br>• Engineering work on the creation of farming fish farms.

<br>• Scientific support for the introduction of innovative technologies and developments in production on a contractual basis.

<br>• Conferences, seminars, meetings, discussions on the most important scientific problems of conservation of biodiversity in the waters of the Aral-Syrdarya Fisheries pool.',
];

?>