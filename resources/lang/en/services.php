<?php 

return [
'text' => '<br><br>– Conducting appraisal of reservoirs in order to determine the possibility of their use of fishery;
<br><br>– Development of recommendations and biological studies on stocking of reservoirs, acclimatization of commercial species of fish and food organisms;
<br><br>– Development of biological studies of the total allowable catch of fish (TAC);
<br><br>– Development of recommendations on fishery unit of natural resources fishing grounds;
<br><br>– Work on eco-design and standardization;
<br><br>– Calculation of damage to fisheries as a result of economic activity;
<br><br>– Carrying out environmental monitoring of the Republic of Kazakhstan reservoirs;
<br><br>– Evaluation of forage resources of water bodies and the identification of the degree of consumption of food organisms and fed the fish in natural water bodies and fish farms;
<br><br>– Development of biological studies and forecast production limit of Artemia cysts, and other valuable aquatic invertebrates;
<br><br>– Review of biological studies, research and development projects;
<br><br>– Methodical assistance, advice on all matters of hydrobiology, ichthyology, aquaculture and fisheries;
<br><br>– Determination of the quality of water in reservoirs for organisms indicators of pollution;
<br><br>– Comprehensive survey of the different types of water bodies (streams, ponds, hot water, etc.) with a conclusion about the suitability of water for various household needs, including fisheries management on hydrochemical and toxicological indicators;
<br><br>– Determination of hydrochemical indices of water; pH, dissolved gases, nutrients and organic matter, mineral salt with the forecast exposure ponds kill phenomena;
<br><br>– Determination of the content of toxic substances (metals, pesticides, etc.) In water fishery ponds;
<br><br>– Development of ecological passports of enterprises for the fishing industry;
<br><br>– The study and evaluation of the epizootic situation of fishery water bodies;
<br><br>– An analysis of the immunological status of fish and carrying out measures to improve the immuno-physiological status of fish in fish farms;
<br><br>– The development of natural-scientific and technical-economic feasibility studies for the organization of especially protected natural territories;
<br><br>– Development of design documentation for the hatcheries of various types (lake-commodity, pond, cage farming, enterprises of industrial fish farming, RAS);
<br><br>– Assistance in the preparation of the business-plan for hatcheries and farms;
<br><br>– Advisory services for bioengineering commercial breeding of fish (salmon, sturgeon, carp, perch);
<br><br>– Conducting selection-breeding work at hatcheries Republic;
<br><br>– Design work on the creation of farm fish farms;

',
];

?>