<?php 

return [
'head' => 'Organizational – legal department',
'manager' => 'Lawyer ',
'managername' => 'Turguldinov Ertugan Sekenovich',
'text1' => 'Activities of the organizational – legal department is governed by regulations of the Department and shall be in accordance with the legislation of the Republic of Kazakhstan, the normative acts of the state bodies, the Charter of the Association.',

'text2' => '<h2>Tasks</h2>
              <p>
              Law Enforcement Partnership, the legal protection of the interests of the Partnership contract, with claims and lawsuits, advising heads of departments and the Association staff on legal issues, ensuring the procedures of public procurement of goods, works and services to the Partnership, ensuring proper Partnership participation in public competitive procurement of goods , works and services, staffing Partnership, Development of current and future plans of acquisition of the Association staff, registration of admission, transfer and dismissal of employees in accordance with labor laws, regulations, instructions and orders of the management, the organization of control over labor discipline state in the structural units and compliance professionals work rules, the development of internal regulations of the Association (orders, instructions, regulations, etc.).
              </p>',

'text3' => '<h2>Function</h2>
              <p> 
                Providing information on the implementation of the Association Councils’ decisions adopted within the framework of existing powers, coordination within the competence of the events on the performance of individual decisions of the Association bodies, provision of consulting and methodological assistance to the structural units and branches of the Association, the formation of the annual plan of the state purchases of the Association, carrying out the procedure of public procurement of goods, works and services to the Partnership, coordination issues of participation of divisions and branches in public procurement, legal security, staffing of the Association, the Association of monitoring performance of obligations under contracts of public procurement.
              </p>',

];

?>