<?php

return array (
  'management' => 'Leadership',
  'history' => 'History of institute',
  'mission' => 'Mission of the center',
  'cooperation' => 'International cooperation',
  'works' => 'Large volume of works on off-budget contracts is performed on such directions as:',
  'research' => 'Directions of research and production activity of institute, strategic directions:',
  'director' => 'General director, Doctor of Biological Sciences',
  'directorname' => 'Isbekov Kuanish Baybolatovich',
  'zamdirector' => 'Deputy general director, Doctor of Biological Sciences',
  'zamdirectorname' => 'Assylbekova Saule Zhangirovna',
  'sekretar' => 'Deputy general director, Doctor of Biological Sciences',
  'sekretarname' => 'Mukhramova Alyona Aleksandrovna',
  'sovetnik' => 'CEO`s councelor',
  'sovetnikname' => 'Romashov Yuri Tulegenovich',
  'phone' => 'Contact number',
  'head' => 'Head division',
  'historytext' => 'Kazakhstan occupies the 2 nd place after Russia among the “Union of independent states” countries in terms of inland waters, the fishery has eight pools, which resulted in a corresponding diversity and fish stocks. The fish fauna of the republic in the first quarter of the 20th century there were about 100 species and subspecies of fish. Subsequently, since the 1930s, it has undergone significant changes mainly due to large-scale acclimatization. Currently, the fish fauna are more than 150 species, many of which are valuable fishing and some - endemic to individual basins. Such species as the Balkhash perch, salmon Aral, Aralbarbel are the bearers of a unique gene pool. Unique is the fish fauna of the Caspian Sea - the world`s gene pool and reserve stocks of sturgeon.
<br><br>
The history of the institute began with the organization in 1929 in Aralsk fisheries research station, Scientific Research Institute of Fisheries and Oceanography. In 1933, a branch of the station has been opened in the city of Balkhash, which in 1934 became an independent research station VNIRO. In 1938 the station was transferred from Scientific Research Institute of Fisheries and Oceanography Systems, Scientific Research Institute of Lake and River Fisheries and converted into its compartment.
<br><br>
In 1957, Aral and Balkhash department of conducting Scientific Research Institute of Lake and River Fisheries were transferred to the Institute of Zoology of the Academy of Sciences of the Kazakh SSR (Scientific Research Institute of Lake and River Fisheries Order number 92 from 29.08.1957g., Regulation of the Council of Ministers of the Kazakh SSR number 749 from 22.10.1957g., Decree of the Kazakh SSR number 474 and 481 from 10.11.1957g ., Institute of Zoology of the order № 149 from 14.11.1957g.), which, moreover, had since the middle of 1950 has ichthyological point in Ust-Kamenogorsk.
<br><br>
Due to the presence of water and rich fishing resources in 1959, it was decided to set up in Kazakhstan in the system of the Republican Academy of Sciences, an independent Institute of Ichthyology and Fisheries .Po the v1959, the Decision of the Council of Ministers of the Kazakh SSR and the Communist Party of Kazakhstan from 20.08.1958g. Number 725 and the Decree of the Presidium of the Academy of Sciences of 19.03.1959g. Number 417 from the Institute of Zoology of structure (based on his department Ichthyology and Hydrobiology) was isolated Institute of Ichthyology and Fisheries KazSSR located in Guryev (now Atyrau) and its subordinate Altai, Aral, Balkhash and Zhezkazgan offices ichthyological reference points.
<br><br>
In 1963.in accordance with the Resolution of the Council of Ministers of the USSR from 02.07.1962g. Number 523 and on the basis of Resolution of the Council of Ministers of the Kazakh SSR from 29.04.1963g. Number 619 Institute of Ichthyology and Fisheries KazSSR was transferred to the State Committee for Fisheries at the USSR Council of Ministers (Transfer Act of 15.12.1962g.) And transformed into the Kazakh Research Institute of Fisheries (KazNIIRH).In the same year, by order of the State Committee for Fisheries № 150 from 02.04.1963g. The institute was relocated from the city of Guryev in Balkhash. In the future, in connection with the transformation of the State Committee for Fisheries of the USSR Council of Ministers at the Ministry of Fisheries of the USSR Institute was subordinated to the Ministry. Remaining in Guryev laboratory sturgeon KazNIIRH then became a member of the Ural-Caspian branch of the Central Research Institute of sturgeon fisheries, passed in 1988 under the authority KaspNIIRH (Astrakhan).
<br><br>
In 1976, on the basis of the Order of the Ministry of Fisheries of the USSR from 17.02.1976g.KazNIIRH number 98 was handed over to the Ministry of Fisheries of the Kazakh SSR. This status of the Institute remained until his transfer head unit in 1987 from the city of Balkhash in Alma-Ata in connection with the beginning of perestroika management system and to strengthen qualified personnel. Subsequent numerous transformations and name changes and the structure of the Institute and its laboratories are largely connected with the search for optimal solutions to the changing socio - economic conditions.
<br><br>
In 1987.basedKazNIIRH, Kapchagai experimental demonstration fish farm, Almaty and Ust-Kamenogorsk pond farms were established Kazakh Scientific and Production Association of Fisheries (KazNPORH) state-cooperative association Kazrybhoz at the State Agricultural Committee of the Kazakh SSR.
<br><br>
In the period of 1989-1991.from the KazNPORH were withdrawn prudhozy and fish farm and KazNIIRH December 1991. It began to operate as an independent scientific - research institution.
<br><br>
Since January, 1992. (Resolution of the Cabinet of 09.01.1992g number 15.) KazNIIRH with four compartments - Altai, Aral, Balkhash and North - Kazakhstan and Pavlodar reference point passed in the Kazakh Academy of Agricultural Sciences.
<br><br>
In 1992, funding for the Ural-Caspian department Ural-Caspian branch of the Central Research Institute of sturgeon fisheries, was discontinued. At its base formed a small scientific enterprise "Zhayyktykorgau" was incorporated in the Committee of Fisheries Ministry of Agriculture of the Republic of Kazakhstan. Later, it was transformed in the Ural-Caspian Fisheries Research Institute.
<br><br>
In 1994, Ural-Caspian Fisheries Research Institute transformed into a department of AtyrauKazNIIRH.In 1995. It disbanded the North-Kazakhstan branch of the institute.
<br><br>
In 1996 KazNIIRH inducted into the National Academic Centre for Agrarian Studies (NATSAI) of the Ministry of Education and Science of Kazakhstan.
<br><br>
In 1999, by order of the Ministry of Education and Science of RK KazNIIRH it was transformed into the State Enterprise "KazNIIRH" and his department renamed branches.
<br><br>
In 2002, the Government Resolution dated October 29, 1148 number of RSE "KazNIIRH" of the Ministry of Education and Science of Kazakhstan transferred to the Ministry of Agriculture (MoA), and then reorganized by transforming the Republican State Enterprise (RSE) on the right of economic management "Scientific - production Centre for Fisheries "(" NPH PX "), the Ministry of Agriculture, together with affiliates.
<br><br>
In 2006, it established the North (Kokshetau) and West Kazakhstan (Uralsk) branch NPTs PX.
<br><br>
In 2007, the RSE "SPC PX" at first reorganized into joint-stock company "KazAgroInnovation".
<br><br>
In 2010, at the Balkhash branch of LLP "KazNIIRH" Distribution Center established knowledge in the field of agriculture "Balkhash".
<br><br>
In 2013, the Northern branch relocated from Kokshetau to Astana maintaining strongholds in the cities of Kostanay, Petropavlovsk, and offices in the city of Karaganda.
<br><br>
In 2015, KazNIIRH transferred to the Non-Profit JSC "National Agricultural Research and Education Center" (NAO "NANOTS").',
  'missiontext' => 'FishRPC Mission - Save and enhance fisheries resources of the Republic of Kazakhstan and to open new possibilities of their use.
<br><br>
Vision - FishRPC through the implementation of the mission and the strategy aims to become a scientific research institute of international level in the field of fisheries and ecology of aquatic biocenoses.
<br><br>
FishRPC Strategy until is to create a system for obtaining scientific products conforming to international standards, a reliable system of implementation of the results of research into the practice of fisheries, a stable financial position, provided by public and private research work orders.',
  'cooperationtext' => 'For more than 90 years of its activity FishRPC continuously monitors the state of ecosystems, water bodies of Kazakhstan and their biological resources, the development of the biological bases of rational use and reproduction of fish stocks, gives annual forecasts of fish catch volumes, proposals for rules and mode of fishing. Every year, on the basis of science - based data KazNIIRH goes Government Decision on limits of fish catch from the Republic of reservoirs. Accumulated many years of data bank on hydrology, hydrobiology, hydrochemistry, toxicology in terms of human impact on the ecosystems of water bodies.Conducted acclimatization work on the introduction of feed and fish fauna of invertebrates.Together with the production of the works on the lake-commercial fish farming.Estimated reserves of valuable bioresource - crustacean Artemia cysts and across the waters of the mineralized waters of the Republic of Kazakhstan. It introduces the industrial fishery: developed technology of cultivation and breeding of fish with warm water and warm-water power stations. By using the latest genetic techniques (chemical mutagenesis and radiation gynogenesis) created highly productive breed groups of carp and silver carp. Scientists fish farmers developed general layout of fish farms, breeding and biotechnology regulatory framework for specific regions of Kazakhstan. A resource-saving technology of fish breeding in polyculture using cheap feed mixtures. Recommendations for prevention and treatment of fish diseases in fish and spawning-vyrastnyh farms, to reduce the incidence of fish in natural waters. Research on the technique of fishing, which allowed to offer new and more effective methods and fishing gear, machines and equipment for mechanization of labor-intensive processes in the fishing industry. It was introduced into production horizontal vacuum boilers of domestic production for the production of fish meal and progressive technology canning sardines. A number of manufacturing technologies and methods of manufacture of canned fish, sausages, fish products smoked, various range of low-value fish glue from fish scales, etc. Economic research has focused on the analysis of economic activities of fisheries in order to increase their economic efficiency and improving work organization.
<br><br>
Since 2001, carried out comprehensive environmental studies aimed at assessing the status of living aquatic resources of transboundary water basins.A refined and developed principles and criteria for the distribution of living aquatic resources between neighboring countries. The research results are used in the work of the Commission on Aquatic Bioresources of the Caspian Sea at the interstate distribution of total allowable catches of sturgeon and other commercially valuable fish. Based on these studies and interaction with scientific - research organizations of the Caspian states to develop common methods for estimating the number of sturgeon species, developed a regional program for conservation and sustainable use of biological resources of the Caspian Sea.
<br><br>
Currently performed research work in the following budget programs:
<br><br>
"Applied research in the field of fisheries for the conservation and restoration of fish resources and other aquatic organisms in the water bodies of international and republican significance Republic of Kazakhstan" on the following projects:
<br><br>
The study of population dynamics and a comprehensive threat assessment factors Caspian seal population
Assessment of natural and artificial reproduction of sturgeon species Ural river
Research on the effectiveness of devices used by fish protection and development recommendations on the modernization and introduction of new fish protection devices in order to preserve fish stocks in Kazakhstan waters
Study of the current state of fisheries hydroecological reservoirs and development of biological studies on the feasibility and priority of the fishery reclamation to preserve and enhance the capacity of fishery ponds
Assessment of the main fishery resources fishing ponds Kazakhstan to develop a careful inventory management strategy and ensure sustainable fisheries
- "Development of effective commercial fish farming technologies in the Republic of Kazakhstan" the following 5 projects:
<br><br>
Development of cost-effective technologies of cultivation of valuable species fish and their introduction to the hatcheries in Kazakhstan
Development and implementation of domestic formulations specialized feed
Ichthyopathological and microbiological evaluation of fish grown in fish farms
Genetic evaluation and updating of the genetic data bank aquaculture
Assessment of the economic and social efficiency of technology of cultivation of fish species
- "Grant funding for research" on the themes:
<br><br>
Development and implementation of biotechnological methods trout cultivation using domestic feed products with the inclusion of probiotic action to improve the conditions of cultivation and increase fish production
Genetic differentiation among populations of the crustacean Artemia as a valuable bioresource salt ponds Kazakhstan
Genetic monitoring of natural and artificial reproduction of sturgeon of the Ural-Caspian basin to preserve their biodiversity and restoration of
Aquatic invertebrates as regional biological indicators of ecological status of water bodies in Kazakhstan.
<br><br>
- "Normative - methodical support of agribusiness industries";
<br><br>
- "Carrying out activities for the dissemination and implementation of innovative experience"
<br><br>
Running large amount of extrabudgetary contracts in areas such as:
<br><br>
- Scientific support of fisheries management in the fixed reservoirs;
<br><br>
- Certification of fishery reservoirs and identify promising reservoirs for fisheries management (commercial fish farming, fishing, shrimp production);
<br><br>
- The development of fish farming - biological foundations of creating fish farms of various types;
<br><br>
- Development of biological studies on acclimatization and stocking;
<br><br>
- Development of biological studies for the calculation of the damage to fisheries of economic activity and recommendations for its reduction;
<br><br>
- Development of methods and fish breeding and technological standards for artificial reproduction and commodity cultivation of new aquaculture species and a large number of other applied research work.',
  'workstext' => '<br>Scientific support of implementation of activity of fishery on the fixed water reservoirs;
				<br>Certification of fishery reservoirs and definition of perspective water reservoirs for implementation of activity of fishery (commodity fish breeding, fishery, production of artemia); 
				<br>Development of fish-breeding and biological justifications for creation of fish-breeding farms of various type; 
				<br>Development of biological justifications for acclimatization and stocking; 
				<br>Development of biological justifications for calculation of damage to fishery from economic activity and the recommendation for decrease of damage.
',
  'researchtext' => '<br>Obtaining competitive results;
					<br>Integration of science, education and production; 
					<br>Introduction of scientific developments;
					<br>Balanced hr personnel;
					<br>Improvement of infrastructure of the organization;
					<br>Improvement of financial policy.',
);
