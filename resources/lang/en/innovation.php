<?php 

return [
'head' => 'Information – innovation department',
'manager' => 'Head Of the Information – innovation department',
'managername' => 'Bolatbekova Zamira Turarovna',
'text1' => 'Research in priority areas to be based on information about the advanced technologies database. Questions about the use of intellectual property, which is a fundamental principle of the innovation potential of any organization, especially relevant at the present stage of economic development and play a key role. An important element in the implementation of this work at the Institute is an information – Innovation department. The department is governed by regulations “On information and innovation department” in terms of the objectives and tasks assigned to the Department, guided by the Constitution of the Republic of Kazakhstan, the applicable laws, normative legal acts of the RK, orders management LLP “KazNIIRH”, the Ministry of Agriculture of the Republic of Kazakhstan and a non-profit joint-stock company “National Agricultural Research – Educational Center',

'text2' => '<h2>Tasks</h2>
              <p>
              The main objectives of the Division are to ensure the effective conduct of research aimed at accelerating scientific and technological progress in the fishing industry, the introduction of the practical results of research and development in the fishery, provide guidance and information support to all researchers and structural subdivisions of LLP “KazNIIRH” maintenance working condition introduced in LLP “KazNIIRH” Integrated management System (ISM) that is certified to national standards of management.
<br><br>
– ST RK ISO 9001 – Quality Management System,
<br><br>
– ST RK ISO 14001 – Environmental Management System
<br><br>
– ST RK OHSAS-18001 – Management System Occupational Health and Safety

ensuring compliance documentation IMS standards organization, work instructions, regulations, external and internal documents of the organization. Also, information support of scientific – industrial and innovative activity of LLP “KazNIIRH” execution of orders management LLP “KazNIIRH” according to the department functions, preparation and submission to the superior organization of plans and concerning the publication of scientific papers reports, participation in conferences and speeches in the media and other rating indicators, development of a database on various aspects of fisheries science, participation in shaping the publications plans, statements in the media creating collections of scientific papers LLP “KazNIIRH ‘organization subscriptions to periodicals, database updating for various publications in specialized journals (in tonnes. h Internet -. resources), search and monitoring of innovation in the field of fisheries, new techniques, manuals, articles, etc., patent search of the prior art patent research subject of the search carried out in the framework of R & D, using the RK patent Foundation, the countries of near and far abroad, CSL MES RK, RK National library, online library of Internet resources, drawing up of the search report, the formulation of the application materials for patents in accordance with the patent Act of the Republic and the Rules of preparation and submission of an application for a patent for an invention, periodic updating of information on the Web site of LLP “KazNIIRH” support of the scientific and technical library.
              </p>',

'text3' => '<h2>Functions</h2>
             <p> 
               Department in accordance with the tasks assigned to it performs the following functions:
              </p>
              <h2>Information:</h2>
              <p> 
               Information support system of monitoring fisheries research, the formation of a data bank of information on various aspects of fisheries science, familiarize researchers with the latest scientific and technical literature, guidelines, recommendations, security documents on paper and electronic media, updating the library collection, informing researchers about new directions in the development of fisheries science, information science staff of information networks and databases provided by LLP “KazNIIRH”, JSC “National Center for Scientific and Technical Information” and other organizations on the paid and free basis, the implementation of information and bibliographic activities: formation of electronic catalogs of scientific and technical libraries and scientific works of employees of LLP “KazNIIRH”, the implementation of subscriptions to periodicals in the directions of activity of LLP “KazNIIRH” content website LLP “KazNIIRH” search of information in the field of fisheries.
              </p>
              <h2>Analytical:</h2>
              <p> 
               Monitoring of the information needs of researchers, structural processing of incoming information, the definition of ways to improve the information – analytical support.
              </p>
              <h2>Organizational and methodical:</h2>
              <p> 
               Studying requests methodological support and practical assistance to young scientists, assistance in passing the courses to improve the skills in the field of patents and scientific and technological information, organization of information support during the Academic Councils and other activities of LLP “KazNIIRH” ensuring acquisition of scientific and methodical literature and periodicals, research reports, scientific and technical library of LLP “KazNIIRH” registration of state programs (projects) research and research results (reports) in the JSC “scientific and technical information National center.”
              </p>
              <h2>Consulting:</h2>
              <p> 
              Organization consulting work for the researchers to find materials in the fund STL LLP “KazNIIRH” of the Internet, the use of networks and databases for STI provided by JSC “National Center for Scientific and Technical Information”.
              </p>',

];

?>