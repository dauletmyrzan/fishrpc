<?php 

return [
'head' => 'Aquaculture laboratory',
'manager' => 'Head of Laboratory',
'managername' => 'Abilov Berdibek Ibragimovich',

'title1' => 'Ongoingactivities',
'text1' => '• Improving the technology of fish genetic conservation of Kazakhstan;
			<br><br>		
			• Improving the technology of breeding of fish species in the waters of Kazakhstan;
			<br><br>				
			• Laying the foundations breeding in aquaculture;
			<br><br>				
			• Conducting appraisal of reservoirs in order to determine the possibility of their use of the fishery;
			<br><br>				
			• Development of design documentation for the hatcheries of various types (lake-commodity, pond, cage farming, enterprises of industrial fish farming, RAS);',
'text2' => '• Assistance in preparing a business plan for the hatcheries and farms;
			<br><br>	
			• Advisory services for bioengineering commercial fish farming(Salmon, sturgeon, carp, perch);
			<br><br>	
			• Carrying out breeding work at hatcheries Republic of Kazakhstan;
			<br><br>	
			• Advice for employers on the management of fish farms
			<br><br>	
			• Design work on the creation of farm fish farms.',
'title3' => '',
'text3' => 'Experienced laboratory staff performs most of the work on the handling of samples, analysis of the material, report writing. The laboratory is equipped with all necessary equipment, scientific and methodological literature, specialized software for data analysis. The laboratory specialists are actively involved in working groups of international and national forums, symposiums, seminars, conferences, actively illuminate the problems and results of the work in the media, publishing articles in various media of Kazakhstan, Russia and other countries. Also, the laboratory receives students from different universities of Kazakhstan to undergo scientific and practical training in the specialty fish farming. The laboratory operates on the basis of a certificate of assessment of the state of measurements in the laboratory, number 95/15 issued December 23, 2015, Almaty branch of JSC “National expertise and certification center».
	<br><br>	
	The staff of the laboratory work carried out on the orders of state bodies of Kazakhstan, such as:
	<br><br>	
	• Development of effective technologies of commodity fish farming in the Republic of Kazakhstan “for 2015-2017 years under the state order for the budget program 212” Scientific research and activities in the field of agriculture and nature“;
	<br><br>	
	• under economic contracts.'
];

?>