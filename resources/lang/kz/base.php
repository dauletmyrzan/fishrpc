<?php

return array (
    'base_title' => 'Ақпараттық база',
    'base_text' => '«ҚазБШҒЗИ» ЖШС-де ғылыми басылымдарды, нормативті-әдістемелік ұсыныстарды, балық аулау, балық шаруашылығы және қоршаған ортаны қорғау салаларындағы заңдарды мониторингілеу жұмыстары жасалады. Ихтиология, гидробиология, гидрохимия мен ихтиотоксикология, тоған балық шаруашылығы, аквакультураның жаңа объектілері, балықтар генетикасы мен селекциясы, биохимиялық әдістер мен ихтиопаразитология т.б. салалар бойынша ғылыми басылымдар жинағы жасалған. Арнайы ғылыми-техникалық кітапхана бар, оның фондында он мыңнан астам кітаптар, периодтық басылымдар, нормативті-әдістемелік ұсыныстар, докторлық және кандидаттық диссертациялардың авторефераттары, патенттер мен балық шаруашылығы ғылымының әртүрлі бағыттары бойынша авторлық куәліктері бар. Өзіндік жеке еңбектердің архиві бар. Электрондық кітапхана каталогы жасалуда. Қазақстанның негізгі балық ауланатын су айдындары жөнінде жеке электронды мәліметтер базасы жасалған.',
    'publications_title' => 'Ғылыми басылымдар',
    'publications_text' => ' «ҚазБШҒЗИ» ЖШС-де 21 ғылыми еңбектер жинағы, монографиялар жарық көрген:
              <br><br>
              1.Амиргалиев Н.А. Искусственные водные объекты Северного и Центрального Казахстана. — Алматы, 1998. — 191 с.;
              <br><br>
              2.Амиргалиев Н.А. Арало-Сырдаринский бассейн: гидрохимия, проблемы водной токсикологии.-Алматы, 2007.-224 с.;
              <br><br>
              3.Камелов А.К., Сокольский А.Ф., Альпейсов Ш.А. Современное состояние и подходы к восстановлению численности русского осетра Урало-Каспийского бассейна. -Алматы, 2005. — 208 с.;
              <br><br>
              4.Сисенгалиева Г.Ж., Бокова Е.Б., Джунусова Г. Атлас нерестилищ осетровых рыб реки Урал. — Атырау, 2004.-154с.;
              <br><br>
              5.Амиргалиев Н.А., Тимирханов С.Р., Альпейсов Ш.А./ Ихтиофауна и экология Алакольской системы озер. — Алматы, 2007. — 368 с.;
              <br><br>
              6.Краткий русско-казахско-английский словарь по рыбному хозяйству. — Алматы, 2005.-40 с.;
              <br><br>
              7.Баймуканов М.Т., Зинченко и др. / Фауна позвоночных животных Маркакольского заповедника.-Алматы, Бастау, 2008. — 100 с.;
              <br><br>
              8.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел I Озера Кокчетавской области (1964-1998г.г.) -Алматы, 2009. -70 с.;
              <br><br>
              9.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел II Озера Костанайской области (1958-2006г.г.).-Алматы, 2009. -88 с.;
              <br><br>
              10.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел III Озера Павлодарской области (1958-2008г.г.).-Алматы, 2010. -64 с.;
              <br><br>
              11.Горюнова А.И., Данько Е.К./Озерный фонд Казахстана: Раздел IV Озера Акмолинской области (1967-1999г.г.).-Алматы, 2011. -108 с.;
              <br><br>
              12.Горюнова А.И., Данько Е.К. Озерное рыбоводство Казахстана. Промысловое использование и рыбоводное освоение малых водоемов Казахстана. — Алматы, 2012. -19 с.;
              <br><br>
              13.Исбеков К.Б., Тимирханов С.Р. Редкие рыбы озера Балхаш. — Алматы, 2009. — 182 с.;
              <br><br>
              14.Жатканбаева Д.М. Основные болезни промысловых рыб Казахстана / ТОО КазНИИРХ. –Алматы, 2012. – 88с.;
              <br><br>
              15.Абдиев Ж.А., Коломин Ю.М.,Фефелов В.В., Хусаинов К.Ш. Водоемы Акмолинской области и их биологические ресурсы: Справочное пособие / Северный филиал ТОО «КазНИИРХ». –Кокшетау, 2013. -60 с. ;
              <br><br>
              16.Библиографический указатель трудов НПЦ РХ по прудовому и индустриальному рыбоводству, генетике и селекции рыб. — Алматы, 2004. — 24 с.;
              <br><br>
              17.Библиографический указатель научных трудов НПЦ РХ по вопросам гидрохимии и водной токсикологии (1966-2008 гг.). — Алматы, 2008.-30 с.;
              <br><br>
              18.Библиографический указатель трудов НПЦ РХ по озеру Балхаш (1959-2007 гг.).- Алматы, 2008.-32 с.
              <br><br>
              19.Асылбекова С.Ж., Исбеков К.Б., Куликов Е.В., Куликова Е.В. / Рекомендации для природопользователей и фермеров по организации и технологическому циклу озерно-товарного рыбоводного хозяйства (ОТРХ). Методическое пособие.- Алматы, 2014.- 132 с.;
              <br><br>
              20.Горюнова А.И., Данько Е.К./ Степные озера Северного Казахстана. Адаптации гидробионтов в условиях периодически высыхающих водоемов.-LAPLAMBERT AcademicPublishing.-2015.-291с.
              <br><br>
              21.Исбеков К.Б./ История Казахского научно-исследовательского института рыбного хозяйства.//- Астана: Жасыл Орда, 2016.- 282 с.
              <br><br>
              22.Нерестилища осетровых рыб р. Жайык. /Атлас/ Главный редактор: Исбеков К.Б. Авторы: Шалгимбаева Г.М., Асылбекова С.Ж., Бокова Е.Б., Ким А.И., Булавина Н.Б. — Алматы, 2017 г.-157 страниц и 200 иллюстраций.
              <br><br>
              23. АТЛАС объектов для проведения гидромелиоративных работ на основных рыбохозяйственных водоемах Республики Казахстан. — Алматы, 2017 г.-22 с.
              <br><br>
              24.Куржыкаев Ж./ Аквакультура-учебное пособие// г. Астана из-во КазАТУ им. С. Сейфуллина 2017 г, 262 стр.
              <br><br>
              25.Ручейники (Trichoptera) Казахстана/ Монография. Министерство образования и науки РК. Казахский национальный аграрный университет, Алматы 2017г.-С. 396 Министерство образования и науки РК// Альпейсов Ш.А., Гришаева О.В., Евсеева А.А., Крайнюк В.Н., Кушникова Л.Б., Пилин Д.В., Склярова О.Н., Смирнова Д.А., Тимирханов С.Р., Эпова Ю.В.
              <br><br>
              26.Акклиматизация рыб и водных беспозвоночных в водоемах Казахстана/С.Ж. Асылбекова, К.Б. Исбеков, Е.В. Куликов, А.Н. Неваленный.-Алматы,2018.-240 с.
              <br><br>
              27.Результаты научных исследований для сохранения и восстановления рыбных ресурсов в водоемах Казахстана/К.Б. Исбеков, С.Ж.Асылбекова, Е.В. Куликов, Д.К. Жаркенов, В.Н.Цой, А.И.Ким.-Алматы:Копиленд, 2018.-215 с.
              <br><br>
              28.Жеке ихтиология: оқулық/С.Ж.Асылбекова, Қ.Ш.Нұрғазы.–Алматы: Қазақ университеті, 2018.-203 бет.
              <br><br>
              29.Балық популяциясының генетикалық негіздері // Оқу құралы./Исбеқов Қ.Б., Қайроллаев К.Қ.-Алматы, 2019.-102 бет.
              <br><br>
              ҚазБШҒЗИ ғалымдары келесі кітаптардың жазылуына үлестерін қосқан:: 5-томды кітап Рыбы Казахстана. – Алма-Ата: Наука, 1986-1992; Современное экологическое состояние бассейна озера Балхаш. – Алматы, 2002.-388 с.; Проблемы гидроэкологической устойчивости в бассейне озера Балхаш. – Алматы, 2003.-584 с.; Красная книга Республики Казахстан / 4-е изд., Т.1: Животные; Часть 11: Позвоночные. – Алматы: Нур-Принт, 2008. -316с.; Сукцессии биоценозов Бухтарминского водохранилища / Баженова О.П., Куликов Е.В., и др. -Омск: Изд.-во ФГОУ ВПО ОмГАУ, 2009. -244 с.; Водные ресурсы Казахстана: оценка, прогноз, управление (30 тт.) –Т.14. Рыбное хозяйство Казахстана: состояние и перспективы./ АмиргалиевН.А., ТимирхановС.Р., ИсбековК.Б. -Караганда: АРКО, 2012(ДСП). – 620 с.
              <br><br>Соңғы 5 жыл аралығында (2010-2015 гг.) 36 рекомендация шығарылған: олардың ішінде бекіре балықтарын өсіру технологиясы бойынша – 11, қоректік организмдердің көбеюі мен сақтауы туралы – 3, балық аурулары бойынша – 6, биоресурстарды рациональды қолдану және сақтау туралы – 6 және жануарлар әлемін пайдаланудағы шектеулер мен тиым салулар бойынша – 6 рекомендация.
			  <br><br>ҚазБШҒЗИ мамандары Қазақстан, Ресей және басқа да шет ел басылымдарында 2 мыңға жуық мақала шығарды. Институттың ғалымдарының ғылыми басылымдары бойынша электронды каталог жүргізіледі.',
    'normative_title' => 'Нормативтік-әдістемелік құжаттама',
    'normative_text' => '',
    'base_first_text' => 'ЖШС «ҚазБШҒЗИ» алыс және жақын шет елдердің алдыңғы қатарлы ғылыми мекемелерімен сулы ортаның экологиясы, бағалы, сирек және жойылу қауіпі бар балықтардың биоалуантүрлілігін сақтау, ихтиоценоздарды популяциялы-генетикалық зерттеу, аквакультура және т.б бойынша (Ресей, Азербайжан, Ұлыбритания, Қытай, Израиль және т.б) халықаралық ұйымдар мен бағдарламалармен жұмыстар жүргізіледі.

Жойылып кету қауіпі төнген жабайы флора мен фаунаны халықаралық саудалау туралы конвенция (СИТЕС).

Біріккен Ұлттар Ұйымының азық-түлік және ауыл шаруашылығы ұйымы (ФАО)

Біріккен Ұлттар Ұйымының даму бағдарламасы (ПРООН).

Халықаралық Аралды қорғау қоры (ХАҚҚ).

«NorthCaspianOperatingCompany» консорциумы (NCOC).

Бекіре балықтарын сақтаудың әлемдік бірлестігі  «WordSturgeonConservationSocicty» (WSCS).

Дүниежүзілік жабайы табиғат қоры  (WWF «WordWildFoud»)',
);
