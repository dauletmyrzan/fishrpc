<?php 

return [
'head' => 'Ихтиология зертханасы',
'manager' => 'Зертхана меңгерушісі',
'managername' => 'Сансызбаев Ербол Турсынбекович',

'title1' => 'Жүргізілетін жұмыс түрлері:',
'text1' => '• Қапшағай суқоймасы және Алакөл көлдер жүйесінің, Алматы және Жамбыл облысының жергілікті маңызы бар суқоймаларының балықтарының популяциясының жағдайына мониторинг жүргізу;
			<br><br>		
			• Балық шаруашылығы саласындағы нормативті-құқықтық және ғылыми-әдістемелік құжаттарды дайындау;
			<br><br>	
			• Қазақстан суқоймаларында балық аулауды бірқалыптандыру үшін қолдану уақытындағы ғылыми негіздемелерді жасау;
			<br><br>	
			• Қазақстан суқоймаларының экологиясына антропогендік әсердің бағасын қарау;',
'text2' => '• Балық шаруашылығына шаруашылықтық жұмыстар уақытындағы әсер еткен зиян есебі;
			<br><br>	
			• Кәсіптік балық түрлерін интродукциялау, суқоймаларды байыту бойынша биологиялық негіздемелер және рекомендациялар ұсыну;
			<br><br>	
			• Балықшаруашылықтық маңызы бар суқоймалар деңгейін өзгерту және ауыстыру бойынша жалпы берілетін балық аулау көлемі бойынша биологиялық негіздеме жасау;
			<br><br>	
			• Табиғат пайдаланушылардың кәсіптік балықшаруашылықтық құрылғылары бойынша ұсыныс дайындау;
			<br><br>	
			• Жүргізілген зерттеулер бойынша аналитикалық есеп дайындау және статистика жасау;',

'title3' => '',

'text3' => 'Қызметкерлер жұмыстың сынама алу, материалды талдау, есеп жазу, сонымен қатар Қапшағай суқоймасы және Алакөл көлдер жүйесінің табиғат пайдаланушыларына әр түрлі ғылыми-зерттеу жұмыстары мен консультация жүргізеді;
<br><br>
Зертхана барлық керекті құрал-жабдықтармен, ғылыми-әдістемелік әдебиетпен, ихтиологиялық талдау үшін арнайы мамандандырылған жобалармен қамтамасыз етілген.
<br><br>
Жыл сайын зертхана ихтиология мамандығы бойынша білім алатын студенттерді ғылыми-өндірістік тәжірибе өтуге қабылдайды. Зертхана қызметкерлері жылына әр түрлі басылымдарда 40-қа жуық мақала шығарады. Сонымен қатар қызметкерлер әр түрлі тақырыптардағы халықаралық және республикалық форумдарда, симпозиум, семинар, конференцияларда белсенді түрде ат салысады.
<br><br>
Зертхана «Ұлттық сараптау және сертификаттау орталығы» Акционерлік қоғамының Алматы филиалынан 23.12.2015 ж. берілген, зертхананың күшін бағалау (аттестациялау) куәлігі №97/15 негізінде мамандандырылған.
<br><br>
Лаборатория қызметкерлері сонымен қатар Қазақстанның мемлекеттік органдарының да тапсырмасын орындайды.
<br><br>
-Балық шаруашылығы су айдындарының немесе олардың бөліктерінің балық өнімділігін, балық ресурстарының аулауға шекті рұқсат етілген бөлімін және басқа да су жануарларының дамуын айқындайтын, халықаралық, республикалық және жергілікті маңызы бар суқоймаларынан балық өсіру және басқару бойынша ұсыныстар беру.
<br><br>
-Қазақстан Республикасының халықаралық және республикалық маңызы бар суқоймаларының гидробионттары мен басқа су ресурстарын сақтау және қалпына келтіру бойынша балық шаруашылығы саласындағы қолданбалы ғылыми зерттеулер.
<br><br>
– Балық аулау облысының пайдаланушыларымен келісім-шарттар.'
];

?>