<?php 

return [
'head' => 'Достижения гранты МОН',
'head_text' => 'За последние пять лет (2016-2020 гг.) научными сотрудниками НПЦ рыбного хозяйства получено более 50 охранных документов на полезные модели и изобретения, более 250 авторских свидетельств, имеются 2 свидетельства о государственной регистрации объектов интеллектуальной собственности (программы для ЭВМ).',

'title1' => 'Организационно-правовая документация',
'text1' => '<br><br>— Устав ТОО «НПЦ РХ», утвержденный Министерством экологии, геологии и природных ресурсов РК, приказ №315-П от 10.12.2020г.;

                      <br><br>— Свидетельство об аккредитации «Товарищество с ограниченной ответственностью «Научно-производственный центр рыбного хозяйства» аккредитуется в качестве субъекта научной и (или) научно-технической деятельности, серия МК № 005483 от 20.03.2019 г., выданное Министерством образования Республики Казахстан

                      <br><br>— Государственная лицензия № 02072P от 28.03.2019 г. на выполнение работ и оказание услуг в области охраны окружающей среды;

                      <br><br>— Аттестат аккредитации Испытательного центра ТОО «Научно-производственный центр рыбного хозяйства» аккредитован(а) в системе аккредитации Республики Казахстан на соответствие требованиям ГОСТ ISO/IEC 17025-2019  «Общие требования к компетентности испытательных и калибровочных лабораторий», №KZ.T.02.1929 от «13» сентября 2017 года. Дата изменения аттестат аккредитации 28.02.2019г.

                      <br><br>— Свидетельство № 11/19 об оценке состояния измерений (аттестации) лаборатории аквакультуры (выдано 18.02.2019 г. Алматинским филиалом «Национальный центр экспертизы и сертификации».

                      <br><br>— Свидетельство № 13/19 об оценке состояния средств измерений (аттестации) лаборатории ихтиологии (выдано 20.12.2018 г. Алматинским филиалом «Национальный центр экспертизы и сертификации»;

                      <br><br>— Свидетельство № 14/19 об оценке состояния средств измерений (аттестации) лаборатории гидроаналитики (выдано 18.02.2019 г. Алматинским филиалом «Национальный центр экспертизы и сертификации»;

                      <br><br>— Свидетельство № 15/19 об оценке состояния средств измерений (аттестации) лаборатории гидробиологии (выдано 20.02.2019г. Алматинским филиалом «Национальный центр экспертизы и сертификации»;

                      <br><br>— Свидетельство № 12/19 об оценке состояния средств измерений (аттестации) лаборатории генетики гидробионтов (выдано 20.02.2019г. Алматинским филиалом «Национальный центр экспертизы и сертификации»;

                      <br><br>— Сертификат соответствия №KZ 7500175.07.03.00614 от 22.11.2019 г. требованиям СТ РК ISO 9001-2016 «Система менеджмента качества.Требования»;

                      <br><br>— Сертификат соответствия №KZ 7500175.07.03.00615 от 22.11.2019 г. требованиям СТ РК ISO 14001-2016 «Система экологического менеджмента»;

                      <br><br>— Сертификат соответствия №KZ 7500175.07.03.00616 от 22.11.2019 г. требованиям СТ РК ISO 45001-2019 «Системы менеджмента безопасности труда и охраны здоровья. Требования и руководство по применению».',

'title2' => 'Патенты',
'text2' => '<br><br><h2>2017 год</h2>
                      <br>1) Мальковый садок с осушаемым очищаемым мелкоячейным полотном: Патент на ПОЛЕЗНУЮ МОДЕЛЬ 2139 Республики Казахстан, МПК А01К 61/00 (2006.01)
                      <br>2) Глубинное гидроакустическое рыбозащитное устройство для плотин малых гидроэлектростанций: Патент на ПОЛЕЗНУЮ МОДЕЛЬ 2150 Республики Казахстан, МПК Е 02В 8/08 (2006.01)
                      <br>3) Способ выращивания сеголеток карпа и судака: Патент на полезную модель 2159 Республики Казахстан, МПК А01К (2006.01)
                      <br>4) Устройство для вылова и транспортировки рыбы из ставных садков: Патент на полезную модель № 2511 Республики Казахстан, А01К 77/00 (2006.01)
                      <br>5) Устройство для вылова молоди рыб из мальковых прудов: Патент на полезную модель № 2512 Республики Казахстан, А01К 77/00 (2006.01)
                      <br>6) Искусственное нерестилище для осетровых рыб: Патент на полезную модель № 2456 Республики Казахстан, А01К 61/00 (2006.01)
                    <br><br><h2>2018 год</h2>
                      <br>1) Комбикорм для молоди форели с пробиотическим препаратом: Патент на изобретение № 32671 Республики Казахстан, А23К 50/80 (2006.01)
                      <br>2) Глубинное рыбозащитное устройство для затопленных водоприемников береговых водозаборов: Патент на полезную модель №2724 Республики Казахстан, Е02В 8/08 (2006.01)
                      <br>3) Устройство для инкубации оплодотворенной икры осетровых рыб в природных водных условиях: Патент на полезную модель № 2709 Республики Казахстан, A01K 61/00 (2006.01)
                      <br>4) Способ мониторинга ареала обитания и охраны ценных видов рыб с помощью беспилотных летательных аппаратов: Патент на полезную модель № 2681 Республики Казахстан, B64C 39/00 (2006.01)
                      <br>5) Рыбозащитное устройство для малых водозаборов: Патент на полезную модель № 2762 Республики Казахстан, E02B 8/08 (2006.01)
                      <br>6) Рыбозащитное устройство для водосбросных сооружений плотин ГЭС и водохранилищ: Патент на полезную модель № 2763 Республики Казахстан, E02B 8/08 (2006.01)
                      <br>7) Устройство для дегазации и аэрации воды в рыбоводных установках: Патент на полезную модель № 2891 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>8) Способ инкубации икры карпа/сазана: Патент на полезную модель № 2925 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>9) Способ выращивания рыбопосадочного материала севрюги: Патент на полезную модель № 3077 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>10) Устройство для транспортировки живого судака: Патент на полезную модель № 3078 Республики Казахстан, А01К 63/02 (2006.01)
                      <br>11) Устройство для сбора яиц артемии: Патент на полезную модель №3079 Республики Казахстан, А01К 80/00 (2006.01)
                      <br>12) Способ выращивания рыбопосадочного материала русского осетра: Патент на полезную модель №3138 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>13) Способ оценки численности рыб эхолотом: Патент на полезную модель №3242 Республики Казахстан, А01К 79/00 (2006.01)
                      <br>14) Усовершенствованная обкидная двустенная рыболовная сеть: Патент на полезную модель №3404 Республики Казахстан, А01К 69/00 (2006.01)
                      <br>15) Способ расчета численности мигрирующих рыб в реках: Патент на полезную модель №3379 Республики Казахстан, А01К 69/00 (2006.01)
                      <br>16) Садок для подращивания молоди частиковых рыб: Патент на полезную модель №3474 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>17) Способ оценки объемов рыбы вылавливаемой любительским рыболовством: Патент на полезную модель №3475 Республики Казахстан, А01К 69/00 (2006.01)
                    <br><br><h2>2019 год</h2>
                      <br>1)  Устройство для выращивания форели с прямоточной системой водоснабжения: Патент на полезную модель №3603 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>2)  Способ получения посадочного материала белого амура в 3-4 рыбоводной зоне: Патент на полезную модель №3888 Республики Казахстан, А01К 61/00 (2017.01)
                      <br>3)  Способ получения и инкубации оплодотворенной икры судака: Патент на полезную модель №3889 Республики Казахстан, А01К 61/00 (2017.01)
                      <br>4)  Устройство для лова кефали: Патент на полезную модель №3926 Республики Казахстан, А01К 69/08 (2006.01)
                      <br>5)  Способ выращивания двухлеток судака в прудах: Патент на полезную модель №3927 Республики Казахстан, А01К 61/00 (2017.01)
                      <br>6)  Способ культивирования уксусной угрицы (Turbatrix aceti) как стартового корма для молоди рыб: Патент на полезную модель №4073 Республики Казахстан, А23К 10/20 (2016.01)
                      <br>7)  Установка с замкнутым водоснабжением для инкубации икры карпа и растительноядных рыб: Патент на полезную модель №4075 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>8)  Способ транспортировки разновозрастных особей сазана/карпа: Патент на полезную модель №4231 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>9)  Усовершенствованный пруд для выращивания ранней молоди сазана: Патент на полезную модель №4232 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>10) Усовершенствованный способ мониторинга и охраны крупных рыбохозяйственных водоемов: Патент на полезную модель №4236 Республики Казахстан, G01N 33/18 (2006.01), А01К 61/00 (2006.01)
                      <br>11) Способ изготовления коллекционных экземпляров рыб и других холоднокровных позвоночных: Патент на полезную модель №4404 Республики Казахстан, А01N 1/00 (2006.01)
                      <br>12) Способ изготовления экспонатов рыб, амфибий, рептилий: Патент на полезную модель №4405 Республики Казахстан, А01N 1/00 (2006.01)
                      <br>13) Русловой нерестовый комплекс для воспроизводства судака: Патент на полезную модель № 4485 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>14) Устройство для крепления сенсора эхолота: Патент на полезную модель №4493 Республики Казахстан, А01К 79/00 (2006.01), F16В 45/00 (2006.01)
                      <br>15) Рыболовная накидная сеть с постоянной площадью облова: Патент на полезную модель №4486 Республики Казахстан, А01К 74/00 (2006.01)
                      <br>16) Устройство для отбора бентосных проб в водоемах с различным типом рельефа дна: Патент на полезную модель №4499  Республики Казахстан, В07В 1/04 (2006.01)
                    <br><br><h2>2020 год</h2>
                      <br>1) Способ выращивания трехлеток судака в прудах: Патент на полезную модель №4967 Республики Казахстан, А01/К 61/00(2006.01), в рамках проекта №АР05130395 (МОН)
                      <br>2) Усовершенствованный съемный контейнер для перевозки живой молоди сазана: Патент на полезную модель №4968 Республики Казахстан, А01/К 61/00(2017.01), в раиках проекта №0147-18-ГК (АО Фонд науки)
                      <br>3) Способ культивирования белого энхитрея (Enchytraeus albidus) в качестве живого стартового корма для молоди рыб: Патент на полезную модель №5065 Республики Казахстан, А23/К 1/00 А01К 61/00(2017.01)
                      <br>4) Усовершенствованная раколовка-ловушка: Патент на полезную модель №5164 Республики Казахстан, А01К 69/08 (2006.01)
                      <br>5) Усовершенствованный гидробиологический бимтрал для лова донных и придонных организмов: Патент на полезную модель №5179 Республики Казахстан, А01М 23/02 (2006.01)
                      <br>6) Усовершенствованный пруд для содержания сеголеток судака: Патент на полезную модель №5163 Республики Казахстан, А01К 61/00 (2006.01), в рамках проекта №0008-17-ГК (АО Фонд науки)
                      <br>7) Способ определения численности рыб в водоемах местного значения: Патент на полезную модель №5219 Республики Казахстан, А01К 69/00 (2006.01)
                      <br>8) Усовершенствованный способ определения уловистости ставных сетей при научном лове рыбы: Патент на полезную модель №5220 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>9) Усовершенствованный способ выращивания сеголеток сазана в прудах: Патент на полезную модель №5221 Республики Казахстан, А01К 61/00 (2006.01), в раиках проекта №0147-18-ГК (АО Фонд науки)
                      <br>10) Способ обеклеивания икры осетровых рыб: Патент на полезную модель №5223 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>11) Способ совмещенной оценки численности рыб ставными сетями и эхолотом: Патент на полезную модель №5222 Республики Казахстан, А01К 69/00 (2006.01)
                      <br>12) Усовершенствованный способ разведения раков в мелководных озерах: Патент на полезную модель № 5189 Республики Казахстан, А01К 61/00 (2006.01)
                      <br>13) Способ повышения результативности зарыбления водоемов: Патент на полезную модель №5273 Республики Казахстан, МПК А01К 61/00 (2006.01)
                      <br>14) Способ получения стартового комбикорма для личинок и молоди судака: Патент на изобретение №34532, МПК А01К 61/10 (2017.01), А23К 50/80 (2016.01), А23К 40/25 (2016.01)
                      <br>15) Усовершенствованный гидробиологический ковшовый дночерпатель: Патент на полезную модель №5266 Республики Казахстан, МПК А01М 23/02 (2006.01)
                      <br>16) Мобильная установка с замкнутым водоснабжением для выращивания молоди ценных видов рыб: Патент на полезную модель №5333 Республики Казахстан, МПК А01К 61/00 (2006.01)
                      <br>17) Экологичный закидной рыболовный невод: Патент на полезную модель №5618 Республики Казахстан, МПК А01К 69/00 (2006.01)
                      <br>18) Усовершенствованный инкубационный аппарат: Патент на полезную модель №5716 Республики Казахстан, МПК А01К 61/00 (2017.01).
                    <br><br><h2>2021 год</h2>
                      <br>1) Способ получения зернистой пастеризованной икры осетровых рыб: Патент на полезную модель №5769 Республики Казахстан, МПК А23L 17/30 (2006.01).
                      <br>2) Орудие для лова кефали в мелководной акватории моря: Патент на полезную модель №6527 Республики Казахстан, МПК АО1К 69/08.',

'title3' => 'Научные публикации',
'text3' => '<h2>Научные публикации в журналах, индексированные в базах данных Scopus и Web of Science</h2>
                      <br><br><h2>2016 год</h2>
                      Study of the influence of combined feed of domestic production of trout micro flora// Shalgimbayeva Saule, Isbekov Kuanysh, Assylbekova Saule, Sadvakasova Asem, Akmuhanova Nurziya, Koishybayeva Saya // journal of biotechnology/Том:Стр.:S5051.Приложение:S.
                      <br><br>Kurzhykayev Zhumagazy., Akhmedinov Serikbay N. // Quantitave development and distribution of zooplankton in medium lakes of the kostanay reqion (north Kazakhstan reqion) // International. journal.of environmetal. et science education 2016.-vol.ll.-№ 15. – 8193-8210
                      
                      <br><br><h2>2017 год</h2>

                      Levin, Boris,   Levina, Marina,   Interesova, Elena,  Pilin D.V. Phylogeny and phylogeography of the roaches, genus Rutilus (Cyprinidae), at the Eastern part of its range as inferred from mtDNA analysis// Hydrobiologia (2017) -Т.788. –P.33-46 ISSN:0018-8158
                      <br><br>Assessment of impacts and potential mitigation for icebreaking vessels transiting pupping areas of an ice-breeding seal//Susan C. Wilsona,⁎, Irina Trukhanovab, Lilia Dmitrievac, Evgeniya Dolgovad, Imogen Crawforda,Mirgaliy Baimukanove, Timur Baimukanove, Bekzat Ismagambetove, Meirambek Pazylbekovf,Mart Jüssig, Simon J. Goodmanc,// Biological Conservation 214, October 2017, Pages 213–222
                      <br><br>Comparative assessment of food resources of valuable Sander lucioperca m the natural habitat and in ponds of the south-eastern Kazakhstan// Zhanara Mazhibayeva, Sayle Asylbekova, Larisa Kovaleva, Tynysbek Barakbayev and Luidmila Scharapova / EM International. Ecology, Environment and Conservation Paper. Vol 23, Issue 3, 2017; Page No.(l728-1737).
                      <br><br>Тhe results of nile tilapia (Оreochromis niloticus l.) Breeding in pond farm of almaty region using locally made experimental productive food// Damir Kayerkeldiyevich Zharkenov, Kuanysh Baybolatovich Isbekov, Toleukhan Sadykulovich Sadykulov, Jоzsef Pekli and Nina Sergeyevna Badryzlova // Ecology environment and conservation journal 0971765X- India Vol 23, Issue 3, 2017; Page No.(1273-1280)
                      <br><br>Risk assessment of pet-traded decapod crustaceans in the Republic of Kazakhstan, the leading country in Central Asia//Uderbayev, T (Uderbayev, Talgat)[ 1 ] ; Patoka, J (Patoka, Jiri)[ 2 ] ; Beisembayev, R (Beisembayev, Ruslan)[ 1 ] ; Petrtyl, M (Petrtyl, Miloslav)[ 2 ] ; Blaha, M (Blaha, Martin)[ 3 ] ; Kouba, A (Kouba, Antonin)[ 3 ]/ KNOWLEDGE AND MANAGEMENT OF AQUATIC ECOSYSTEMS//Выпуск:418, Номер статьи:30,
                       
                      <br><br><h2>2018 год</h2>

                      <br><br>Structural Indicators of Zooplankton in the Shardara Reservoir (Kazakhstan) and the Main Influencing Factors/ Elena Krupa, Sophia Barinova, Saule Assylbekova, Kuanysh Isbekov// Turkish Journal of Fisheries and Aquatic Sciences 18: 659-669 р. (2018) trjfas.org
                      <br><br>The use of zooplankton distribution maps for assessment of ecological status of the Shardara reservoir (Southern Kazakhstan)// Elena Krupa, Sophia Barinova, Kuanysh Isbekov, Saule Assylbekova//Ecohydrology & Hydrobiology, Volume 18, Issue 1, January 2018, Pages 52-65.
                      <br><br>Morfometric parameters of a three-year-old pikeperch (Stizostedion lucioperca) grown in pond farmin in the Almaty region in a polyculture with carp and herbivorous fish// Saya Koishybayeva, Shokhan Alpeisov, Saule Assylbekova, Tynysbek Barakbayev //EurAsian Journal of BioSciences Eurasia J Biosci 12, 69-75 (2018)
                      <br><br>The application of phytoplankton in ecological assessment of the balkhash lake (Kazakhstan). BARINOVA, S. – KRUPA, E. – TSOY, V. – PONAMAREVA, L., Applied Ecology and Environmental Research 16(3) · June 2018 with197 Reads DOI: 10.15666/aeer/1603_20892111
                      
                      <br><br><h2>2019 год</h2> 

                      <br><br>Growth and length-weight relationships of Aral Sazan Cyprinus Carpio Aralensis Spishakow, 1935 (Cyprinidae; Osteichthyes) in the Sarysu River Watershed//Vladimir Krainyuk, Saule Assylbekova, Olga Kirichenko, Kuanysh Isbekov, Talgat Abzhanov// Eurasian Journal of Biosciences, 2019 — Volume 13 Issue 1, pp. 533-539
                      <br><br>Assessment of the production potential of two-year-old pike-perch cultivated in ponds for the formation of RBS// Nina Badryzlova, Saya Koishybayeva, Saule  Assylbekova, Kuanysh Isbekov// Eurasian Journal of Biosciences, 2019 — Volume 13 Issue 1, pp. 409-417
                      <br><br>Influence of different feeds and feed additives on fish-breeding and biological indicators at rearing rainbow trout// A. A. Aitkaliyeva, Sh. A. Alpeisov, K. B. Isbekov, S. Zh. Assylbekova, N. S. Badryzlova// Eurasian Journal of Biosciences, 2019 — Volume 13 Issue 1, pp. 437-442
                      <br><br>Impacts of water level changes in the fauna, flora and physical properties over the Balkhash Lake watershed /B. Isbekov, Kuanysh & N. Tsoy, Vyacheslav & Cretaux, J & V. Aladin, Nikolai & Plotnikov, Igor & Clos, Guillaume & Bergé-Nguyen, Muriel & Zh. Assylbekova, Saule. (2019). Lakes & Reservoirs: Research & Management. 10.1111/lre.12263. Lakes & Reservoirs: Research & Management. 10.1111/lre.12263. (2019). Lakes & Reserv. 2019;00:1–14., © 2019 John Wiley & Sons Australia, Ltd. wileyonlinelibrary.com/journal/lre.
                      <br><br>Caddisfly Assemblages in Metal Contaminated Rivers of the Tikhaya Basin, East Kazakhstan / Anna A. Evseeva, Liubov V. Yanyginа// Bulletin of Environmental Contamination and Toxicology. – Том: 102. – Выпуск: 3. – Стр.: 316-322,https://doi.org/10.1007/s00128-019-02561-w, https://rdcu.be/blBUV.
                      <br><br>Actual status of fishing reserves of the Yesil River // Zhumagazy Kurzhykayev, Kuanysh Syzdykov, Ainur Assylbekova, Dinara Sabdinova, Viktor Fefelov // ZOOLOGIA. – Том 36: 1-9, № e30437, ISSN 1984-4689 (online),   https://zoologia.pensoft.net/article/30437/.
                      
                      <br><br><h2>2020 год</h2>

                      <br><br>Метод учета и определения линейных размеров каспийских тюленей (Pusa caspica) на лежбищах с помощью мультикоптеров // М. Т. Баймуканов, Л. А. Жданко, Т. Т. Баймуканов, Е. С. Дауенев, С. Е. Рыскулов, А. М. Баймуканова // Зоологический журнал. – 2020. — Том 99. – № 2. – С. 215–222.
                      <br><br>Evaluation of the habitat state of the Zhaiyk River Ichthyofauna in modern conditions and its influence on the impacts of anthropogenic factors //  Saule Assylbekova, Kuanysh Isbekov, Damir Zharkenov, Yevgeniy Kulikov, Yerbolat Kadimov, Olga Sharipova// Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 467-473.
                      <br><br>Technology of formation of replacement-brood stock of pikeperch in conditions of fish farms in Kazakhstan // Nina Badryzlova, Saya Koishybayeva, Saule Assylbekova, Kuanysh Isbekov// Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 441-447.
                      <br><br>Prospects for growing juveniles and rearing fingerlings of pikeperch (Sander lucioperca) in cages in the conditions of fish farming of Almaty region // Gulmira M. Ablaisanova, Saule Zh. Assylbekova, Adilkhan Ab. Sambetbaev, Piotr J. Gomulka, Kuanysh B. Isbekov, Nina S. Badryzlova, Saya K. Koishybayeva // Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 293-299.
                      <br><br>Technology of cultivation of feeder fish for culturing tilapia (Tilapia) and clarid catfish (Clarias gariepinus) in the VI fish-breeding zone of Kazakhstan // Z.T. Bolatbekova, S.Zh. Assylbekova, B.T. Kulatayev, T. Policar, K.B. Isbekov, S.К. Koishybayeva // Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 475-481.
                      <br><br>Use of domestic starter feeds for culturing clarid catfish and tilapia // Alyona Mukhramova, Saule Assylbekova, Adilkhan Sambetbaev, Tomáš Policar, Kuanysh Isbekov, Saya Koishybayeva, Nina Badryzlova // Eurasian Journal of Biosciences, 2020 – Volume 14, lssue l, pages 453-458.
                      <br><br>A new strain group of common carp: The genetic differences and admixture events between Cyprinus carpio breeds // Nedoluzhko, Artem; Slobodova, Natalia; Sharko, Fedor; Shalgimbayeva, Gulmira; Tsygankova, Svetlana; Boulygina, Eugenia; Jeney, Zsigmond; Nguyen, Van Quan; Nguyen, Đức Thế Thế; Phạm, Thế Thư; Volkov, Alexander; Fernandes, Jorge; Rastorguev, Sergey // Ecology and Evolution, 2020;00:1–9.
                      
                      <br><br><h2>2021 год</h2> 

                      Abdybekova AM, Assylbekova SZh, Abdibayeva AA, Zhaksylykova AA, Barbol BI, Aubakirov MZh, Torgerson PR (2021). Studies on the population biology of helminth parasites of fish species from the Caspian Sea drainage basin. Journal of Helminthology 95, e12, 1–8. https://doi.org/10.1017/S0022149X2100002X
                      <br><br>Aubakirova, M.; Krupa, E.; Mazhibayeva, Z.; Isbekov, K.; Assylbekova, S. The Role of External Factors in the Variability of the Structure of the Zooplankton Community of Small Lakes (South-East Kazakhstan). Water 2021, 13, 962. https://doi.org/10.3390/w13070962
                      <br><br>Abdybekova AM, Assylbekova SZh, Abdibayeva AA, Zhaksylykova AA, Barbol BI, Aubakirov MZh, Torgerson PR (2021). Studies on the population biology of helminth parasites of fish species from the Caspian Sea drainage basin. Journal of Helminthology 95, e12, 1–8. https://doi.org/10.1017/S0022149X2100002X
                      <br><br>Aubakirova, M.; Krupa, E.; Mazhibayeva, Z.; Isbekov, K.; Assylbekova, S. The Role of External Factors in the Variability of the Structure of the Zooplankton Community of Small Lakes (South-East Kazakhstan). Water 2021, 13, 962. https://doi.org/10.3390/w13070962
                      <br><br>Artem V.Nedoluzhko, Maria V.Gladysheva-Azgari, Gulmira M.Shalgimbayeva, Alexander A.Volkov, Natalia V.Slobodova, Svetlana V.Tsygankova, Eugenia S.Boulygina, Van Q.Nguyen, The T. Pham, Duc T.Nguyen, Fedor S.Sharko, Sergey M.Rastorguev. Genetic contribution of domestic European common carp (Cyprinus carpio carpio) and Amur carp (Cyprinus carpio haematopterus) to the wild Vietnamese carp population as revealed by ddRAD sequencing. Aquaculture 544 (2021), 737049, https://doi.org/10.1016/j.aquaculture.2021.737049
                      <br><br>Shalgimbayeva, G.; Volkov, A.; Slobodova, N.; Sharko, F.; Tsygankova, S.; Nguyen, V.Q.; Pham, T.T.; Nguyen, D.T.; Assylbekova, S.Zh., Alekseev, Y., Nedoluzhko, A., Fernandes, J.M.O. and Rastorguev, S. Genetic Investigation of Aral Wild Common Carp Populations (Cyprinus carpio) Using ddRAD Sequencing. Diversity 2021, 13(7), 295; P. 1-9  https://doi.org/10.3390/d13070295
                      <br><br>Ekaterina V. Mikodina, Gulmira M. Shalgimbaeva, Alexander A. Volkov. Landscape and Climate Role in the Formation of Sturgeon Reproduction Biotopes in the Ural River (Zhaiyk).  E3S Web of Conferences 265, 01011 (2021). APEEM 2021. https://doi.org/10.1051/e3sconf/202126501011
                      <br><br>Abilov, B.I, Isbekov, K. B, Assylbekova, S. Zh, Bulavina, N. B, Kulmanova, G.A, Koishybayeva, S. K, Nikolova, L. Evaluation of Production and Economic Performance of Farmed Carp Using Small Lake-Commercial Fish Farms System in Southeastern Kazakhstan  Archives of Razi Institute, DOI: https://doi.org/10.22092/ARI.2021.355785.1722
                      <br><br>Kushnikova L.B. - снс Nigmetzhanov S.B. – и.о. нс, Samarkhanov T.N., Myrzagaliyeva A.B., Chlachula .J,    Czerniawska J.  Geoenvironmental Implications and Biocenosis of Freshwater Lakes in the Arid Zone of East Kazakhstan (Импакт-фактор: 2.576) https://www.mdpi.com/2071-1050/13/10/5756. Sustainability 2021, 13(10), 5756,  https://doi.org/10.3390/su13105756',

'title4' => 'Коммерциализация',
'text4' => '<br><br><h2>2017 год</h2>

                      В 2017 году ТОО «НПЦ РХ» разработана технология производства рыбопосадочного материала и товарной продукции судака, имеющая мировую новизну. Пилотный проект «Искусственное воспроизводство, выращивание и реализация рыбопосадочного материала судака для зарыбления естественных водоемов» по получению молоди судака для зарыбления естественных водоемов реализуется научными сотрудниками ТОО «НПЦ РХ» в Кызылординской области. Проектная мощность 240 тысяч сеголетков судака в год.

                      Выполнение проекта по коммерциализации судака позволит поддерживать промысловые запасы этого объекта рыболовства, стабилизировать уловы и поставки товарной продукции судака на внутренний и внешний рынок, что усилит экспортные возможности Казахстана. Данный проект выполняется при поддержке АО «Фонд науки».

                      <br><br><h2>2018 год</h2>

                      В 2018 г. институтом получен грант по линии Фонда науки МОН РК по проекту: «Искусственное воспроизводство и выращивание рыбопосадочного материала сазана с целью дальнейшей его реализации для зарыбления естественных водоемов Республики Казахстан».

                      Зарыбление естественных водоемов Казахстана жизнестойким рыбопосадочным материалом сазана позволит поддерживать промысловые запасы этого объекта рыболовства, стабилизировать уловы и поставки товарной продукции сазана на внутренний рынок Казахстана. Предлагаемая технология разведения и выращивания рыбопосадочного материала сазана является эффективной для применения на рыбоводных предприятиях Казахстана.',

'title5' => 'ГРАНТ МОН РК (КМУ) по изучению артемии Artemia franciscana на 2021-2023 гг.',
'text5' => '<h2>ИРН AP09058158 «Изучение экспортоориентированного биоресурса — Artemia franciscana для разработки биологического обоснования по их интродукции, с целью повышения продуктивности соляных водоемов РК»</h2>
                      <h2>Актуальность.</h2>
                      Данная работа даст возможность повышения продуктивности горько-соленных водоемах РК путем внедрения нового для Казахстана вида – Artemia franciscana. Новизна работы заключается в том, что, работ направленных на повышения запасов и качества цист артемии до настоящего время в РК не было. 
                      <br><br>
                      <h2>Цель.</h2>
                      Изучить возможности и основные критерии выживания рачка Artemia franciscana и сравнить продукционные характеристики двух популяций ценного биоресурса – Artemia parthenogenetica и Artemia franciscana, в целях повышения продуктивности горько-соленных водоемов РК, для получения высококачественной конкурентоспособной продукции для реализации на внутреннем и внешнем рынках. 
                      <br><br>
                      <h2>Ожидаемые результаты.</h2>

                      В рамках проекта будет разработано биологическое обоснование по интродукции и рациональному использованию промысловых ценных беспозвоночных в горько-соленных водоёмах Казахстана. Реализация поставленных задач поможет обнаружить межпопуляционную конкурентоспособность двух популяций рачков, степень их выживаемости, вследствие, чего определится преобладание более продуктивной популяций. Исследования, определяющие экономические аспекты выращивания артемии в регионах РК практически отсутствуют, в связи с чем, существует необходимость проведения работ для оценки влияния различных факторов на продукционные популяционные характеристики Artemia franciscana при интродукции цист артемии в естественные водоемы.
                      <br><br>
                      <h2>Достигнутые результаты за полгода.</h2>

                      Произведен поиск поставщиков и заключены договора на поставку оборудования и расходных материалов, проведен закуп оборудования и расходных материалов для реализации проекта. Начаты обследования физико-географических и морфологических характеристик горько-соленных водоемов Петропавловской (озера Менгесор и Становое) и Павлодарской (озера Шарбакты, Сейтень, Казы и Калатуз) областей, с использованием навигационных приборов, беспилотных летательных аппаратов и др. По каждому водоему были получены координаты, замеры глубин, солёности воды, температуры и прозрачности воды. Проведена оценка гидрохимического режима выбранных водоемов РК, собран материал по гидробиологии, гидрохимии, генетике и биотехнологии.
                      <br><br>
                      <h2>ФИО членов исследовательской группы с их идентификаторами (Scopus Author ID, Researcher ID, ORCID, если имеются) и ссылками на соответствующие профили.</h2>
                      <br><br> <h2>Баракбаев Т.Т.,</h2> доктор философии (РhD), Директор Аральского филиала ТОО «НПЦРХ» руководитель проекта, https://orcid.org/0000-0002-9047-5274,  https://www.scopus.com/authid/detail.uri?authorId=56974343200;
                      <br><br> <h2>Мажибаева Ж.О.,</h2> доктор философии (РhD) заведующая лабораторией гидробиологии и гидроаналитики ТОО «НПЦРХ», https://orcid.org/0000-0002-5013-0503, https://www.scopus.com/authid/detail.uri?authorId=57200420063;
                      <br><br> <h2>Фефелов В.В.,</h2> зав. лабораторией Северного филиала ТОО «НПЦРХ», https://orcid.org/0000-0002-1916-393X,  https://www.scopus.com/authid/detail.uri?authorId=57211521877;
                      <br><br> <h2>Минат А., младший научный сотрудник гидробиологии и гидроаналитики ТОО «НПЦРХ», магистр;
                      <br><br> <h2>Кожижанова Б.А.,</h2> младший научный сотрудник гидробиологии и гидроаналитики ТОО «НПЦРХ», магистр, https://orcid.org/0000-0002-9012-0406,
                      <br><br> <h2>Молдрахман А.С.,</h2> младший научный сотрудник гидробиологии и гидроаналитики исполнитель ТОО «НПЦРХ», магистрант, https://orcid.org/0000-0002-9619-4262,
                      <br><br> <h2>Болатбекова З.Т.</h2> младший научный сотрудник лаборатории генетики гидробионтов ТОО «НПЦРХ», докторант 3 курса, https://orcid.org/0000-0001-8766-662X, https://www.scopus.com/authid/detail.uri?authorId=57216550134;
                      <br><br> <h2>Бочарова Е.С.,</h2> специалист-генетик ВНИРО, https://www.scopus.com/redirect.uri?url=https://orcid.org/0000-0001-9978-3006,  https://www.scopus.com/authid/detail.uri?authorId=54683458400 
                      <br><br>
                      <h2>Информация для потенциальных пользователей.</h2>

                      Работы по проекту направлены на повышения запасов и качества цист артемии, чего до настоящего время в РК не было. Реализация проекта имеет важность для национальной экономики. Казахстан обладает 10-15 % мировых запасов артемии, лимит добычи цист с водоемов республики на 2019 год установлен в размере 1492,81 тонн (лимит/квота). Основную часть указанного объема составляют озера Павлодарской области (1116,0 тонн в год). Освоение и повышение продуктивности биоресурса цист артемии в соленых водоемах Казахстана может быть одним из основных направлений развития отечественного производства стартовых биокормов водного происхождения для нужд рыбоводства и аквакультуры.
                      <br><br>',

'title6' => 'ГРАНТ МОН РК (КМУ) по разработке РЗУ на 2021-2023 гг.',
'text6' => '<h2>ИРН АР09058066 «Разработка активного передвижного гидроакустического рыбозащитного устройства, для защиты рыб и молоди от попадания в водосбросы плотин крупных ГЭС и водохранилищ»</h2>

                      <h2>Актуальность.</h2>
                      В настоящее время не производятся эффективные рыбозащитные устройства для защиты рыб и молоди рыб от попадания в водосбросные сооружения плотин крупных ГЭС и водохранилищ. Водосбросные сооружения крупных гидроузлов, характеризуются сверхвысокими (до 500 м3/с) объемами сбрасываемой воды. При этом на большом участке перед водосбросом возникает мощное течение, которое затягивает через водосброс рыбу и ее мальков.  Традиционные виды рыбозащиты здесь малопригодны, ввиду экстремальных условий сверхвысокого объема водосброса. Научная новизна проекта заключается в реализации нового подхода в области рыбозащитных устройств – активной подвижной высокоинтенсивной акустической рыбозащиты, способной эффективно защищать рыб и молодь на водосбросах плотин крупных ГЭС и водохранилищ.
                      <br><br>
                      <h2>Цель.</h2>
                      Разработать эффективное рыбозащитное устройство (далее РЗУ), для защиты рыб и молоди рыб от попадания в водосбросные сооружения ГЭС и других крупных гидроузлов.
                      <br><br>
                      <h2>Ожидаемые результаты.</h2>

                      Разработка эффективного рыбозащитного устройства для крупных гидроузлов с залповыми водосбросами, изготовление и испытание опытного образца, оформлении пакета документов на изготовление и эксплуатацию.
                      <br><br>
                      <h2>Достигнутые результаты за полгода.</h2>

                      Проведено исследование гидрологических и ихтиологических условий водоема и водосброса плотины. Для данных целей в качестве модельного водоема выбрано Кировское водохранилище УКООС в Западно-Казахстанской области. В качестве модельного водосброса выбрана плотина Кировского водохранилища. В плане выполнения гидрологических исследований проведено  75 промеров глубин, и 85 замеров скорости течения, на акватории водохранилища и в районе водосбросного сооружения плотины. В плане ихтиологических исследований проведено 10 научных ловов, установлены концентрации и пути миграций рыб. Взяты на определение видового, возрастного и размерно-весового состава ихтиофауны 320 экземпляров рыб. Для определения концентрации, миграций и ската молоди рыб, взято 20 проб ранней молоди кругом Расса, и 10 проб поздней молоди мальковой волокушей.
                      <br><br>
                      В результате проведенных исследований получены исходные гидрологические и ихтиологические данные, для принятия правильных решений по конструкции, комплектации и режиму эксплуатации активного передвижного гидроакустического рыбозащитного устройства.
                      <br><br>

                      <h2>ФИО членов исследовательской группы с их идентификаторами (Scopus Author ID, Researcher ID, ORCID, если имеются) и ссылками на соответствующие профили.</h2>

                      <br><br> <h2>Туменов А.Н.:</h2> директор Западно-Казахстанского филиала, номер ORCID — https://orcid.org/0000-0001-7995-2001;   Web of Science Researcher ID — AAU-4496-2020; Ссылка на профиль в Web of science — <br><br> https://app.webofknowledge.com/ author/#/record/10019388?lang =ru_RU&SID=F43MOImdIjBQwD1lXBJ
                      <br><br> Ссылка на профиль Scopus — https://www.scopus.com/authid/detail.uri?authorId=57193997565;
                      <br><br> <h2>Пилин Д.В.:</h2>  старший научный сотрудник Западно-Казахстанского филиала, номер ORCID — https://orcid.org/0000-0003-4188-1185; Web of Science Researcher ID — AAK-9968-2020; Ссылка на профиль в Web of science —http://apps.webofknowledge.com/WOS_GeneralSearch_input.do?product=WOS&search_mode=GeneralSearch&SID=E1oHp3RxGWKIOCrGl5N&preferencesSaved=
                      <br><br> Ссылка на профиль Scopus — https://www.scopus.com/authid/detail.uri?authorId=57191248333&amp;eid=2-s2.0-84988378030
                      <br><br> <h2>Мухрамова А.А.:</h2> главный ученый секретарь, номер ORCID https://orcid.org/0000-0002-4701-6195
                      <br><br> Web of Science Researcher ID — AAU-9605-2020; Ссылка на профиль Scopus: https://www.scopus.com/authid/detail.uri?authorId=57216540827
                      <br><br> <h2>Тулеуов А.М.:</h2> научный сотрудник Западно-Казахстанского филиала, номер ORCID https://orcid.org/0000-0003-3313-5203
                      <br><br> <h2>Булеков Н.У.:</h2> младший научный сотрудник Западно-Казахстанского филиала;
                      <br><br> <h2>Баяндина А.М.:</h2> главный экономист, номер ORCID https://orcid.org/0000-0003-4975-0681

                      <h2>Информация для потенциальных пользователей.</h2>

                      Данный вид рыбозащитного устройства может быть применен в области водного и сельского хозяйства для водопользователей, на крупных ГЭС и водохранилищах, для повышения уровня безопасности молоди рыб. Согласно действующего Водного кодекса Республики Казахстан эксплуатация водозаборов без РЗУ не допускается. Данная разработка направлена на решение насущной проблемы оснащения водосбросов плотин крупных ГЭС и водохранилищ (напр. каскад ГЭС на р.Ертыс, Капшагайская ГЭС, плотины водохранилищ Актюбинское море, Шардара, УКООС, др.), эффективными отечественными РЗУ.
                      <br><br>',

'title7' => 'Научно-техническая программа «Аквакультура» (ПЦФ) на 2021-2023 гг.',
'text7' => '',

'title8' => 'Научно-техническая программа «Природные ресурсы» (ПЦФ) на 2021-2023 гг.',

'management' => 'Руководство',
];

?>