<?php 

return [
'head' => 'Западно-Казахстанский филиал', 
'manager' => 'Директор Западно-Казахстанского филиала', 
'managername' => 'Туменов Артур Насибуллаулы, PhD', 
'title1' => 'Общая информация', 
'text1' => 'Западно-Казахстанский филиал ТОО «НПЦ РХ» осуществляет постоянный мониторинг состояния экосистем водоемов Западно-Казахстанской и Актюбинской областей, разрабатывает биологические основы рационального использования и воспроизводства рыбных запасов, дает ежегодные прогнозы объемов вылова рыбы, предложения по правилам и режиму рыболовства. Ежегодно на основании научно-обоснованных данных филиала, выходит Постановление Правительства по лимитам вылова рыбы из водоемов Западно-Казахстанской и Актюбинской областей. Накоплен многолетний банк данных по гидрологии, гидробиологии, гидрохимии и составе ихтиофауны изучаемых водоемов.', 
'title3' => 'Направление научно-исследовательской деятельности', 
'text3' => '<br>• Мониторинг экологического состояния водоемов Западно-Казахстанской и Актюбинской областей.

<br>• Изучение феногенетической структуры популяций основных промысловых видов рыб в водоемах водоемов Западно-Казахстанской и Актюбинской областей.

<br>• Совершенствование биотехнологий разведения ценных видов рыб в водоемах Казахстана.

<br>• Создание научных основ применения предосторожного подхода в регулировании рыболовства на водоемах Казахстана.

<br>• Обоснование создания и развития сети особо охраняемых природных территорий для сохранения биоразнообразия рыб.

<br>• Внедрение достижений рыбохозяйственной науки в производство.

<br>• Прохождение учебно-научно-производственной практики для студентов Западно-Казахстанского аграрно-технического университета имени Жангир хана по специальности «Рыбное хозяйство».', 
'title4' => 'Услуги предоставляемые филиалом', 
'text4' => '<br>• Проведение бонитировки водоемов с целью их рыбохозяйственного использования.

<br>• Определение ущерба, наносимого рыбному хозяйству от антропогенного воздействия.

<br>• Разработка рекомендаций и биологических обоснований по зарыблению водоемов региона и акклиматизации промысловых видов рыб и кормовых организмов.

<br>• Работы по экологическому проектированию и нормированию.

<br>• Разработка биологических обоснований общего допустимого улова рыб (ОДУ).

<br>• Разработка рекомендаций по рыбохозяйственному устройству промысловых участков природопользователей.

<br>• Проведение мониторинговых исследований состояния низших гидробионтов –беспозвоночных животных, в разнотипных водоемах.

<br>• Оценка состояния кормовых ресурсов водоемов.

<br>• Выявление степени потребления кормовых организмов и накормленности рыб в естественных водоемах.

<br>• Экспертная оценка состояния сообществ низших гидробионтов водоемов по литературным и архивным материалам.

<br>• Оценка загрязнения водоемов органическими веществами по состоянию низших гидробионтов – индикаторов сапробности водной толщи и грунтов.

<br>• Разработка биологических обоснований прогноза и лимита добычи цист артемии, а также других ценных водных беспозвоночных.

<br>• Рецензирование биологических обоснований, научных разработок и проектов по разнотипным водоемам с присутствием гидробиологической направленности.

<br>• Методическая помощь, консультации по вопросам гидробиологии.

<br>• Разработка проектов по определению норм предельно-допустимых сбросов (ПДС) для предприятий воспроизводства и переработки рыбной отрасли.

<br>• Проведение диагностических и лечебно-оздоровительных работ в рыбоводных хозяйствах.

<br>• Научное обоснование при экологической паспортизации водоёмов.

<br>• Изучение и оценка эпизоотической обстановки рыбохозяйственных водоемов и рыбоводных хозяйств.

<br>• Разработка технико-экономических обоснований для развития рыбного хозяйства и организации особо охраняемых природных территорий.

<br>• Комплексное научно-методическое сопровождение и практическая поддержка при организации товарного рыбоводства: разработка биологического обоснования на организацию и ведение товарного рыбоводства, с определением оптимальных направлений и технологий аквакультуры.

<br>• Посреднические услуги в обеспечении рыбопосадочным материалом и специальным инвентарем.

<br>• Содействие в составлении бизнес-плана для рыбоводных предприятий и фермерских хозяйств.

<br>• Оказание консультативных услуг по биотехнике товарного выращивания осетровых рыб.

<br>• Консультации и участие специалистов по искусственному воспроизводству карпа (сазана) и растительноядных рыб.

<br>• Консультации для предпринимателей по вопросам ведения рыбоводных хозяйств.

<br>• Проектировочные работы по созданию фермерских рыбоводных хозяйств.

', 
];

?>