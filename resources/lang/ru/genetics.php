<?php 

return [
'head' => 'Лаборатория генетики гидробионтов',
'manager' => 'Заведующий лабораторией',
'managername' => 'Шалгимбаева Гульмира Мухаметкалиевна',

'text1' => '<h2>Выполняемые виды работ:</h2>
              <p> <br>• Проведение исследований по оценке популяционно-генетической структуры видов — объектов промысла;
                  <br>• Проведение генетического анализа водных организмов для определения видовой, популяционной и индивидуальной генетической изменчивости;
                  <br>• Проведение генетической паспортизации производителей ремонтно-маточных стад осетровых, разработка генетических паспортов пород объектов искусственного воспроизводства.
                  <br>• Проведение научных экспертиз водных биоресурсов и продукции из них с целью определения видового состава и происхождения представленного заказчиком материала.
                  <br>• Проведение генетической сертификации объектов, импорт и экспорт которых регулируется правилами CITES (Convention International Tradein Endangered Species of Wild Fauna and Flora)
                  </p>',
'text2' => '<p> Лаборатория оснащена всем необходимым оборудованием, научно-методической литературой, специализированными программами для анализа данных.
                  <br><br>
                  Сотрудники лаборатории постоянно повышают свою квалификацию изучением научной литературы, участием в научных конференциях, семинарах, проходят стажировки по направлениям деятельности.
                  <br><br>
                  Лаборатория функционирует с 2017 года.
                  <br><br>
                  Сотрудниками лаборатории выполняются работы по заказам государственных органов Республики Казахстан
              </p>',
];

?>